extends Node

var quest
var state setget _set_state
var journal_page

func _init(new_quest):
	quest = new_quest
	journal_page = JournalPage.new(_get_title())

func add_to_journal():
	Runtime.Game.Journal.add_page(journal_page)

func _set_state(new_state):
	var new_entry = _get_state_change_entry(state, new_state)
	state = new_state
	
	if new_entry:
		journal_page.add_entry(new_entry)

func _get_state_change_entry(_old_state, _new_state):
	return null

func _get_title():
	return quest.title
