extends Spatial


export(Achievement.achievement) var achievement_to_grant;


func player_entered():
	Runtime.Game.Achievement.unlock(achievement_to_grant)


func _on_Area_body_entered(body):
	var rp = Util.get_root_parent(body)
	if  rp == null:               return
	# TODO: One word. PlayerExpert.
	if !rp.is_in_group("player"): return
	player_entered()
