extends "res://quest/QuestJournal.gd"

func _init(quest).(quest): pass

#var journal = {
#	q.S_NONE:                "", # Same as no key.
#	q.S_MATILDA_FIRST:       "I talked to %s. %s seems unhappy."                        % [q.txt_matilda, q.txt_matilda_she],
#	q.S_MATILDA_TALK_TO_JIM: "I told %s I would talk it out with %s."                   % [q.txt_matilda_her, q.txt_jim],
#	q.S_JIM_FIRST:           "I talked to %s. %s's the local smith."                    % [q.txt_jim, q.txt_jim_he],
#	q.S_JIM_TALK_TO_MATILDA: "%s wants me to check up with %s, %s %s."                  % [q.txt_jim_he, q.txt_matilda, q.txt_jim_he, q.txt_matilda_wife],
#	q.S_MATILDA_BREAKUP:     "If %s marriage is making %s unhappy, %s should break up." % [q.txt_matilda_her, q.txt_matilda_her, q.txt_matilda_she],
#	q.S_MATILDA_UNLIKEABLE:  "I told %s %s is not a likeable person."                   % [q.txt_matilda, q.txt_matilda_she],
#}
