extends "res://dialogtree/DialogTree.gd"

var q = preload("../Quest.gd").new()

enum {
	S_DEFAULT
	S_WHY
	S_BREAKUP
	S_WHY_ASSHOLE
	S_UNLIKEABLE
	S_TALK_TO_JIM
	
	S_TALK_TO_JIM_AFTER
}

# TODO: Improve transitive states that don't make sense when resuming conversation.

var state = S_DEFAULT
var queue = {
	S_DEFAULT: [
		"I swear, I'm going to go insane in this place.",
		{
			"Why?": S_WHY,
		},
	],
	S_WHY: [
		"I mean, look at this. There are only bunnies to eat, and no procedural generation whatsoever.",
		"Also, my %s %s is an asshole." % [q.txt_jim_husband, q.txt_jim],
		"This is awful, I wish I was somewhere else.",
		{
			"Sounds awful. You should break up.":             S_BREAKUP,
			"Why do you say %s is an asshole?" % [q.txt_jim]: S_WHY_ASSHOLE,
		},
	],
	
	S_BREAKUP:     [ "Great idea. I think I will do it." ], # TODO: Tell Jim # TODO: Run away?
	S_WHY_ASSHOLE: [
		"%s spends all of %s time in the forge, coming home really late."           % [q.txt_jim_he, q.txt_jim_his],
		"Lately %s's been really secretive too. I don't think %s likes me anymore." % [q.txt_jim_he, q.txt_jim_he],
		{
			"I can see why %s wouldn't like someone like you." % [q.txt_jim_he]: S_UNLIKEABLE,
			"I'll talk to %s."                                 % [q.txt_jim]:    S_TALK_TO_JIM,
		},
	],
	S_UNLIKEABLE:  [ "WOW. Rude! I can't believe you just said that!" ], # TODO: Aggression
	S_TALK_TO_JIM: [ "I don't see how that would help, but okay.", S_TALK_TO_JIM_AFTER ],
	
	S_TALK_TO_JIM_AFTER: [ "So, you're gonna talk to %s or what?" % [q.txt_jim_him] ], # TODO: I've changed my mind
}
