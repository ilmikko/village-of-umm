extends "res://quest/Quest.gd"

# TODO: Allow Jim to have multiple professions (needs to be crafty though for payoff)

enum {
	# First states
	S_MATILDA_FIRST      # Matilda was talked to first.
	S_JIM_FIRST          # Jim was talked to first.
	
	# Second states
	S_MATILDA_TALK_TO_JIM # Told Matilda we would talk to Jim.
	S_JIM_TALK_TO_MATILDA # Agreed that we would check up with Matilda.
	
	# End states
	S_MATILDA_BREAKUP    # Matilda was told to break up.
	S_MATILDA_UNLIKEABLE # Matilda was told to buzz off.
}

# TODO: "him/his/he/she/her/hers" should move to general lingua library.

var txt_jim         = "Jim"
var txt_jim_husband = "husband"
var txt_jim_him     = "him"
var txt_jim_his     = "his"
var txt_jim_he      = "he"

var txt_matilda      = "Matilda"
var txt_matilda_wife = "wife"
var txt_matilda_her  = "her"
var txt_matilda_hers = "hers"
var txt_matilda_she  = "she"

func _init():
	title = "%s and %s" % [txt_matilda, txt_jim]
	quest_journal = preload("res://quest/live/matildaandjim/QuestJournal.gd").new(self)
	._init()
