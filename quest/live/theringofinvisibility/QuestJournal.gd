extends "res://quest/QuestJournal.gd"

var state_change_entries setget ,_get_state_change_entries

func _init(quest).(quest): pass

func _get_state_change_entries():
	if state_change_entries != null:
		return state_change_entries

	state_change_entries = 	{
		quest.S_NONE: {
			quest.S_AGREED_TO_HELP: "I found a little boy who was crying. After asking him why, he revealed he lost a magic ring. It's probably just a game he's playing, but nevertheless I agreed to help him"
		},
		quest.S_AGREED_TO_HELP: {
			 quest.S_FOUND_RING: "I found the ring. Just looks like a normal ring to me though. I should take it to the kid"
		},
		quest.S_FOUND_RING: {
			quest.S_RING_RETURNED: "Ugh that was pointless. He didn't even want it"
		}
	}
	
	return state_change_entries

func _get_state_change_entry(old_state, new_state):
	if old_state == null:
		old_state = quest.S_NONE
	return _get_state_change_entries()[old_state][new_state]
