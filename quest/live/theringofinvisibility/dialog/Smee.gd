extends Node


enum {
	S_DEFAULT
	S_EXPLAIN_RING_GONE
	S_GENDER_STEREOTYPES
	S_FIND_THE_RING
	S_EXPLAIN_DAY
	S_MAD
	S_IT_IS_OKAY
	S_RING_FOUND
	S_COMPLETED
	S_AGREED_TO_HELP
}

var state = S_DEFAULT setget _set_state
func _set_state(new_state):
	state = new_state

	if state == S_AGREED_TO_HELP:
		var quest_id = Quests.THE_RING_OF_INVISIBILITY
		Runtime.Game.Quest.start(quest_id)
		Runtime.Game.Quest.progress(quest_id, Runtime.Game.Quest.get_quest(quest_id).S_AGREED_TO_HELP)

var queue = {
	S_DEFAULT: [
		"*crying* It... it... It's gone!",
		"*crying* They stole it from us!",
		{
			"What's gone, kid": S_EXPLAIN_RING_GONE,
			"Man up": S_GENDER_STEREOTYPES
		},
	],
	
	S_EXPLAIN_RING_GONE: [
		"The ring! The Ring of Invisibility of course!",
		"I have a magic ring that makes me invisible and someone stole it",
		"I know it was Saur, he's always after my precious ring!",
		{
			"I'll find it for you": S_FIND_THE_RING,
			"What have you done today?": S_EXPLAIN_DAY,
		}
	],
	
	S_EXPLAIN_DAY: [
		"Oh so much, I went to my uncle, you know him! He's the best carpenter in Umm",
		"Then I went chasing bunnies, but I got very muddy so had to clean up",
		"I then went for a swim in the pond so my mummy wont complain that I'm dirty",
		{
			"I'll find the ring for you": S_FIND_THE_RING
		}
	],
	
	S_FIND_THE_RING: [
		"Thank you, thank you, thank you.",
		S_AGREED_TO_HELP
	],
	
	S_GENDER_STEREOTYPES: [
		"Strengh and weakness have nothing to do with gender!",
		"Stop telling boys we have to 'Man up', you're so gross.",
		"It's sexist too! Women can be strong as well, it's not just a mans trait",
		"Because of you, men are afraid to show their feelings and be honest",
		"You're the reason the world is so messed up!",
		{
			"Fucking snow flake": S_MAD,
			"You are right, what was I thinking": S_IT_IS_OKAY
		}
	],
	
	S_MAD: [
		"Ugh, I don't have time for this, I hate you!"
	],
	
	S_IT_IS_OKAY: [
		"That's okay. We all make mistakes. It's not you're fault the patriarchy taught you such silly things"
	],
	
	S_RING_FOUND: [
		"You found it! Now drop it on the floor, I'm bored of this Lord of The Rings rip off game I was playing",
		"Magic isn't real",
		S_COMPLETED
	],
	
	S_COMPLETED: [
		"Thanks again for finding the ring"
	],
	
	S_AGREED_TO_HELP: [
		"Have you found it yet?"
	]
}

func start(person_speaking_with):
	var held_item = person_speaking_with.Actor.HolderHandler.held_object()

	if held_item != null && held_item.name == "ring_of_invisibility" && state == S_AGREED_TO_HELP:
		state = S_RING_FOUND
		var quest = Runtime.Game.Quest.get_quest(Quests.THE_RING_OF_INVISIBILITY)
		quest.state = quest.S_RING_RETURNED
