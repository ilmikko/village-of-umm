extends "res://quest/Quest.gd"

enum {
	S_NONE
	S_AGREED_TO_HELP
	S_FOUND_RING
	S_RING_RETURNED
}

func _init():
	title = "The Ring Of Invisibility"
	quest_journal = preload("res://quest/live/theringofinvisibility/QuestJournal.gd").new(self)
	var player = Runtime.Game.PlayerController
	player.Actor.HolderHandler.connect("object_held", self, "_player_holding_object")

func _player_holding_object(object):

	if object == null || object.name != "ring_of_invisibility" || state != S_AGREED_TO_HELP:
		return

	state = S_FOUND_RING
	quest_journal.state = S_FOUND_RING
