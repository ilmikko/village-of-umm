extends Node

var quest_journal
var state setget _set_state
var title
var title_sub = ""

func start():
	quest_journal.add_to_journal()

func _set_state(new_state):
	quest_journal.state = new_state
	state = new_state
