# Village of Umm

It's a village, in the world of Umm.

## TODO

* Herbarium
* Deprecate PlayerController
* Have a single camera for PlayerController (instead of 3)
* Every Actor should have a "camera attachment point"

## Future Mechanics

* Ailments: A system for getting sick and recovering
* Farming
* Hunting
* Gathering
* Cooking/brewing
* Gathering water
* Temperature system
* Stockpiling
* Making fires (keeping wood piles, making fire place, managing heat and light)
* Mini games (cards, dice)
* Time matters (if you have a task, you can run out of time to finish it, so prioritise)
* Cleaning (e.g. dirt can affect mood)
* Breeding (creatures have genes that can be passed on)
* Climbing rocks buildings etc.
* Oceans
* Sailing
* Fishing
* Islands in the oceans

## Ideas

* The inn of culinary marvels (weird recipes, quests for you to gather the ingredients)
* Hermits hut
* Village idiot
* Rival village
* Strange temple you are told to avoid
* The luckiest man in Umm
* Castle
* Visitors from a far away land
* Caravans (travelling salesman etc)
* Barbarians pillaging
* Roads
* Rivers
* Carpenter routine, gathering wood, going to the pond and working at his house

## Existing mechanics

* Vitals: Hunger, thirst
* Journal
* Save/Load
