extends "res://actor/common/Task.gd"

func _init(brain).(brain): pass


var altitude_sensor


var base_hover = 0.1
var hover_correction_intensity = 0.1


func tick():
	var current_altitude = altitude_sensor.altitude()
	var target_altitude = 3.0
	
	var d = (target_altitude - current_altitude);
	driver.intent_hover = base_hover + d*hover_correction_intensity;


func _started():
	altitude_sensor = brain.get_node("AltitudeSensor")
