tool
extends Spatial

export(float, 0.0, 1.0) var life = 0.0 setget _set_life
func _set_life(_life):
	life = _life
	rebuild()

export(float, 0.0, 0.1) var buzz_intensity = 0.001 setget _set_buzz_intensity
func _set_buzz_intensity(_buzz_intensity):
	buzz_intensity = _buzz_intensity
	rebuild()

export(float, 0.0, 1.0) var breathing_intensity = 0.001 setget _set_breathing_intensity
func _set_breathing_intensity(_breathing_intensity):
	breathing_intensity = _breathing_intensity
	rebuild()

var mat

func rebuild():
	if !mat:
		mat = $MeshInstance.get_surface_material(0)
	mat.set_shader_param("buzz_intensity", life * buzz_intensity)
	mat.set_shader_param("breathing_intensity", life * breathing_intensity)
