shader_type spatial;

uniform float breathing_intensity : hint_range(0.0, 0.01) = 0.001;
uniform float buzz_intensity : hint_range(0.0, 0.01) = 0.001;

const float PI = 3.1415926535897;
const float TAU = 2. * PI;

vec3 buzz(float t) {
	return vec3(sin(t*100.0), cos(t*110.0), 0.0);
}

void vertex() {
	// Some lively motion.
	VERTEX += vec3(0.0, 0.0, sin(TIME*7.5)) * breathing_intensity;
	// Buzzing motion.
	VERTEX += buzz(TIME) * buzz_intensity;
}

vec3 stripes(vec3 xyz) {
	float s = cos(xyz.z*60.0 + 0.6) * 0.5 + 0.5;
	return mix(vec3(0.0), vec3(1.0, 1.0, 0.0), s*s*s);
}

vec3 eyes(vec3 col, vec3 xyz, float t) {
	vec3 eye_offset = vec3( 0.03, -0.021, 0.105);
	vec2 eye_detail_big   = vec2(-0.006) + vec2(sin(t*40.1), cos(t*40.0))*0.02*buzz_intensity;
	vec2 eye_detail_small = vec2( 0.008) - vec2(sin(t*40.1), cos(t*40.0))*0.012*buzz_intensity;
	
	// Eyes
	vec3 left  = xyz + eye_offset;
	col  *= 1.0 - smoothstep(0.975, 0.985,  1.0-length(left));
	float le  = smoothstep(0.998, 0.999, 1.0-length(left + vec3(eye_detail_small,  0.003))) * 0.8;
	      le += smoothstep(0.991, 0.992, 1.0-length(left + vec3(eye_detail_big,   -0.002))) * 0.5;
	col += le;
	
	vec3 right = xyz + eye_offset * vec3(-1.0, 1.0, 1.0);
	col  *= 1.0 - smoothstep(0.975, 0.985,  1.0-length(right));
	float re  = smoothstep(0.998, 0.999, 1.0-length(right + vec3(eye_detail_small, 0.008))) * 0.8;
	      re += smoothstep(0.991, 0.992, 1.0-length(right + vec3(eye_detail_big,  -0.002))) * 0.5;
	col += re;
	
	return col;
}

vec3 mouth(vec3 uvw, float t) {
	// Mouth
	vec3 mouth_offset = vec3(0.0, 0.01, 0.12);
	float smoot = 0.004;
	float rad = 0.99;
	float m = step(0.5, atan(uvw.y + mouth_offset.y, uvw.x) / TAU + 0.5);
	float d = 1.0;
	d -= smoothstep(rad-smoot, rad, 1.0-length(uvw + mouth_offset));
	d += smoothstep(rad, rad+smoot, 1.0-length(uvw + mouth_offset));
	return vec3(max(m, d));
}

void fragment() {
	vec3 uvw = VERTEX.xyz;
	
	uvw = (CAMERA_MATRIX         * vec4(uvw, 1.0)).xyz;
	uvw = (inverse(WORLD_MATRIX) * vec4(uvw, 1.0)).xyz;
	
	vec3 col = vec3(0.0);
	
	col += stripes(uvw);
	col  = eyes(col, uvw, TIME);
	col *= mouth(uvw, TIME);
	
	ALBEDO = col;
}