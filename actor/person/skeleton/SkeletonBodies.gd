tool
extends Spatial


onready var bodies = [
	$PersonBody/Upper/PersonHead,
	$PersonBody/Upper/UpperBody,
	$PersonBody/Upper/ArmLeft/PersonArm/Rotation/Arm,
	$PersonBody/Upper/ArmLeft/PersonArm/Rotation/Rotation/Forearm,
	$PersonBody/Upper/ArmRight/PersonArm/Rotation/Arm,
	$PersonBody/Upper/ArmRight/PersonArm/Rotation/Rotation/Forearm,
	$PersonBody/Lower/PersonLegLeft/PersonLeg/ThighJoint/Thigh,
	$PersonBody/Lower/PersonLegLeft/PersonLeg/ThighJoint/LegJoint,
	$PersonBody/Lower/PersonLegRight/PersonLeg/ThighJoint/Thigh,
	$PersonBody/Lower/PersonLegRight/PersonLeg/ThighJoint/LegJoint,
]


func unfreeze():
	for body in bodies:
		body.mode = RigidBody.MODE_RIGID


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_skin()
	rebuild_variants()


func rebuild_skin():
	var skin_material = SpatialMaterial.new()
	skin_material.albedo_color = skin_color
	
	if !is_inside_tree(): return
	$PersonBody/Upper/PersonHead.skin_material = skin_material
	$PersonBody/Upper/ArmRight/PersonArm.skin_material = skin_material
	$PersonBody/Upper/ArmLeft/PersonArm.skin_material = skin_material
	$PersonBody/Lower/PersonLegLeft/PersonLeg.skin_material = skin_material
	$PersonBody/Lower/PersonLegRight/PersonLeg.skin_material = skin_material


func rebuild_variants():
	if !is_inside_tree(): return
	
	var rng = RandomNumberGenerator.new()
	rng.seed = variant
	
	$PersonBody/Upper/PersonHead.variant               = rng.randi()
	$PersonBody/Upper/UpperBody.variant                = rng.randi()
	
	var arm_variant = rng.randi()
	$PersonBody/Upper/ArmLeft/PersonArm.variant        = arm_variant
	$PersonBody/Upper/ArmRight/PersonArm.variant       = arm_variant
	
	var leg_variant = rng.randi()
	$PersonBody/Lower/PersonLegLeft/PersonLeg.variant  = leg_variant
	$PersonBody/Lower/PersonLegRight/PersonLeg.variant = leg_variant


func remove_root_parents():
	for body in bodies:
		body.remove_from_group('has_root_parent')


func _ready():
	rebuild()


export(Color) var skin_color setget _set_skin_color
func _set_skin_color(c):
	if skin_color == c: return
	skin_color = c
	rebuild_skin()


export(int, 0, 9999) var variant = 0 setget _set_variant
func _set_variant(v):
	if variant == v: return
	variant = v
	rebuild_variants()
