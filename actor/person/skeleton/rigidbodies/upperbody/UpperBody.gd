tool
extends Spatial


const AVAILABLE_VARIANTS = 2


export(int, 0, 1e9) var variant = 0 setget _set_variant
func _set_variant(_variant):
	variant = _variant
	rebuild()


export(Material) var skin_material setget _set_skin_material
func _set_skin_material(_skin_material):
	skin_material = _skin_material
	rebuild()


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_variants()


func rebuild_variants():
	Util.remove_children($Variants)
	var inst = load("res://actor/person/skeleton/rigidbodies/upperbody/variant/UpperBodyVariant%d.tscn" % (variant % AVAILABLE_VARIANTS)).instance()
	$Variants.add_child(inst)
