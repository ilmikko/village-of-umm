tool
extends Spatial


const AVAILABLE_VARIANTS = 3


var CROUCH_INTENSITY_THIGH = 0.55
var CROUCH_INTENSITY_LEG   = 0.7
var CROUCH_INTENSITY_FOOT  = 0.15

var KICK_INTENSITY_THIGH = 0.65
var KICK_INTENSITY_LEG   = 2.0
var KICK_INTENSITY_FOOT  = 0.025

var JUMP_INTENSITY_THIGH = 0.46
var JUMP_INTENSITY_LEG   = 0.8
var JUMP_INTENSITY_FOOT  = 0.2

# The threshold where into the kicking animation the kick shape gets enabled.
var KICK_SHAPE_ENABLE_THRESHOLD = 0.5


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_anims()
	rebuild_skin()
	rebuild_variants()


func rebuild_anims():
	var base_crouching = Vector3(0, -direction, 0) * anim_crouching * PI;
	base_crouching    += Vector3(1, 0, 0) * anim_crouching * PI;
	var base_jumping   = Vector3(1, 0, 0) * sin(anim_jumping * PI) * PI;
	var base_kicking   = Vector3(1, 0, 0) * TAU/4 * -sin(anim_kicking * anim_kicking * TAU);
	
	$ThighJoint.rotation                    = (
		base_crouching * CROUCH_INTENSITY_THIGH
		+ base_jumping * JUMP_INTENSITY_THIGH
		+ base_kicking * KICK_INTENSITY_THIGH
	)
	$ThighJoint/LegJoint.rotation           = (
		base_crouching * -CROUCH_INTENSITY_LEG
		+ base_jumping * -JUMP_INTENSITY_LEG
		+ base_kicking * KICK_INTENSITY_LEG * (1.0 - anim_kicking)
	)
	$ThighJoint/LegJoint/FootJoint.rotation = (
		base_crouching * CROUCH_INTENSITY_FOOT
		+ base_jumping * -JUMP_INTENSITY_FOOT
		+ base_kicking * KICK_INTENSITY_FOOT
	)

	rebuild_anims_kickshape()


func rebuild_anims_kickshape():
	var kickshape = get_node_or_null("ThighJoint/LegJoint/FootJoint/KinematicBody/CollisionShape")
	kickshape.disabled = anim_kicking < KICK_SHAPE_ENABLE_THRESHOLD

func rebuild_skin():
	$ThighJoint/LegJoint/Leg/MeshInstance3.material_override = skin_material


func rebuild_variants():
	Util.remove_children($ThighJoint/Thigh/Variants)
	var inst = load("res://actor/person/skeleton/rigidbodies/leg/variant/LegVariant%d.tscn" % (variant % AVAILABLE_VARIANTS)).instance()
	$ThighJoint/Thigh/Variants.add_child(inst)


export(float, 0.0, 1.0) var anim_crouching = 0.0 setget _set_anim_crouching
func _set_anim_crouching(a):
	anim_crouching = a
	rebuild()


export(float, 0.0, 1.0) var anim_jumping = 0.0 setget _set_anim_jumping
func _set_anim_jumping(a):
	anim_jumping = a
	rebuild()


export(float, 0.0, 1.0) var anim_kicking = 0.0 setget _set_anim_kicking
func _set_anim_kicking(a):
	anim_kicking = a
	rebuild()


export(float, -1.0, 1.0) var direction = 0.0 setget _set_direction
func _set_direction(_direction):
	direction = _direction
	rebuild()


export(Material) var skin_material setget _set_skin_material
func _set_skin_material(_skin_material):
	skin_material = _skin_material
	rebuild()


export(int, 0, 1e9) var variant = 0 setget _set_variant
func _set_variant(_variant):
	variant = _variant
	rebuild()
