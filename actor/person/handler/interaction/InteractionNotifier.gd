tool
extends Spatial

export(NodePath) var holder_handler_path

var NodeMatcher = preload("res://script/class/NodeMatcher.gd")

var interaction_handlers = {
	"primary": [],
	"secondary": [],
}

func add_interaction(interaction_type, handler, interacted_node_matcher = null, held_node_matcher = null):
	if interacted_node_matcher == null: interacted_node_matcher = NodeMatcher.new()
	if held_node_matcher == null: held_node_matcher = NodeMatcher.new()

	interaction_handlers[interaction_type].append({
		"handler": handler,
		"interacted_node_matcher": interacted_node_matcher,
		"held_node_matcher": held_node_matcher
	})


func interact(actor, interactor, interaction_type) -> bool:
	var object = interactor.ray_object()
	var interaction_occurred = _interact_with_object_if_interactable(actor, object)

	for interaction_handler in interaction_handlers[interaction_type]:
		if interaction_handler["interacted_node_matcher"].is_matching(object) and interaction_handler["held_node_matcher"].is_matching(_get_held_object()):
			interaction_handler["handler"].interaction_triggered(actor, object, interaction_type)
			interaction_occurred = true
	return interaction_occurred


func _interact_with_object_if_interactable(actor, object):
	var interacted_with_object = _should_interacted_object_be_called(object)
	if interacted_with_object:
		object.handle_interaction_by_actor(actor)
	
	return interacted_with_object


func _should_interacted_object_be_called(interacted_object):
	return (
		interacted_object != null &&
		interacted_object.has_method("handle_interaction_by_actor")
	)


func _get_held_object():
	return get_node(holder_handler_path).held_object()


export(NodePath) var person


onready var Person = get_node(person)
