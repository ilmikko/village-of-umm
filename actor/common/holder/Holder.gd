extends Spatial


var held_object
signal held_object_signal(held_object)


var held_object_parent
var held_object_collision_shapes


func drop_object():
	if held_object == null: return
	
	var old_transform = held_object.global_transform
	remove_child(held_object)
	
	if held_object_parent == null || !is_instance_valid(held_object_parent):
		held_object_parent = get_tree().get_root()
	held_object_parent.add_child(held_object)

	for collision in held_object_collision_shapes:
		if collision.is_in_group('keep_collision'): continue
		# held_object.add_child(collision)
		collision.disabled = false
	
	held_object.global_transform = old_transform
	held_object.mode = RigidBody.MODE_RIGID
	
	held_object = null
	emit_signal("held_object_signal", null)



func hold_object(node):
	if held_object != null: return
	
	held_object = node
	emit_signal("held_object_signal", held_object)
	held_object_parent = held_object.get_parent()
	
	node.mode = RigidBody.MODE_STATIC
	if node.get_parent():
		node.get_parent().remove_child(node)
	
	# If the node has a HolderOrigin, use that origin as the transform.
	var new_transform = Transform()
	if node.get_node_or_null("HolderOrigin") != null:
		new_transform = node.get_node("HolderOrigin").transform.inverse()

	add_child(node)
	node.transform = new_transform
	_add_held_object_collision_shapes()


func _add_held_object_collision_shapes():
	held_object_collision_shapes = Util.get_collisions(held_object)

	for collision in held_object_collision_shapes:
		if collision.is_in_group('keep_collision'): continue
		collision.disabled = true
		# collision.get_parent().remove_child(collision)


var primed setget _set_primed
func _set_primed(p):
	if primed == p: return
	primed = p
	if held_object == null || !is_instance_valid(held_object): return
	if !held_object.is_in_group('primeable'): return
	held_object.primed = p
