extends Node

var brain
var driver
var child_task = null
var child_task_running_time = Duration.new()

func _init(new_brain):
	brain = new_brain
	driver = brain.driver

func process(delta):
	if child_task:
		child_task.process(delta)

func start():
	_started()

func tick():
	if child_task:
		child_task.tick()

func finish():
	_finished()

func _started():
	pass

func _finished():
	pass

func _start_child_task(new_child_task):
	if child_task == null:
		Runtime.Game.Time.connect("at_least_1_0_seconds_passed", self, "_update_child_task_running_time")
	else:
		child_task.finish()
	child_task = new_child_task.new(brain)
	child_task_running_time = Duration.new()

func _update_child_task_running_time(delta):
	child_task_running_time.add_seconds(delta)
