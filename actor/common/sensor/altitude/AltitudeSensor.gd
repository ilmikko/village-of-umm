extends Spatial


func altitude():
	return abs(global_transform.origin.y - $RayCast.get_collision_point().y)
