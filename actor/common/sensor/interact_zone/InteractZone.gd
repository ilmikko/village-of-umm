extends Area

func get_interactable_items(): 
	return get_overlapping_bodies() + get_overlapping_areas()
