tool
extends Spatial

signal ground_changed(current_ground)
signal ground_normal_changed(current_ground_normal)


# How much ahead/behind the ground RayCast should the RayCastNormal be on the
# Z axis.
export(float, -5.0, 5.0) var normal_offset = 0.0 setget _set_normal_offset
func _set_normal_offset(_normal_offset):
	normal_offset = _normal_offset
	rebuild()


# How long the raycasts are. Affects the collision box as well.
export(float, 0.1, 5.0) var ray_length = 1.0 setget _set_ray_length
func _set_ray_length(_ray_length):
	ray_length = _ray_length
	rebuild()


var current_ground        = null
var current_ground_normal = null


func rebuild():
	if !is_inside_tree(): return
	
	$RayCast.cast_to       = Vector3(0, -ray_length, 0)
	$RayCastNormal.cast_to = Vector3(0, -ray_length, 0)
	
	$RayCastNormal.transform.origin.z = -normal_offset
	
	var box = BoxShape.new()
	# Extents are half of the ray length, not to be confused with scale.
	box.extents = Vector3(0.1, ray_length / 2.0, 0.1)
	$Area/CollisionShape.shape = box
	$Area/CollisionShape.transform.origin.y = -ray_length/2.0


func check_ray_collision():
	var c = $RayCast.get_collider()
	if  c == current_ground: return
	
	current_ground = c
	
	Debug.text(self, "GroundSensor current ground:", current_ground)
	emit_signal("ground_changed", current_ground)


func update_ray_normal():
	var n = $RayCastNormal.get_collision_normal()
	if  n == current_ground_normal: return
	
	current_ground_normal = n
	
	Debug.text(self, "GroundSensor current ground normal:", current_ground_normal)
	emit_signal("ground_normal_changed", current_ground_normal)


func _on_Area_body_entered(_body):
	check_ray_collision()


func _on_Area_body_exited(_body):
	check_ray_collision()


func _on_Slow_timeout():
	check_ray_collision()


func _on_Fast_timeout():
	update_ray_normal()
