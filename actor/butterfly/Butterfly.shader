shader_type spatial;

render_mode cull_disabled;

uniform float fly_intensity  : hint_range(0.0, 1.0) = 1.0;
uniform vec4  wing_color : hint_color = vec4(1.0, 0.0, 1.0, 1.0);

void vertex() {
	VERTEX.y += abs(VERTEX.x * 0.5) * (cos(TIME*30.0) * 1.5) * fly_intensity;
}

vec2 rand2(vec2 p) {
	vec2 a = vec2(42189.0, -4902.045125);
	vec2 b = vec2(0.3424, -1400326.3);
	vec2 some = vec2(
		dot(p, a),
		dot(p, b)
	);
	return fract(sin(some));
}

float worley2(vec2 p) {
	float dist = 1.0;
	vec2 i_p = floor(p);
	vec2 f_p = fract(p);
	for (int y=-1;y<=1;y++) {
		for (int x=-1;x<=1;x++) {
			vec2 xy = vec2(float(x), float(y));
			vec2 rp = rand2(i_p + xy);
			vec2 diff = xy + rp - f_p;
			dist = min(dist, length(diff));
		}
	}
	return dist;
}

void fragment() {
	NORMAL *= FRONT_FACING ? 1.0 : -1.0;
	
	vec2 uv = UV * 2.0 - 1.0;
	
	float d = 0.0;
	
	vec2 center_size     = vec2(0.35, 0.90);
	vec2 center_pos      = vec2(0.00, 0.00);
	vec2 front_wing_size = vec2(0.45, 0.75);
	vec2 front_wing_pos  = vec2(0.40, 0.20);
	vec2 back_wing_size  = vec2(0.65, 0.85);
	vec2 back_wing_pos   = vec2(0.20, 0.35);
    
	d += smoothstep(    center_size.x,     center_size.y, 1.0 - length(uv + center_pos));
	d += smoothstep(front_wing_size.x, front_wing_size.y, 1.0 - length(uv + front_wing_pos * vec2( 1.0, 1.0)));
	d += smoothstep(front_wing_size.x, front_wing_size.y, 1.0 - length(uv + front_wing_pos * vec2(-1.0, 1.0)));
	d += smoothstep( back_wing_size.x,  back_wing_size.y, 1.0 - length(uv + back_wing_pos  * vec2( 1.0,-1.0)));
	d += smoothstep( back_wing_size.x,  back_wing_size.y, 1.0 - length(uv + back_wing_pos  * vec2(-1.0,-1.0)));
	
	// Colouring
	vec3 uvw = VERTEX.xyz;
	
	uvw = (CAMERA_MATRIX         * vec4(uvw, 1.0)).xyz;
	uvw = (inverse(WORLD_MATRIX) * vec4(uvw, 1.0)).xyz;
	
	vec2 wing_uv = uvw.xz;
	
	float c = smoothstep(0.1, 1.0, d * d);
	
	c *= 1.0 - step(0.99, 1.0 - length(uvw * vec3(1.5, 1.5, 0.11)));
	
	float w = abs(worley2(wing_uv*20.0) - worley2(c*wing_uv*28.0));
	
	w *= w * 3.0;
	
	vec3 col = wing_color.rgb * w;
	ALBEDO = c * col;
	
	d = step(0.1, d) - c*(1.0-wing_color.a);
	ALPHA  = d;
}