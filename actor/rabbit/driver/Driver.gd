tool
extends RigidBody

# The maximum amount of angular force this character can emit.
var ANGULAR_ABILITY = 0.05
# Base angular damping amount.
var ANGULAR_DAMPING = 0.01
# Base gravity intensity. (TODO: Abstract this away)
var GRAVITY_INTENSITY = 0.03
# The maximum amount of linear force this character can emit.
var LINEAR_ABILITY = 0.2
# Base linear damping amount.
var LINEAR_DAMPING = 0.02


var intent_move setget _set_intent_move
func _set_intent_move(m):
	intent_move = m
	linear_intent = Vector3(intent_move.x, 0, intent_move.y)


# How much rotation the actor wants to apply.
var angular_intent = Vector3(0, 0, 0)
# How much velocity the actor wants to apply.
var linear_intent = Vector3(0, 0, 0)


# TODO: Should be in brain, not in the driver
var target_rotation
export(Vector3) var target_location


# TODO: Should be in brain, not in the driver
func move_towards(new_target_location):
	target_location = new_target_location


func _integrate_forces(state):
	_integrate_forces_gravity(state)
	_integrate_forces_angular(state)
	_integrate_forces_angular_damping(state)
	_integrate_forces_linear(state)
	_integrate_forces_linear_damping(state)


func calculate_angular_intent():
	if not target_location:
		return
	target_rotation = global_transform.looking_at(target_location, Vector3.UP).basis
	var intent_y = global_transform.basis.x.cross(target_rotation.x).dot(Vector3.UP)
	angular_intent = Geo.Vector3(0, intent_y, 0)


func calculate_linear_intent():
	if not target_location or not target_rotation or target_rotation.x.dot(global_transform.basis.x) < 0.90:
		return

	var direction_facing = -global_transform.basis.z
	# TODO: Rethink this line
	direction_facing *= global_transform.origin.distance_to(target_location) * 0.1
	linear_intent = direction_facing


func _integrate_forces_angular(state):
	calculate_angular_intent()
	if angular_intent.length() > ANGULAR_ABILITY:
		angular_intent = angular_intent.normalized() * ANGULAR_ABILITY
	state.apply_torque_impulse(angular_intent)


func _integrate_forces_angular_damping(state):
	state.apply_torque_impulse(-state.angular_velocity * ANGULAR_DAMPING)


func _integrate_forces_gravity(state):
	state.apply_central_impulse(Vector3.DOWN * GRAVITY_INTENSITY)


func _integrate_forces_linear(state):
	calculate_linear_intent()
	if linear_intent.length() > LINEAR_ABILITY:
		linear_intent = linear_intent.normalized() * LINEAR_ABILITY
	state.apply_central_impulse(linear_intent)


func _integrate_forces_linear_damping(state):
	state.apply_central_impulse(-state.linear_velocity * LINEAR_DAMPING)
