extends "res://actor/common/brain/Brain.gd"

var wait_script = preload("res://actor/rabbit/tasks/Wait.gd")
var flee_script = preload("res://actor/rabbit/tasks/Flee.gd")

const predators = ["person"]


func get_foods_within_range():
	var items = $InteractZone.get_interactable_items()
	var foods = []
	for item in items:
		if item.is_in_group("food"):
			foods.append(item)

	return foods

func _on_FieldOfView_item_detected(item):
	._on_FieldOfView_item_detected(item)
	look_out_for_predators(item)

func look_out_for_predators(item):
	if !(current_task is Flee) && is_predator(item):
		set_task(flee_script)
		current_task.predator = item

func task_completed(_is_successful):
	set_task(wait_script)

func is_predator(item):
	for predator in predators:
		if item.is_in_group(predator):
			return true
	return false
