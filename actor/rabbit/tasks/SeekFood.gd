extends "res://actor/common/Task.gd"
class_name SeekFood

var Eat = preload("Eat.gd")

var target_location: Vector3
var target_found = false
var target_rotation: Basis
var rotation_lerp:= 0.0
var rotation_speed:= 0.25

func _init(brain).(brain):
	pass

func process(_delta):
	if _is_at_food():
		brain.set_task(Eat)

func tick():
	var closest_food_source = _closest_known_food_source()
	if closest_food_source:
		set_target_location(closest_food_source["location"])

func set_target_location(new_target):
	target_found = true
	target_location = new_target
	brain.set_target_location(target_location)

func _is_at_food():
	return target_found and brain.get_foods_within_range().size() > 0

func _closest_known_food_source():
	var known_food_sources = brain.world_knowledge_base.get_items_knowledges_in_group('food')
	if known_food_sources.size() == 0:
		return null

	var character_origin = brain.global_transform.origin
	var closest_known_food = known_food_sources[0]
	var closest_known_food_distance = character_origin.distance_to(closest_known_food.location)
	
	for known_food_source in known_food_sources:
		var distance_to_food = character_origin.distance_to(known_food_source.location)
		if closest_known_food_distance > distance_to_food:
			closest_known_food = known_food_source
			closest_known_food_distance = distance_to_food
	
	return closest_known_food


