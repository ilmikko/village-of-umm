extends "res://actor/common/Task.gd"

var food: Node
var food_knowledge: ItemKnowledge

func _init(brain).(brain):
	var foods = brain.get_foods_within_range()
	if foods.size() < 1:
		brain.task_completed(false)
		return

	food = foods[0]
	food_knowledge = brain.world_knowledge_base.get_item_knowledge(food)

func tick():
	if !food:
		if food_knowledge:
			brain.world_knowledge_base.remove_knowledge(food_knowledge)
		brain.task_completed(true)
		return

	var _consumed = food.consume(35)
