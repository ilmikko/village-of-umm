extends "res://actor/common/Task.gd"

var time_in_direction = 100
var target_location: Vector3
var facing_angle = 0.0

func _init(character).(character): pass

func tick():
	# TODO: Pass real delta
	var delta = 1
	time_in_direction += delta

	if time_in_direction > rand_range(2, 5):
		time_in_direction = 0
		find_new_target()

func _started():
	pass #tick()

func find_new_target():
	var rand_x = rand_range(-10,10)
	var rand_y = 0
	var rand_z = rand_range(-10,10)
	var random_location = Vector3(rand_x, rand_y, rand_z)
	target_location = random_location + brain.driver.global_transform.origin
	brain.set_target_location(target_location)
