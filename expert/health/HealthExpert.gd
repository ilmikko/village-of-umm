extends Node


# Average amount of health every body has.
var NORMAL_HEALTH = 100.0
# Minimum amount of health every body has.
var MIN_HEALTH = 5.0
# Amount of extra health from volume.
var VOLUME_HEALTH_MULTIPLIER = 1.5


func for(item):
	var health = Runtime.Handler.fetch(item, Runtime.Handler.Health)
	return health.current()


func lose(item, amount):
	var health = Runtime.Handler.fetch(item, Runtime.Handler.Health)
	return health.lose(amount)


func max_health(body) -> float:
	var h = NORMAL_HEALTH
	
	# The bigger the body is, the more health it has.
	# TODO: This shouldn't be based on volume, but mass instead.
	h *= Runtime.Expert.Volume.volume(body) * VOLUME_HEALTH_MULTIPLIER
	
	h += MIN_HEALTH
	
	print("HealthExpert: body %s health calculated %s" % [body.get_path(), h])
	return h
