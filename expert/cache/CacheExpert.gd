extends Spatial

# How much space to give between the far clipping plane and the items to cache.
var CAMERA_CACHE_THRESHOLD_FAR = 64.0


var CACHE_EPSILON = 0.00000001


var cached = {}
var cache_node = Spatial.new()


var to_be_loaded  = []


func cache(scene):
	if is_cached(scene): return
	var path = scene.get_path()
	print("CacheExpert: Caching %s" % path)
	to_be_loaded.append(scene)
	cached[path] = scene


func instance(scene):
	# We need to be visible in front of a camera for the caching to work.
	# Culled objects won't get cached properly.
	var cameras = Runtime.Expert.Camera.get_viewport_cameras()
	if  cameras.size() == 0:
		# We cannot cache right now; add to the queue.
		to_be_loaded.push_front(scene)
		return
	
	if cache_node.get_parent() != cameras[0]:
		cameras[0].add_child(cache_node)
	# Cache items 64 units before the camera cull end.
	cache_node.transform.origin.z = -cameras[0].far + 64.0
	
	var instance = scene.instance()
	# Scale the scene down so we don't see it.
	instance.scale *= CACHE_EPSILON
	cache_node.add_child(instance)
	return instance


func cache_load_next():
	if to_be_loaded.empty(): return
	var scene = to_be_loaded.pop_front()
	instance(scene)


func cache_tick():
	instance_remove_next()
	cache_load_next()


func instance_remove_next():
	Util.remove_children(cache_node)


func is_cached(scene):
	return cached.has(scene.get_path())


func _ready():
	$Timer.start()


func _on_Timer_timeout():
	cache_tick()
