tool
extends Node

# Keep sorted alphabetically
var Breakage         = preload("res://expert/breakage/BreakageExpert.gd").new()
var Cache            = preload("res://expert/cache/CacheExpert.tscn").instance()
var Camera           = preload("res://expert/camera/CameraExpert.gd").new()
var Collision        = preload("res://expert/collision/CollisionExpert.gd").new()
var ConsumableEffect = preload("res://expert/consumableeffect/ConsumableEffectExpert.gd").new()
var Control          = preload("res://expert/control/ControlExpert.gd").new()
var Crane            = preload("res://expert/crane/CraneExpert.gd").new()
var FPS              = preload("res://expert/fps/FPSExpert.gd").new()
var Ghost            = preload("res://expert/ghost/GhostExpert.gd").new()
var Health           = preload("res://expert/health/HealthExpert.gd").new()
var InteractHint     = preload("res://expert/interacthint/InteractHintExpert.gd").new()
var ItemName         = preload("res://expert/itemname/ItemNameExpert.gd").new()
var Loading          = preload("res://expert/loading/LoadingExpert.gd").new()
var LoadingInstance  = preload("res://expert/loading/instance/InstanceLoadingExpert.gd").new()
var LoadingResource  = preload("res://expert/loading/resource/ResourceLoadingExpert.gd").new()
var Material         = preload("res://expert/material/MaterialExpert.gd").new()
var PhysicsBody      = preload("res://expert/physicsbody/PhysicsBodyExpert.gd").new()
var Tracker          = preload("res://expert/tracker/TrackerExpert.gd").new()
var ToolAnimation    = preload("res://expert/toolanimation/ToolAnimationExpert.gd").new()
var ToolHit          = preload("res://expert/toolhit/ToolHitExpert.gd").new()
var Volume           = preload("res://expert/volume/VolumeExpert.gd").new()


func _ready():
	# These are used in other Experts; load them first.
	add_child(Camera)
	add_child(FPS)
	
	add_child(Cache)
	add_child(LoadingInstance)
	add_child(LoadingResource)
	add_child(ToolAnimation)
	add_child(ToolHit)
	add_child(Tracker)
	
	# Keep sorted alphabetically
	add_child(Breakage)
	add_child(Collision)
	add_child(ConsumableEffect)
	add_child(Control)
	add_child(Crane)
	add_child(Ghost)
	add_child(Health)
	add_child(ItemName)
	add_child(InteractHint)
	add_child(Loading)
	add_child(Material)
	add_child(PhysicsBody)
	add_child(Volume)
