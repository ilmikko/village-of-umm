extends Node

const GIB_WOOD_P = preload("res://world/effect/gib/wood/GibWoodParticles.tscn")
const GIB_WOOD_1 = preload("res://world/effect/gib/wood/GibWood1.tscn")
const GIB_WOOD_2 = preload("res://world/effect/gib/wood/GibWood2.tscn")
const GIB_WOOD_3 = preload("res://world/effect/gib/wood/GibWood3.tscn")


# Called on a body if we're fairly sure it will break soon.
func prepare(body):
	if body == null: return
	if body.is_in_group('no_breakage'): return body
	if body is StaticBody: return prepare_staticbody(body)
	if body is RigidBody:  return prepare_rigidbody(body)


func break(body):
	if body == null: return
	if body.is_in_group('no_breakage'): return body
	if body is StaticBody: return break_staticbody(body)
	if body is RigidBody:  return break_rigidbody(body)
	push_warning("BreakageExpert does not know how to break %s" % body)


# StaticBodies can be made RigidBodies with MODE_STATIC to prepare for breakage.
func prepare_staticbody(body) -> RigidBody:
	var new_body = RigidBody.new()
	new_body.mode = RigidBody.MODE_STATIC
	
	# Disable collisions so we can perform the body transform.
	# TODO: Remember if there were collisions already disabled
	for c in Util.get_collisions(body): c.disabled = true
	
	# TODO: Figure out mass for object
	
	# Preserve the scene filename since this is used by experts.
	new_body.filename = Scene.filepath(body)
	
	Util.replace_with_transform_and_children(body, new_body)
	
	# Re-enable collisions on the new body.
	for c in Util.get_collisions(new_body): c.disabled = false
	
	return new_body


func prepare_rigidbody(body) -> RigidBody:
	if body.mode == RigidBody.MODE_STATIC:
		return body
	
	# Prepare by ensuring we have cached the breakage effects.
	Runtime.Expert.Cache.cache(GIB_WOOD_1)
	Runtime.Expert.Cache.cache(GIB_WOOD_2)
	Runtime.Expert.Cache.cache(GIB_WOOD_3)
	Runtime.Expert.Cache.cache(GIB_WOOD_P)
	return body


# RigidBodies can break in multiple ways.
func break_rigidbody(body):
	if body.mode == RigidBody.MODE_STATIC:
		return break_rigidbody_static(body)
	
	if !body.is_in_group('no_breakage_effects'):
		# TODO: Break depending on material
		# TODO: Break into multiple pieces if possible
		# TODO: Custom breakages of objects
		var parent = body.get_parent()
		var effect = GIB_WOOD_P.instance()
		parent.add_child(effect)
		effect.global_transform = body.global_transform
		effect.restart()
		
		# TODO: Random vectors
		
		var w1 = GIB_WOOD_1.instance()
		parent.add_child(w1)
		w1.global_transform = body.global_transform
		w1.apply_central_impulse(Vector3(1.0, 0.0, 0.0))
		
		var w2 = GIB_WOOD_2.instance()
		parent.add_child(w2)
		w2.global_transform = body.global_transform
		w2.apply_central_impulse(Vector3(1.0, 0.0, 0.0))
		
		var w3 = GIB_WOOD_3.instance()
		parent.add_child(w3)
		w3.global_transform = body.global_transform
		w3.apply_central_impulse(Vector3(1.0, 0.0, 0.0))
	
	body.queue_free()


# RigidBodies with MODE_STATIC are simply made rigid.
func break_rigidbody_static(body):
	body.mode = RigidBody.MODE_RIGID


# StaticBodies become RigidBodies.
func break_staticbody(body):
	var rigidbody = prepare_staticbody(body)
	return break_rigidbody_static(rigidbody)


func _ready():
	Runtime.Expert.Cache.cache(GIB_WOOD_1)
	Runtime.Expert.Cache.cache(GIB_WOOD_2)
	Runtime.Expert.Cache.cache(GIB_WOOD_3)
	Runtime.Expert.Cache.cache(GIB_WOOD_P)
