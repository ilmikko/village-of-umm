extends Node


# CraneExpert decides how and where to attach a crane to a node.
func attach(body, controller):
	controller.linked_controller = body
	
	var crane = CRANE.instance()
	crane.attach(controller)
	
	match Scene.filepath(body):
		"res://actor/person/driver/Driver.tscn":
			Util.move_child_under(crane, body.CraneBase)
		_:
			Util.move_child_under(crane, body)


const CRANE = preload("res://player/camera/crane/Crane.tscn")
