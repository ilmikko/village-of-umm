extends Node


func simple_sphere(body):
	var col = CollisionShape.new()
	
	var sphere = SphereShape.new()
	sphere.radius = 0.1
	col.shape = sphere
	return col


func add_simple_sphere(body):
	body.add_child(simple_sphere(body))
