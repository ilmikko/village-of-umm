extends Node

# This node answers questions such as: "Where is the player right now?"

signal player_trackers_signal(_player_trackers)
var    _player_trackers = []


func connect_and_emit_last_signal(signal_name, other, other_method):
	connect(signal_name, other, other_method)
	other.call_deferred(other_method, _player_trackers)


func add_player_tracker(t):
	if t in _player_trackers: return
	_player_trackers.append(t)
	emit_signal("player_trackers_signal", _player_trackers)
	Debug.text(self, "player_trackers", _player_trackers)


func remove_player_tracker(t):
	_player_trackers.erase(t)
	emit_signal("player_trackers_signal", _player_trackers)
	Debug.text(self, "player_trackers", _player_trackers)


func set_player_tracker(t):
	for t in _player_trackers:
		_player_trackers.erase(t)
	_player_trackers = []
	_player_trackers.append(t)
	emit_signal("player_trackers_signal", _player_trackers)
	Debug.text(self, "player_trackers", _player_trackers)


func get_player_trackers():
	return _player_trackers
