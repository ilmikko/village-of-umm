extends Node

const ghost_scene = preload("res://expert/ghost/Ghost.tscn")

func convert_to_ghost(scene):
	var ghost = ghost_scene.instance()
	ghost.scene_packed = scene
	return ghost
