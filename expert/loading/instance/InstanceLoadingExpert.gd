extends Node


func instance(scene):
	var signaler = Node.new()
	signaler.add_user_signal(INSTANCED, [])
	# If this is already instanced, we add it to the "fast track" queue.
	# TODO: Can we do better? call_deferred to send the signal didn't work :(
	if instanced.has(scene.get_path()):
		noop_instance_queue.push_back([scene, signaler])
	else:
		scene_instance_queue.push_back([scene, signaler])
	return signaler


func should_queue(path):
	if instanced.has(path): return false
	if queued.has(path):    return false
	return true


func tick():
	Debug.text(self, "scene_instance_queue", scene_instance_queue)
	
	while noop_instance_queue.size() > 0:
		var item = noop_instance_queue.pop_front()
		var scene      = item[0]
		var signaler   = item[1]
		
		# TODO: Delegate this to the CacheExpert?
		var instance = scene.instance()
		
		signaler.emit_signal(INSTANCED, instance)
	
	while to_remove.size() > 0:
		remove_child(to_remove.pop_front())
	
	if scene_instance_queue.size() == 0: return
	
	# Check if we can instance the scene in chunks.
	# This is only possible if we have generated metadata of the scene.
	var first_item = scene_instance_queue[0]
	var first_item_scene      = first_item[0]
	var first_item_scene_path = first_item_scene.get_path()
	var first_item_meta       = first_item_scene_path.replace("res://", METADATA) + ".gd"
	
	if should_queue(first_item_scene_path):
		# TODO: Don't make a new file every time
		if File.new().file_exists(first_item_meta):
			var scenes = load(first_item_meta).new().scenes
			var scene_paths = []
			for scene in scenes:
				var scene_path = "res://" + scene
				if !should_queue(scene_path): continue
				queued[scene_path] = true
				scene_paths.push_front(scene_path)
			for p in scene_paths:
				scene_instance_queue.push_front([load(p), null])
		else:
			Debug.print(self, "%s does not have InstanceLoadingExpert metadata." % first_item_scene_path)
		
	# Load the next atomic thing in the queue.
	var item = scene_instance_queue.pop_front()
	var scene      = item[0]
	var signaler   = item[1]
	
	var scene_path = scene.get_path()
	
	Debug.text(self, "now instancing", scene_path)
	
	# This here is the expensive operation.
	# TODO: Delegate this to the CacheExpert?
	# TODO: We should see how long this took, and if it didn't take long, use
	# some more of our loading budget
	var instance = Runtime.Expert.Cache.instance(scene)
	
	instanced[scene_path] = true
	
	if signaler == null: return
	
	signaler.emit_signal(INSTANCED, scene.instance())


func _ready():
	var t = Timer.new()
	t.one_shot = false
	t.wait_time = 0.32
	t.connect("timeout", self, "_on_Timer_timeout")
	add_child(t)
	t.start()


var INSTANCED = "instanced"
var METADATA  = "res://meta/scene/"
var scene_instance_queue = []
var noop_instance_queue = []

var instanced = {}
var queued    = {}

var to_remove = []


func _on_Timer_timeout():
	tick()
