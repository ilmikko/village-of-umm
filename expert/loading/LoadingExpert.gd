extends Node

# This Expert combines multiple loading methods for more specific loading
# scenarios.

var started = {}
# TODO: Remove this
func load_and_change(s):
	assert(get_tree().change_scene(s) == OK)


func root_load_start(n):
	Runtime.UI.Loading.show()
	call_deferred("root_load_start_defer", n)


func root_load_start_defer(n):
	scene_load_start(n)


func root_load_end(n):
	scene_load_end(n)
	call_deferred("root_load_end_defer", n)


func root_load_end_defer(_n):
	Runtime.UI.Loading.hide()


func scene_load_start(n):
	var s = OS.get_ticks_msec()
	if (n in started):
		Debug.warn(self, "Node %s already found in scores" % n)
		return
	started[n] = s
	Debug.print(self, "START @ %s" % [s])


func scene_load_end(n):
	var e = OS.get_ticks_msec()
	if  !(n in started):
		Debug.warn(self, "Node %s not found in scores" % n)
		return
	var s = started[n]
	var score = e - s
	# TODO: Save score for the scenes.
	Debug.print(self, "END @ %s; SCORE: %s" % [e, score])
