extends Node

# TODO: This code seems to be a cause of the crashes.

# Implements a ResourceLoader and runs in a separate thread for loading
# resources in the background.
#
#signal resource_loaded(resource)
#
#var resource_load_semaphore = Semaphore.new()
#var resource_load_queue = []
#
#
#func queue(resource):
#	resource_load_queue.append(resource)
#	resource_load_semaphore.post()
#
#
#func tick():
#	resource_load_semaphore.post()
#
#
## _null argument has to be there, otherwise the thread doesn't execute.
#func _resource_loader_thread(_null):
#	var loaders = []
#	while resource_loader_thread_run:
#		resource_load_semaphore.wait()
#
#		Debug.text(self, "resource_load_queue", resource_load_queue)
#
#		while !resource_load_queue.empty():
#			var res = resource_load_queue.pop_front()
#			var loader = ResourceLoader.load_interactive(res)
#			if  loader == null: # Check for errors.
#				Debug.warn(self, "ResourceLoader did not know how to handle %s!" % res)
#				return
#			loaders.append(loader)
#
#		if loaders.size() == 0: continue
#
#		var loader = loaders[0]
#
#		var err = loader.poll()
#		if  err == ERR_FILE_EOF:
#			# Done!
#			var res = loader.get_resource()
#			Debug.print(self, "LOADED %s" % res)
#			emit_signal("resource_loaded",  res)
#			loaders.erase(loader)
#		elif err != OK:
#			Debug.print(self, "ERROR %s" % loader)
#			# Error.
#			loaders.erase(loader)
#
#
#var resource_loader_thread_run = true
#var resource_loader_thread
#func _ready():
#	resource_loader_thread = Thread.new()
#	resource_loader_thread.start(self, "_resource_loader_thread")
#
#	var t = Timer.new()
#	t.one_shot = false
#	t.wait_time = 0.1
#	t.connect("timeout", self, "_on_Timer_timeout")
#	add_child(t)
#	t.start()
#
#
#func _exit_tree():
#	resource_loader_thread_run = false
#	resource_load_semaphore.post() # unlock thread
#	resource_loader_thread.wait_to_finish()
#
#
#func _on_Timer_timeout():
#	tick()
