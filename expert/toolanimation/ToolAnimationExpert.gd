extends Node


# Chooses which animation to use when holding specific tools.
func person_arm_animation_variables(item):
	if item.is_in_group("animation_farm_hoe"):
		return [
			1.1,   # ANIM_ATTACK_INTENSITY_A
			-0.65, # ANIM_ATTACK_INTENSITY_B
			0.4,   # HOLDER_PRIME_ATTACK_THRESHOLD
		]
	
	# Default animation.
	return [
		0.9,  # ANIM_ATTACK_INTENSITY_A
		0.15, # ANIM_ATTACK_INTENSITY_B
		0.6,  # HOLDER_PRIME_ATTACK_THRESHOLD
	]
