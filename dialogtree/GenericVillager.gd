extends "res://dialogtree/DialogTree.gd"

enum {
	STATE_DEFAULT,
	STATE_THANKS,
	STATE_PISS_OFF,
}

var state = STATE_DEFAULT
var queue = {
	STATE_DEFAULT: [
		"Hello adventurer! Welcome to the wonderful village of Umm",
		"I hope you enjoy your stay",
		{
			"Thanks":   STATE_THANKS,
			"Piss off": STATE_PISS_OFF,
		},
	],
	STATE_THANKS: [ "You are welcome." ],
	STATE_PISS_OFF: [ "I don't like you." ],
}
