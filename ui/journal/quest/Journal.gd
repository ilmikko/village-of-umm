extends PanelContainer
 
var PageListTitle = preload("res://ui/journal/quest/PageListTitle.tscn")

onready var page_container = $HSplitContainer/PageContainer
onready var pages_titles_container = $HSplitContainer/Left/ScrollContainer/PagesTitlesContainer

enum views {
	   ARCHIVED
	   ACTIVE
}

var current_view = views.ACTIVE
 
func show():
	   load_pages()
	   .show()

func load_pages():
	   _display_page(JournalPage.new(''))
	   var pages
	   if current_view == views.ARCHIVED:
			   pages = Runtime.Game.Journal.archived
	   else:
			   pages = Runtime.Game.Journal.active

	   Util.remove_children(pages_titles_container)
	   
	   if pages.size() == 0: return

	   for page in pages:
			   var page_title = PageListTitle.instance()
			   page_title.title = page.title
			   page_title.connect("title_pressed", self, "_title_pressed", [page])
			   page_title.connect("archive_pressed", self, "_archive_pressed", [page])
			   pages_titles_container.add_child(page_title)

	   _display_page(pages[0])


func _display_page(page):
	   page_container.page = page


func _title_pressed(page):
	   _display_page(page)


func _archive_pressed(page):
	   if current_view == views.ARCHIVED:
			   Runtime.Game.Journal.activate_page(page)
	   else:
			   Runtime.Game.Journal.archive_page(page)
	   load_pages()


func _on_Active_pressed():
	  current_view = views.ACTIVE
	  load_pages()


func _on_Archived_pressed():
	   current_view = views.ARCHIVED
	   load_pages()
