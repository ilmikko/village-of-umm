extends VBoxContainer

var EntryContainer = preload("res://ui/journal/quest/EntryContainer.tscn")

onready var title = $Title
onready var entries = $ScrollContainer/Entries
var page setget _set_page

func _set_page(new_page):
	page = new_page
	_rebuild()

func _rebuild():
	Util.remove_children(entries)
	title.bbcode_text = "[center]%s[/center]" % page.title
	for entry in page.entries:
		var entry_container = EntryContainer.instance()
		entry_container.entry = entry
		entries.add_child(entry_container)
