extends WindowDialog


func set_state(paused):
	if paused:
		Runtime.Game.paused = true
		popup_centered()
	else:
		Runtime.Game.paused = false
		hide()


func _on_Back_pressed():
	set_state(false)


func _on_Save_pressed():
	Runtime.GameScene.save_all()
	set_state(false)


func _on_Load_pressed():
	Runtime.GameScene.load_all()
	set_state(false)


func _on_Journal_pressed():
	Runtime.UI.Journal.show()
	hide()
