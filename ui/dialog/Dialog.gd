extends Control


enum {
	DIALOG_TYPE_UNKNOWN
	DIALOG_TYPE_END
	DIALOG_TYPE_SPEECH
	DIALOG_TYPE_OPTIONS
	DIALOG_TYPE_STATE_CHANGE
}


var current_controller = null


var dialog_tree
var dialog_queue = []
var dialog_started = false


func dialog_end():
	dialog_started = false
	if current_controller:
		current_controller.primary = true
	
	Runtime.Game.Time.disconnect('at_least_0_0_5_seconds_passed', self, '_on_at_least_0_0_5_seconds_passed')

	visible = false
	dialog_queue = []
	_set_dialog_options({})
	text_area.text = ""
	next_hint.text = ""
	end_hint.text  = ""


func dialog_has_options() -> bool:
	return !dialog_options.empty()


func dialog_type(any) -> int:
	var  t = typeof(any)
	if   t == TYPE_STRING:
		return DIALOG_TYPE_SPEECH
	elif t == TYPE_INT:
		return DIALOG_TYPE_STATE_CHANGE
	elif t == TYPE_DICTIONARY:
		return DIALOG_TYPE_OPTIONS
	elif t == TYPE_NIL:
		return DIALOG_TYPE_END
	else:
		return DIALOG_TYPE_UNKNOWN


func dialog_open_state(s):
	_set_state(s)
	_set_dialog_options({})
	
	if !dialog_tree.queue.has(s):
		push_error("Bug: Someone didn't implement all the dialog tree options.")
		dialog_queue = [
			"Lmao, timeout, it looks like I'm bugged out.",
			"This is because the game's developers didn't implement dialog state '%s' properly." % s,
			"Please file a pull request to http://gitlab.com/ilmikko/godot-playground to fix.",
			"You can kick me now if you're frustrated.",
		]
		return
	dialog_queue = dialog_tree.queue[s].duplicate()


func dialog_seconds_left():
	var sl = (dialog_text.length() - dialog_text_index)
	sl /= DIALOG_VISIBLE_CHARACTERS_PER_SECOND
	sl += DIALOG_SECONDS_BETWEEN_SPEECHES
	Debug.text(self, "Dialog seconds:", "%s" % [sl])
	return sl


func dialog_select(option):
	if dialog_options.empty(): return
	
	var reply = dialog_options_list[option]
	var next_state = dialog_options[reply]
	
	dialog_open_state(next_state)
	dialog_next()


func dialog_show():
	if dialog_queue.empty(): return
	
	if current_controller:
		current_controller.primary = false

	visible = true
	end_hint.text = "[%s] End" % Util.key_names_for_action("ui_dialog_end")
	dialog_next()


func dialog_next():
	next_hint.text = ""
	
	var next = dialog_queue.pop_front()
	var type = dialog_type(next)
	
	if   type == DIALOG_TYPE_END:
		dialog_end()
	elif type == DIALOG_TYPE_SPEECH:
		_set_dialog_text(next)
	elif type == DIALOG_TYPE_STATE_CHANGE:
		_set_state(next)
		dialog_next()
	elif type == DIALOG_TYPE_OPTIONS:
		_set_dialog_options(next)
	else:
		push_warning("Unknown type passed to dialog_next: %s -> %s (%s)" % [typeof(next), type, next])


func dialog_next_maybe():
	if dialog_still_talking(): return false
	if dialog_has_options():   return false
	dialog_next()
	return true


func dialog_peek_next():
	if dialog_queue.empty(): return null
	return dialog_queue[0]


func dialog_skip():
	dialog_tick((dialog_text.length() + DIALOG_SECONDS_BETWEEN_SPEECHES + 1) * DIALOG_VISIBLE_CHARACTERS_PER_SECOND)


func dialog_skip_maybe():
	if ! dialog_still_talking(): return false
	dialog_skip()
	return true


func dialog_still_talking() -> bool:
	return dialog_seconds_left() > 0


func dialog_tick(n):
	dialog_text_index += n * DIALOG_VISIBLE_CHARACTERS_PER_SECOND
	text_area.visible_characters = int(dialog_text_index)
	
	if dialog_still_talking():
		if dialog_text_index / DIALOG_VISIBLE_CHARACTERS_PER_SECOND < DIALOG_SHOW_SKIP_AFTER_SECONDS:   return
		if dialog_seconds_left() < DIALOG_SHOW_SKIP_IF_SECONDS_LEFT: return
		
		next_hint.text = "[%s] Skip" % Util.key_names_for_action("ui_dialog_skip")
		return

	var next_type = dialog_type(dialog_peek_next())
	if  next_type == DIALOG_TYPE_SPEECH:
		next_hint.text = "[%s] Next" % Util.key_names_for_action("ui_dialog_next")
	elif next_type == DIALOG_TYPE_OPTIONS:
		dialog_next()


func start(dialog_tree_instance, controller):
	dialog_tree = dialog_tree_instance
	dialog_tree.start(controller)
	current_controller = controller
	_set_state(dialog_tree.state)
	dialog_open_state(state)
	dialog_show()


func _input(_event):
	if not visible: return

	if Input.is_action_just_pressed("ui_dialog_end"):
		dialog_end()
		return accept_event()
	if Input.is_action_just_pressed("option_select_1"):
		dialog_select(0)
		return accept_event()
	if Input.is_action_just_pressed("option_select_2"):
		dialog_select(1)
		return accept_event()
	if Input.is_action_just_pressed("option_select_3"):
		dialog_select(2)
		return accept_event()
	if Input.is_action_just_pressed("option_select_4"):
		dialog_select(3)
		return accept_event()
	if Input.is_action_just_pressed("ui_dialog_skip"):
		if dialog_skip_maybe():
			return accept_event()
	if Input.is_action_just_pressed("ui_dialog_next"):
		if dialog_next_maybe():
			return accept_event()


func _options_to_string(opts):
	var options_string = ""
	for i in len(opts):
		options_string += "[%s] %s\n" % [str(i + 1), opts[i]]
	return options_string

# How many seconds to wait after speech ends before showing "Next" or options.
export var DIALOG_SECONDS_BETWEEN_SPEECHES = 0.5
# How many seconds to wait before we show "Skip" hint.
export var DIALOG_SHOW_SKIP_AFTER_SECONDS = 2.0
# How many seconds should be left at least to show "Skip" hint.
# This is to prevent flashing the hint if only a few characters remain.
export var DIALOG_SHOW_SKIP_IF_SECONDS_LEFT = 2.0
# How many characters per second to display in the animation.
export var DIALOG_VISIBLE_CHARACTERS_PER_SECOND = 15.0


var dialog_options = {} setget _set_dialog_options
var dialog_options_list = []
func _set_dialog_options(_dialog_options):
	dialog_options =     _dialog_options
	dialog_options_list = dialog_options.keys()
	# TODO: Hashed shuffle instead of completely random
	dialog_options_list.shuffle()
	options_area.text = _options_to_string(dialog_options_list)


var dialog_text = "" setget _set_dialog_text
var dialog_text_index = 0.0
func _set_dialog_text(_dialog_text):
	dialog_text = _dialog_text
	dialog_text_index = 0.0
	text_area.text = _dialog_text
	text_area.visible_characters = int(dialog_text_index)
	if not dialog_started:
		dialog_started = true
		Runtime.Game.Time.connect('at_least_0_0_5_seconds_passed', self, '_on_at_least_0_0_5_seconds_passed')


var state = 0 setget _set_state
func _set_state(_state):
	state = _state
	if dialog_tree != null:
		dialog_tree.state = _state


onready var text_area    = $Panel/Control/MarginContainer/VSplitContainer/Text
onready var options_area = $Panel/Control/MarginContainer/VSplitContainer/Options
onready var end_hint     = $Panel/Control/VBoxContainer/EndHint
onready var next_hint    = $Panel/Control/VBoxContainer/NextHint


func _on_at_least_0_0_5_seconds_passed(n):
	dialog_tick(n)
