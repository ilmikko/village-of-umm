extends Control


var quest_title = "" setget _set_quest_title
func _set_quest_title(text):
	if text:
		quest_title = text
		$QuestTitle/Title/TitleLabel.text = text
		$QuestTitle/Title/TitleLabel.visible = true
	else:
		$QuestTitle/Title/TitleLabel.visible = false


var quest_title_sub = "" setget _set_quest_title_sub
func _set_quest_title_sub(text):
	if text:
		quest_title_sub = text
		$QuestTitle/TitleSub/TitleSubLabel.text = text
		$QuestTitle/TitleSub/TitleSubLabel.visible = true
	else:
		$QuestTitle/TitleSub/TitleSubLabel.visible = false


var center_label = "" setget _set_center_label
func _set_center_label(text):
	if text:
		center_label = text
		$CenterLabel.text = text
		$CenterLabel.visible = true
	else:
		$CenterLabel.visible = false


func quest_title_animate():
	Util.control_center($QuestTitle/Title/TitleLabel)
	Util.control_center($QuestTitle/TitleSub/TitleSubLabel)
	$QuestTitle/QuestTitleAnimation.play("FadeIn")


func _ready():
	_set_quest_title("")
	_set_quest_title_sub("")
