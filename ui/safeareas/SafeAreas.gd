tool
extends Control


export var main_size_max = Vector2(1500, 1150) setget _set_main_size_max
func _set_main_size_max(_main_size_max):
	main_size_max = _main_size_max


export var main_size = Vector2(1200, 800) setget _set_main_size
func _set_main_size(_main_size):
	_main_size.x = min(_main_size.x, main_size_max.x)
	_main_size.y = min(_main_size.y, main_size_max.y)
	main_size = _main_size
	rebuild_main_area()


var viewport_size = Vector2(1200, 800)


func rebuild():
	if !is_inside_tree(): return
	rebuild_main_area()


func rebuild_main_area():
	if !is_inside_tree(): return
	$Center/MainArea.rect_size = main_size
	Util.control_center($Center/MainArea)


func _resized():
	viewport_size = get_viewport().size
	_set_main_size(viewport_size)
	

func _ready():
	assert(get_tree().get_root().connect("size_changed", self, "_resized") == OK)
	_resized()
