#!/usr/bin/env sh
# Finds all groups that we have defined.

read_tscn() {
	awk '
	/^\]/       { r=0; }
	/./         { if (r) { sub("^\"", "", $0); sub("\",?$", "", $0); print $0 } }
	/groups=\[/ { r=1; }
	' $@;
}

read_tscn_loop() {
	while read tscn; do read_tscn "$tscn"; done
}

find . -type f -iname "*.tscn" | read_tscn_loop;
