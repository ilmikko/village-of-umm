#!/usr/bin/env sh

# Usage: ./scene_structure.sh
DIR="$(dirname $0)";

FILE="$@";
META="$DIR/../meta/scene";

ext_resource_scenes() {
	cat "$@" | grep 'ext_resource' | grep -o 'res://.*.tscn';
}

ext_resource_scenes_each() {
	while read scene; do
		scene="$(scene_path "$scene")";
		scenes="$(ext_resource_scenes_loop "$scene")";
		echo "$(echo "$scenes" | wc -l) $scene";
		echo "$scenes";
	done
}

ext_resource_scenes_loop() {
	# TODO: The list is sorted by references.
	# ....: Even better would be to benchmark each of these scenes in Godot by
	# ....: loading them and seeing how long it takes; we want to load the ones
	# ....: with shortest loading time first.
	meta="$(echo "$META/$@.gd")";
	scenes=$(ext_resource_scenes "$@" | ext_resource_scenes_each);
	mkdir -p "$(dirname "$meta")";
	scenes_meta="$(echo "$scenes" | sort -h)";
	echo "var scenes     = [ $(echo "$scenes_meta" | awk '{ if ($2) print "\""$2"\"," }') ]"  > "$meta";
	echo "var references = [ $(echo "$scenes_meta" | awk '{ if ($1) print $1"," }') ]"       >> "$meta";
	echo "$scenes";
}

scene_path() {
	echo "$@" | sed 's;res://;;';
}

main() {
	ext_resource_scenes_loop "$(scene_path "$@")";
}

main "$FILE";
