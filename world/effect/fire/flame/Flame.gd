tool
extends Spatial


export(float, 0.1, 10.0) var heat_distance = 1.0 setget _set_heat_distance
func _set_heat_distance(d):
	heat_distance = d
	$HeatArea.heat_distance = d

export(float, 0.0, 10.0) var hotness = 1.0 setget _set_hotness
func _set_hotness(h):
	hotness = h
	$HeatArea.hotness = h

export(float, 0.0, 1.0) var intensity = 1.0 setget _set_intensity
func _set_intensity(i):
	intensity = i
	$HeatArea.intensity = i
