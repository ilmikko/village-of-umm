shader_type spatial;
render_mode cull_disabled, vertex_lighting;

uniform sampler2D burning;
uniform sampler2D alpha;
uniform float flame_sway_speed = 1.0;
uniform float flame_sway_intensity = 0.4;
uniform float flame_pulse_speed = 1.0;
uniform float flame_pulse_intensity = 1.0;

void vertex() {
	float size_d = 1.0 + 0.5 * cos(TIME * flame_pulse_speed + UV.x) * flame_pulse_intensity;
	size_d = 0.75 + size_d * 0.25;
	VERTEX *= size_d;
	
	float sway_d = (1.0 - UV.y);
	
	VERTEX.x += sway_d * cos(TIME * flame_sway_speed + UV.x * 45.0) * flame_sway_intensity;
	VERTEX.y += sway_d * cos(TIME * flame_sway_speed) * 0.1 * flame_sway_intensity;
	VERTEX.z += sway_d * sin(TIME * flame_sway_speed - UV.x * 92.0) * flame_sway_intensity;
}

void fragment() {
	vec4 burning_col = texture(burning, SCREEN_UV + vec2(0, -TIME * 0.3));
	EMISSION = burning_col.rgb;
	ALPHA = texture(alpha, UV).r;
}