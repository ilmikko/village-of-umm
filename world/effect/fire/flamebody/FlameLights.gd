tool
extends Spatial


const INTENSITY_MULTIPLIER = 5.0


export(float, 0.0, 1.0) var intensity = 0.0 setget _set_intensity
func _set_intensity(i):
	intensity = i
	rebuild()


func rebuild():
	if !is_inside_tree(): return
	var i = intensity * INTENSITY_MULTIPLIER
	$OmniLight.light_energy =  i
	$OmniLight2.light_energy = i
	$OmniLight3.light_energy = i
