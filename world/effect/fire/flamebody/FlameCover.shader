shader_type spatial;
render_mode vertex_lighting;

uniform sampler2D burning;
uniform sampler2D alpha;
uniform float intensity : hint_range(0.0, 1.0) = 0.1;

void fragment() {
	vec4 burning_col = texture(burning, SCREEN_UV + vec2(0, -TIME * 0.3));
	EMISSION = burning_col.rgb * intensity;
	
	float a = 0.0;
	
	a += texture(alpha, SCREEN_UV * 2.5 + vec2(0,   -TIME * 0.3)).r;
	a += texture(alpha, SCREEN_UV * 2.5 + vec2(0.9, -TIME * 0.5)).r * intensity;
	a *= intensity + texture(alpha, SCREEN_UV * 2.5 + vec2(0.5, -TIME * 0.1)).r * intensity;
	
	ALPHA = min(a, 1.0);
}