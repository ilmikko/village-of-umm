tool
extends Spatial


export(float, 0.0, 1.0) var intensity = 0.0 setget _set_intensity
func _set_intensity(i):
	intensity = i
	rebuild()


func rebuild():
	$MeshInstance.material_override.set_shader_param("intensity", intensity)
