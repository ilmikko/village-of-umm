tool
extends Spatial


const INTENSITY_MULTIPLIER = 1.8


export(float, 0.0, 5.0) var size = 1.0 setget _set_size
func _set_size(s):
	size = s
	rebuild()


export(float, 0.0, 1.0) var intensity = 0.0 setget _set_intensity
func _set_intensity(i):
	intensity = i
	rebuild()


func rebuild():
	if !is_inside_tree(): return
	
	var s = Vector3(1, 1, 1) * size * (intensity + intensity*intensity*INTENSITY_MULTIPLIER)
	$MeshInstance.scale = s
	$UpwardsFlame.scale = s
