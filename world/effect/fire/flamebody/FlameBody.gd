extends Spatial


const FLAME_COVER_BOX = preload("res://world/effect/fire/flamebody/FlameCoverBox.tscn")
const FLAME_COVER_CYLINDER = preload("res://world/effect/fire/flamebody/FlameCoverCylinder.tscn")
const FLAME_EFFECT = preload("res://world/effect/fire/flamebody/FlameEffect.tscn")
const FLAME_LIGHTS = preload("res://world/effect/fire/flamebody/FlameLights.tscn")


var body = null


var intensity = 0.0 setget _set_intensity
func _set_intensity(i):
	if i < 0:
		queue_free()
		return
	
	if i > 1:
		i = 1
		if  body:
			# Original object is destroyed in the fire, only the fire remains.
			var t = self.global_transform
			body.remove_child(self)
			body.get_parent().add_child(self)
			self.global_transform = t
			
			# Also remove all cover effects as the object is gone.
			for c in $Covers.get_children():
				intensity_updates.erase(c)
				c.queue_free()
			
			Runtime.Expert.Breakage.break(body)
			body = null
	
	intensity = i
	$HeatArea.intensity = i
	for c in intensity_updates:
		c.intensity = i


var intensity_updates = []


# The cover effect is a "flaming" texture applied on top of the shape.
# It depends on the shape itself.
func add_cover_effect(cover, shape, size):
	cover.intensity = intensity
	cover.scale = size
	cover.transform = shape.transform
	$Covers.add_child(cover)
	intensity_updates.append(cover)


# The flame effect is a simple multidirectional flame put at the middle of the
# object. The intensity of the flame effect controls its size.
func add_flame_effect(_shape, size):
	var flame = FLAME_EFFECT.instance()
	flame.intensity = intensity
	flame.scale = size
	$Effects.add_child(flame)
	intensity_updates.append(flame)


func add_light_effect(_body):
	var light = FLAME_LIGHTS.instance()
	light.intensity = intensity
	$Effects.add_child(light)
	intensity_updates.append(light)


func catch_fire():
	_set_intensity(intensity)
	
	body = get_parent()
	
	if body is StaticBody:
		body = Runtime.Expert.Breakage.prepare_staticbody(body)
	
	var extents = Runtime.Expert.Volume.extents_no_transforms(body)
	
	$HeatArea.heat_distance = 1.1 + extents.length() * 1.75
	
	for col in Util.get_collisions(body):
		add_flame_effect(col, extents)
		
		var cover
		if col.shape is BoxShape:
			cover = FLAME_COVER_BOX.instance()
		elif col.shape is CylinderShape:
			cover = FLAME_COVER_CYLINDER.instance()
		else:
			push_warning("Fire cover effect not implemented for shape %s" % col.shape)
		
		if cover:
			add_cover_effect(cover, col, extents)
	
	# Heat that doesn't come from a "source" should be less than the source
	# itself.
	$HeatArea.hotness = 0.3
	
	add_light_effect(body)
	
	$Timer.start()


func burn_tick():
	if body == null:
		_set_intensity(intensity - 0.009)
	else:
		if intensity > 0.75 && !(body is RigidBody):
			body = Runtime.Expert.Breakage.break(body)
		_set_intensity(intensity + 0.005)


func _on_Timer_timeout():
	burn_tick()
