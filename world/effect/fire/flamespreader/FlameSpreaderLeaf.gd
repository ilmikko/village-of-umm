tool
extends Spatial


# TODO: Join this class with FlameLights.gd as it has many similar methods
const INTENSITY_LIGHT_MULTIPLIER = 5.0
const MAX_INTENSITY = 5.0


export(float, 0.0, 1.0) var intensity = 0.0 setget _set_intensity
func _set_intensity(i):
	intensity = i
	if intensity > MAX_INTENSITY: intensity = MAX_INTENSITY
	rebuild()


func rebuild():
	if !is_inside_tree(): return
	$HeatArea.intensity = min(intensity, 2.0)
	$OmniLight.light_energy = min(intensity, 1.0) * INTENSITY_LIGHT_MULTIPLIER
	
	var s = Vector3(1, 1, 1) * intensity
	if  s.y > 4.0: s.y = 4.0
	$MeshInstance.scale  = s
	$MeshInstance2.scale = s
