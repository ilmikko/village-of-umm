extends Spatial


const LEAF = preload("res://world/effect/fire/flamespreader/FlameSpreaderLeaf.tscn")

var spread_radius = 1.0

# Tries its best to spread as far as it can on a flammable surface.
# TODO: Control flammability of a surface / have natural die-outs

# TODO: Pre-fetch occupancy data as a grid, eventually falling back to areas

func spread_tick():
	var leaf = LEAF.instance()
	
	# Spiral out slowly
	var angle = randf()*TAU
	var x = cos(angle) * spread_radius
	var y = sin(angle) * spread_radius
	$Cast.transform.origin = Vector3(x, 0, y)
	
	var  collider = Util.get_root_parent($Cast/Area.get_collider())
	if  (collider != null && collider.is_in_group("flame")):
		# We can't expand here, fire is already there. Grow that fire instead.
		collider.intensity += 0.05;
		spread_radius += 0.1
		return
	
	spread_radius = 0.0
	
	var collision = $Cast/Body.get_collision_point()
	if  collision.length() == 0:
		# No collision
		return
	
	# A single fire can only have this many children.
	if get_child_count() > 25:
		return
	
	# This area is free to expand to
	leaf.global_transform = leaf.global_transform.rotated(Vector3.DOWN, randf()*TAU).translated($Cast/Body.get_collision_point())
	
	add_child(leaf)


func _on_Timer_timeout():
	spread_tick()
