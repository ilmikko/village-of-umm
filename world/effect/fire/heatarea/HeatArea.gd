tool
extends Area


# TODO: Get rid of the timer, use scheduling


var FLAME_BODY = load("res://world/effect/fire/flamebody/FlameBody.tscn")


var bodies_in_influence = {}


export(float, 0.1, 10.0) var heat_distance = 1.0 setget _set_heat_distance
func _set_heat_distance(d):
	heat_distance = d
	rebuild()

export(float, 0.0, 10.0) var hotness = 1.0

export(float, 0.0, 1.0) var intensity = 1.0



func body_entered(body):
	# TODO: Heat up bodies based on their size; not uniformly
	if body is StaticBody:
		if !body.is_in_group("flammable"):
			return
	if !bodies_in_influence.has(body):
		bodies_in_influence[body] = 0.0


func body_exited(body):
	if body is StaticBody:
		if !body.is_in_group("flammable"):
			return
	if  bodies_in_influence.has(body):
		bodies_in_influence.erase(body)


func heat_bodies(bodies):
	var heat_source = global_transform.origin
	for body in bodies:
		if body.is_in_group("on_fire"):
			bodies.erase(body)
			continue
		
		# The flame's hotness is inverse square law; edges are not really hot,
		# but the centre is REALLY hot.
		var distance = (body.global_transform.origin - heat_source).length()
		distance /= heat_distance
		if distance > 1:
			distance = 1
		distance = (1 - distance)
		distance *= distance
		
		# Apply distance back; smaller heat_distance means the heat area is less
		# hot.
		distance *= heat_distance;
		
		# The intensity of the area is also inverse.
		# A starting fire isn't really hot, but the more intense it gets the
		# more exponentially hot it gets.
		distance *= intensity;
		
		distance *= hotness;
		
		# TODO: Track temperature of bodies in a central location
		bodies[body] += distance
		Debug.text(self, "Body %s temp:" % body.name, bodies[body])
		if bodies[body] < 1: continue
		
		make_catch_fire(body)


func make_catch_fire(body):
	if body.is_in_group("on_fire"): return
	body.add_to_group("on_fire")
	
	var flame_body = FLAME_BODY.instance()
	body.add_child(flame_body)
	flame_body.catch_fire()


func rebuild():
	if !is_inside_tree(): return
	
	var sphere = SphereShape.new()
	sphere.radius = heat_distance
	$CollisionShape.shape = sphere


func _on_HeatArea_body_entered(body):
	body_entered(body)


func _on_HeatArea_body_exited(body):
	body_exited(body)


func _on_Timer_timeout():
	heat_bodies(bodies_in_influence)
