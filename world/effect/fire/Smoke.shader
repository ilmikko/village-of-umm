shader_type spatial;
render_mode vertex_lighting;

uniform sampler2D alpha;

// The billboard shader keeps the vertices facing a certain direction regardless
// of camera positioning.
void vertex() {
	vec3 pos = VERTEX.yxz; // Rearrange axes to determine rotation
	pos *= vec3(1.0, -1.0, 1.0); // Multiply by negative 1 to invert normals
	pos += vec3(0.0, 0.0, -0.5);
	VERTEX.xyz = (CAMERA_MATRIX * vec4(pos, 0.0)).xyz;
}

void fragment() {
	ALPHA = texture(alpha, UV).r;
}