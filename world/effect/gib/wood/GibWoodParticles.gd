tool
extends Spatial


var mat : ParticlesMaterial


func restart():
	for particle in get_children():
		particle.restart()
		particle.emitting = true


func rebuild():
	if !mat:
		mat = ParticlesMaterial.new()
		for particle in get_children():
			particle.process_material = mat
	
	mat.direction = Vector3(0.0, 0.0, 0.0)
	mat.emission_box_extents = extents
	mat.emission_shape = ParticlesMaterial.EMISSION_SHAPE_BOX
	mat.flag_rotate_y = true
	
	mat.gravity = Vector3(0.0, 0.0, 0.0)
	mat.damping = 1.0
	
	mat.initial_velocity = 8.0
	mat.initial_velocity_random = 0.5
	
	mat.angular_velocity = 3.0
	mat.angular_velocity_random = 0.5
	
	mat.scale_random = 0.3
	mat.scale_curve = scale_curve
	
	for particle in get_children():
		particle.lifetime = 0.8


func _ready():
	rebuild()
	restart()


export var emitting = false setget _set_emitting
func _set_emitting(e):
	if e == emitting: return
	emitting = e
	if !e: return
	restart()


export var extents = Vector3(1.0, 1.0, 1.0) setget _set_extents
func _set_extents(e):
	if e == extents: return
	extents = e
	rebuild()


export(CurveTexture) var scale_curve setget _set_scale_curve
func _set_scale_curve(c):
	if c == scale_curve: return
	scale_curve = c
	rebuild()
