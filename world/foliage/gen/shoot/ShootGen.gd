tool
extends Spatial


func absorb_water(litres):
	_set_size(size + litres)


func level_add():
	var level = (size/settings.shoot_level_threshold)
	
	# Spawn more fruits if possible.
	level_add_fruit(level)
	
	# Spawn more flowers if possible.
	# TODO
	
	# Spawn more shoots if possible.
	level_add_shoot(level)


func level_add_fruit(level):
	if  floor(level) > settings.fruit_level_max || floor(level) < settings.fruit_level_min: return

	for spawn in spawns:
		var fruit = FRUIT.instance()
		
		fruit.settings = settings
		fruit.level    = level
		
		fruit.transform  = fruit.transform.translated(
			Vector3.UP * 0.7
		).rotated(
			Vector3.FORWARD, PI/2.0
		)
		
		fruits.append(fruit)
		spawn.add_child(fruit)
		fruit.rebuild()


func level_add_shoot(level):
	if  level > settings.shoot_level_max: return
	
	# TODO: Add absolute maximum leaf count we can have.
	var next_spawns = []
	
	for spawn in spawns:
		for i in range(settings.spawn_count_per_shoot):
			var shoot = SHOOT.instance()
			shoot.settings = settings
			shoot.level    = level
			
			shoot.transform = shoot.transform.rotated(
				Vector3.UP,      settings.spawn_rotation.x
			).rotated(
				Vector3.FORWARD, settings.spawn_rotation.y
			).rotated(
				Vector3.UP,      settings.spawn_rotation.z * level + (TAU/settings.spawn_count_per_shoot) * i
			)
			
			shoots.append(shoot)
			
			spawn.add_child(shoot)
			next_spawns.append(shoot)
	
	spawns = next_spawns


func level_remove():
	var level = (size/settings.shoot_level_threshold)
	if  level > settings.shoot_level_max: return
	
	var next_spawns_map = {}

	for spawn in spawns:
		var spawn_parent = spawn.get_parent()
		# Deduplicate multiple shoots that end up with the same parent with a map.
		next_spawns_map[spawn_parent.get_path()] = spawn_parent
		if !spawn.is_in_group('shoot_gen_root'):
			shoots.erase(spawn)
			spawn.queue_free()

	var next_spawns = []
	for path in next_spawns_map:
		next_spawns.append(next_spawns_map[path])

	spawns = next_spawns


export(float, 0.0, 10.0) var size = 0.0 setget _set_size
func _set_size(s):
	if spawns.empty():
		spawns = [$Root]
	
	if s <= 0:
		Util.remove_children($Root)
		fruits = []
		shoots = []
		spawns = []
		size = max(s, 0.0)
		return
	
	Debug.text(self, "foliage size", size)
	Debug.text(self, "shoots", shoots)
	Debug.text(self, "fruits", fruits)
	
	# Spawn step
	var next_threshold = (ceil(size/settings.shoot_level_threshold))*settings.shoot_level_threshold
	var prev_threshold = next_threshold - settings.shoot_level_threshold
	var dist  = min(abs(next_threshold-size), abs(prev_threshold-size))
	var times = abs(size-s)/settings.shoot_level_threshold - dist
	
	if size < next_threshold && s >= next_threshold || size == 0:
		for _i in (times):
			level_add()
			size += settings.shoot_level_threshold
	elif size > prev_threshold && s <= prev_threshold:
		for _i in (times):
			level_remove()
			size -= settings.shoot_level_threshold
	
	size = s
	
	
	# Grow shoots.
	for i in range(shoots.size()):
		var shoot = shoots[i]
		
		var shoot_threshold_p = min(size - shoot.level*settings.shoot_level_threshold, 1.0)
		var shoot_size = (s * settings.size_curve_max * pow(settings.size_ratio, shoot.level)) / (s + settings.size_curve_c)
		
		shoot.size = shoot_size * shoot_threshold_p
		
		if shoot.level > 0:
			var y = shoot.get_parent().size * settings.spawn_ratio
			shoot.transform.origin = Vector3(0, y, 0)
	
	# Grow fruits.
	for i in range(fruits.size()):
		var fruit = fruits[i]
		if  fruit == null || !is_instance_valid(fruit):
			continue
		
		var fruit_threshold_p = s - fruit.level*settings.shoot_level_threshold
		fruit.size = fruit_threshold_p * settings.size_ratio


var fruits = []
var shoots = []

var spawns = []


const FRUIT = preload("res://world/foliage/gen/fruit/FruitGen.tscn")
const SHOOT = preload("res://world/foliage/gen/shoot/Shoot.tscn")
const AXIS = preload("res://debug/visual/Axis.tscn")


var settings = null
export(Script) var settings_script = preload("res://world/foliage/gen/shoot/settings/Default.gd") setget _set_settings
func _set_settings(s):
	settings_script = s
	settings = settings_script.new()
	if size > 0:
		Debug.warn(self, "FoliageGen settings changed after size was set")


func _enter_tree():
	if settings == null:
		settings = settings_script.new()
