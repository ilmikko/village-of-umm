tool
extends Spatial

func rebuild():
	if !is_inside_tree(): return
	
	mat.albedo_color = lerp(
		settings.shoot_color_start,
		settings.shoot_color_end,
		min(size, 1.0)
	)
	
	var wither = clamp(size - settings.leaf_wither_threshold, 0.0, 1.0)
	
	$Trunk/MeshInstance.material_override = mat
	$Leaf/MeshInstance.material_override  = mat
	
	var trunk_width = size * settings.trunk_ratio
	$Trunk.scale = Vector3(trunk_width, size, trunk_width)
	
	var leaf_size = 1.0 - settings.trunk_leaf_ratio
	var leaf_ratio = settings.leaf_ratio * (1.0 - wither)
	$Leaf.scale  = Vector3(leaf_ratio, 1.0, leaf_ratio) * leaf_size * size * 0.5
	$Leaf.transform.origin = Vector3(0, size * settings.trunk_leaf_ratio, 0)


var level

var mat = preload("res://material/foliage/gen/Shoot.tres").duplicate()


export(float, 0.0, 10.0) var size = 0.0 setget _set_size
func _set_size(s):
	size = s
	rebuild()


export(float, 0.0, 10.0) var tweak = 0.0 setget _set_tweak
func _set_tweak(t):
	tweak = t
	settings.leaf_ratio = t
	rebuild()


export(float, 0.0, 10.0) var tweak2 = 0.0 setget _set_tweak2
func _set_tweak2(t):
	tweak2 = t
	settings.trunk_leaf_ratio = t
	rebuild()


var settings = preload("res://world/foliage/gen/shoot/settings/Default.gd").new() setget _set_settings
func _set_settings(s):
	settings = s
	rebuild()
