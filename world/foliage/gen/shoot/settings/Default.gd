extends Node

var fruit_level_min       = 3.0
var fruit_level_max       = 3.0

var fruit_color_ripe = Color(0.8, 0.0, 0.0, 1.0)
var fruit_color_raw  = Color(0.0, 0.6, 0.1, 1.0)
var fruit_height_drop = 0.1
var fruit_size_max   = 1.0
var fruit_size_max_c = 1.5
var fruit_squish = 1.0
var fruit_variant = 0
# How ripe the fruit needs to be to fall from the tree.
var fruit_ripeness_fall_threshold        = 0.85
# How ripe the fruit needs to be to be harvestable.
var fruit_ripeness_harvestable_threshold = 0.2


var trunk_ratio = 0.05
var trunk_leaf_ratio = 0.75

var leaf_ratio            = 0.9
var leaf_wither_threshold = 0.7


var shoot_count_per_level = 1.0
var shoot_color_start     = Color(0.0, 0.6, 0.0, 1.0)
var shoot_color_end       = Color(0.43, 0.6, 0.03, 1.0)
var shoot_color_threshold = 1.0
var shoot_level_threshold = 0.75
var shoot_level_max       = 3.0


var spawn_count_per_shoot = 2.0
var spawn_rotation        = Vector3(0.17, 0.17, 0.7)
var spawn_ratio           = 0.5

var size_curve_max = 1.5
var size_curve_c   = 1.5
var size_ratio     = 0.5
