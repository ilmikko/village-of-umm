extends Node


var flower_max_depth = 1.0
var flower_min_depth = 1.0
var flower_color = Color(1.0, 1.0, 1.0, 1.0)
var flower_scale = 0.04
var flower_leaf_level_max = 5.0
var flower_leaf_transform_rate = 0.03
var flower_new_leaf_threshold = 0.25
var flower_new_leaf_count = 3.0
var flower_rot_alpha = 0.7
var flower_rot_beta = 0.8
