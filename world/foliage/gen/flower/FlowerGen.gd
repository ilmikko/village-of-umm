tool
extends Spatial


func leaf_add():
	var level = (size/settings.flower_new_leaf_threshold)
	if  level/settings.flower_new_leaf_count > settings.flower_leaf_level_max: return
	
	for i in settings.flower_new_leaf_count:
		var leaf = PETAL.instance()
		leaf.transform = leaf.transform.rotated(
			Vector3.RIGHT, settings.flower_rot_alpha
		).rotated(
			Vector3.UP, level * settings.flower_rot_beta + (i/settings.flower_new_leaf_count) * TAU
		)
		
		leaf.color = settings.flower_color
		$Leaves.add_child(leaf)
		leaves.append(leaf)


func leaf_remove():
	var level = (size/settings.flower_new_leaf_threshold)
	if  level/settings.flower_new_leaf_count - 0.5 > settings.flower_leaf_level_max: return
	
	for i in settings.flower_new_leaf_count:
		var leaf = leaves.pop_back()
		if  leaf == null: return
		$Leaves.remove_child(leaf)
		leaf.queue_free()


func rebuild():
	pass


export(float, 0.0, 10.0) var size = 0.0 setget _set_size
func _set_size(s):
	if s == 0:
		Util.remove_children($Leaves)
		leaves = []
	
	var next_threshold = (floor(size/settings.flower_new_leaf_threshold)+1)*settings.flower_new_leaf_threshold
	var prev_threshold = next_threshold - settings.flower_new_leaf_threshold
	var times = abs(size-s)/settings.flower_new_leaf_threshold
	
	if size < next_threshold && s >= next_threshold:
		for _i in (times):
			leaf_add()
			size += settings.flower_new_leaf_threshold
	elif size >= prev_threshold && s < prev_threshold:
		for _i in (times):
			leaf_remove()
			size -= settings.flower_new_leaf_threshold
	
	size = s
	
	for i in range(leaves.size()):
		var level = floor(i/settings.flower_new_leaf_count)
		var leaf = leaves[i]
		var sc = s - settings.flower_new_leaf_threshold*(level+1)
		leaf.scale = Vector3(1, 1, 1) * sc
		leaf.transform.origin = -leaf.transform.basis.y * sc * settings.flower_leaf_transform_rate


var settings = preload("res://world/foliage/gen/flower/settings/Default.gd").new()

var leaves = []

var PETAL = preload("res://world/foliage/gen/flower/Petal.tscn")
