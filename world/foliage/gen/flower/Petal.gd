tool
extends Spatial


var mat = preload("res://world/foliage/gen/flower/Petal.tres")


func _ready():
	$MeshInstance.set_surface_material(0, mat)


export(Color) var color = Color(1.0, 0.0, 0.0, 1.0) setget _set_color
func _set_color(c):
	color = c
	mat.albedo_color = color
