extends Spatial

var interact_hint = "Pick"

var foliagegen_settings = preload("res://world/foliage/gen/shoot/settings/Default.gd")

var NodeMatcher = preload("res://script/class/NodeMatcher.gd")

func handle_interaction_by_actor(actor):
	var node_matcher = NodeMatcher.new()
	node_matcher.matching_group ="seed_plantable"
	var self_node_matcher = NodeMatcher.new()
	self_node_matcher.matching_node = self
	actor.InteractionNotifier.add_interaction("secondary", self, node_matcher, self_node_matcher)


func interaction_triggered(actor, plantable_object, interactor_name):
	if plantable_object == null: return
	if interactor_name != "secondary": return
	actor.HolderHandler.drop()
	plantable_object.plant(self)
	
