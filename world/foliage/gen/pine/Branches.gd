tool
extends MultiMeshInstance

const BranchMesh = preload("res://mesh/foliage/tree/PineTreeBranch.obj")

export(int) var count = 100 setget _set_count
func _set_count(_count):
	count = _count
	rebuild()

export(float, -90.0, 90.0) var angle_min = 0.0 setget _set_angle_min
func _set_angle_min(_angle_min):
	angle_min = _angle_min
	rebuild()

export(float, 0.0, 60.0) var height_min = 0.0 setget _set_height_min
func _set_height_min(_height_min):
	height_min = _height_min
	rebuild()

export(float, 0.0, 60.0) var height_max = 0.0 setget _set_height_max
func _set_height_max(_height_max):
	height_max = _height_max
	rebuild()

export(float, 0.0, 5.0) var scale_min = 1.0 setget _set_scale_min
func _set_scale_min(_scale_min):
	scale_min = _scale_min
	rebuild()

export(float, 0.0, 5.0) var scale_max = 1.0 setget _set_scale_max
func _set_scale_max(_scale_max):
	scale_max = _scale_max
	rebuild()

export(float, 0.0, 6.0) var theta = 0.0 setget _set_theta
func _set_theta(_theta):
	theta = _theta
	rebuild()

export(float, 0.0, 1.0) var randomness = 0.0 setget _set_randomness
func _set_randomness(_randomness):
	randomness = _randomness
	rebuild()

export(float, 0.0, 1000.0) var random_seed = 0.0 setget _set_random_seed
func _set_random_seed(_random_seed):
	random_seed = _random_seed
	rebuild()

func rebuild():
	if !is_inside_tree():
		return
	
	if !multimesh:
		multimesh = VolatileMultiMesh.new()
		# Set format for multimesh data.
		multimesh.transform_format = MultiMesh.TRANSFORM_3D
		multimesh.set_custom_data_format(MultiMesh.CUSTOM_DATA_FLOAT)
		multimesh.set_color_format(MultiMesh.COLOR_NONE)
	
	var rand = RandomNumberGenerator.new()
	rand.seed = random_seed
	if random_seed == 0:
		rand.seed = Hash.hash(global_transform)
	
	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh = BranchMesh
	multimesh.instance_count = count
	
	var f = 1.0 / multimesh.instance_count
	var p = 0.0
	var r = rand.randf() * TAU
	
	var basis = Basis()
	
	var height_delta = height_max - height_min
	var scale_delta = scale_max - scale_min
	
	for index in (multimesh.instance_count):
		var rf = 1.0 - randomness*rand.randf()
		var offset = Vector3(0, height_min + height_delta * p + rf, 0)
		var scale = Geo.Vector3(scale_min + (1.0 - p) * scale_delta * rf)
		multimesh.set_instance_transform(index, Transform(basis.rotated(Vector3.FORWARD, angle_min* (TAU/360) * rf).rotated(Vector3.DOWN, r * rf).scaled(scale), offset))
		multimesh.set_instance_custom_data(index, Color(
			0, #width_min_max.x +rand.randf()*(width_min_max.y -width_min_max.x),
			0, #height_min_max.x+rand.randf()*(height_min_max.y-height_min_max.x),
			0, # Unused channel
			0  # Unused channel
		))
		r += theta;
		p += f;


func _init():
	rebuild()


func _ready():
	rebuild()
