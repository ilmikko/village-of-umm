tool
extends Spatial


export(float, 0.0, 10.0) var size = 0.0 setget _set_size
func _set_size(s):
	size = s
	$ShootGen.size = s


func absorb_water(litres):
	_set_size(size + litres)
