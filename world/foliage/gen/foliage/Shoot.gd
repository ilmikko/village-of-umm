tool
extends Spatial


func add_shoot_level():
#	# Create a flower or a fruit if the conditions are OK.
#	if  settings.flower_max_depth >= depth && depth >= settings.flower_min_depth:
#		# TODO: Flowers first.
#		if flowers.size() == 0:
#			var flower = FLOWER.instance()
#			flower.scale     *= settings.flower_scale
#			flower.transform  = flower.transform.translated(
#				Vector3.UP * 0.7
#			).rotated(
#				Vector3.FORWARD, PI/2.0
#			)
#			flower.settings = settings
#			flowers.append(flower)
#			$Flower.add_child(flower)
#			flower.rebuild()
	
	# Stop creating new branches if we hit max depth.
	if  depth >= settings.depth_max: return
	
	var level = (size/settings.new_shoot_threshold)
	if  level/settings.new_shoot_count > settings.new_shoot_max_level: return
	
	for i in range(settings.new_shoot_count):
		var shoot = SHOOT.instance()
		shoot.settings = settings
		shoot.depth    = depth + 1
		shoot.transform = shoot.transform.rotated(
			Vector3.UP,      settings.rot_beta
		).rotated(
			Vector3.FORWARD, settings.rot_alpha
		).rotated(
			Vector3.UP,      settings.rot_gamma * level + (TAU/settings.new_shoot_count) * i
		)
		shoots.append(shoot)
		$Shoots.add_child(shoot)
		shoot.rebuild()


func remove_shoot_level():
	var level = (size/settings.new_shoot_threshold)
	if  level/settings.new_shoot_count - 0.5 > settings.new_shoot_max_level: return
	
	for _i in range(settings.new_shoot_count):
		var shoot = shoots.pop_back()
		if  shoot == null: return
		$Shoots.remove_child(shoot)
		shoot.queue_free()


func rebuild():
	$MeshOrigin.scale = Vector3(length*settings.ratio_length_width, length, length*settings.ratio_length_width)
	$MeshOrigin/MeshInstance2.scale = Vector3(settings.leaf_width, 1, settings.leaf_height)


var size = 0.0 setget _set_size
func _set_size(s):
	if s <= 0:
		Util.remove_children($Shoots)
		Util.remove_children($Flower)
		shoots  = []
		flowers = []
		fruits  = []
		s = 0
	
	var next_threshold = (floor(size/settings.new_shoot_threshold)+1)*settings.new_shoot_threshold
	var prev_threshold = next_threshold - settings.new_shoot_threshold
	var dist  = min(abs(next_threshold-s), abs(prev_threshold-s))
	var times = abs(size-s)/settings.new_shoot_threshold - dist
	
	if size < next_threshold && s >= next_threshold:
		for _i in (times):
			add_shoot_level()
			size += settings.new_shoot_threshold
	elif size >= prev_threshold && s < prev_threshold:
		for _i in (times):
			remove_shoot_level()
			size -= settings.new_shoot_threshold
	
	size = s
	length = (s * settings.max_length * pow(settings.ratio_size, depth)) / (s + settings.max_length_curve)
	
	# Move flower attachment point.
	$Flower.transform.origin = Vector3(0, length * settings.flower_ratio * 2.0, 0)
	
	# Grow shoots.
	for i in range(shoots.size()):
		var level = floor(i/settings.new_shoot_count)
		var shoot = shoots[i]
		var nl = s      - settings.new_shoot_threshold*(level+1)
		var nh = length - settings.new_shoot_threshold*level*settings.ratio_size*0.5
		if (nl * settings.ratio_size < 0.0):
			print(nl)
		shoot.size = nl * settings.ratio_size
		shoot.transform.origin   = Vector3(0, nh, 0)
	
	# Grow flowers.
	for i in range(flowers.size()):
		var level = i
		var flower = flowers[i]
		var nl = s       - settings.new_shoot_threshold*(level+1)
		flower.size = nl * settings.ratio_size
	
	# Grow fruits.
	for i in range(fruits.size()):
		var level = i
		var fruit = fruits[i]
		var nl = s      - settings.new_shoot_threshold*(level+1)
		fruit.size = nl * settings.ratio_size
	
	rebuild()


var settings = preload("res://world/foliage/gen/foliage/settings/Default.gd").new()

var shoots  = []
var fruits  = []
var flowers = []

# How long this shoot is.
var length = 0.0

# How many branches in we are.
var depth  = 0

var SHOOT  = load("res://world/foliage/gen/shoot/Shoot.tscn")
var FRUIT  = preload("res://world/foliage/gen/fruit/FruitGen.tscn")
var FLOWER = preload("res://world/foliage/gen/flower/FlowerGen.tscn")
