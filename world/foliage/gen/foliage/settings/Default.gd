extends Node

# TODO: Clean up

var ratio_length_width = 0.05
var ratio_size = 0.5

var depth_max = 2.0

var flower_max_depth = 1.0
var flower_min_depth = 1.0
var flower_color = Color(1.0, 1.0, 1.0, 1.0)
var flower_scale = 0.04
var flower_leaf_level_max = 5.0
var flower_leaf_transform_rate = 0.03
var flower_new_leaf_threshold = 0.25
var flower_new_leaf_count = 3.0
var flower_rot_alpha = 0.7
var flower_rot_beta = 0.8

var flower_ratio = 0.55

var leaf_height = 0.5
var leaf_width = 3.5

var rot_alpha = 7.0
var rot_beta  = 7.0
var rot_gamma = 0.7

var max_length_curve = 1.5
var max_length       = 1.5

var new_shoot_count        = 2.0
var new_shoot_max_level    = 3.0
var new_shoot_origin_ratio = 0.5
var new_shoot_threshold    = 0.5
