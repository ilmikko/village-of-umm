tool
extends Spatial


func rebuild():
	ripeness = (size * settings.fruit_size_max) / (size + settings.fruit_size_max_c)
	
	scale =            Vector3(1, settings.fruit_squish, 1) * ripeness
	transform.origin = Vector3(0.0, 0.0, ripeness*settings.fruit_height_drop)
	
	$MeshInstance.mesh = FRUIT_VARIANTS[settings.fruit_variant]
	$MeshInstance.get_surface_material(0).albedo_color = lerp(
		settings.fruit_color_raw,
		settings.fruit_color_ripe,
		ripeness
	)
	
	if ripeness > settings.fruit_ripeness_harvestable_threshold:
		# TODO: Static objects considered harvestable?
		# Runtime.Expert.PhysicsBody.make_rigidbody_static(self)
		pass
	if ripeness > settings.fruit_ripeness_fall_threshold:
		var fruit = Runtime.Expert.PhysicsBody.make_rigidbody(self)
		fruit.add_to_group("food")


export(float, 0.0, 10.0) var size = 0.0 setget _set_size
func _set_size(s):
	size = s
	rebuild()


var settings = null
export(Script) var settings_script = preload("res://world/foliage/gen/fruit/settings/Default.gd") setget _set_settings
func _set_settings(s):
	settings_script = s
	settings = settings_script.new()
	if size > 0:
		Debug.warn(self, "FoliageGen settings changed after size was set")


func _init():
	if settings == null:
		settings = settings_script.new()


var ripeness = 0.0
var level    = 0.0


const FRUIT_VARIANTS = [
	preload("res://mesh/foliage/fruit/fruit0.obj"),
	preload("res://mesh/foliage/fruit/fruit1.obj"),
	preload("res://mesh/foliage/fruit/fruit2.obj"),
	preload("res://mesh/foliage/fruit/fruit3.obj"),
]
