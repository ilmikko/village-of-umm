extends Node

var fruit_color_ripe = Color(0.8, 0.0, 0.0, 1.0)
var fruit_color_raw  = Color(0.0, 0.6, 0.1, 1.0)

# How ripe the fruit needs to be to fall from the tree.
var fruit_ripeness_fall_threshold        = 0.9
# How ripe the fruit needs to be to be harvestable.
var fruit_ripeness_harvestable_threshold = 0.2

var fruit_height_drop = 0.1

var fruit_size_max   = 1.0
var fruit_size_max_c = 1.5

var fruit_squish = 1.0

var fruit_variant = 0
