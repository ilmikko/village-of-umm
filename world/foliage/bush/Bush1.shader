shader_type spatial;
render_mode cull_disabled, depth_draw_alpha_prepass;

void vertex() {
	VERTEX.x += cos(TIME * 5.0) * 0.1 * UV.x;
}

void fragment() {
	float a = cos(UV.y * 200.0);
	a = step(0.0, a);
	
	ALBEDO = vec3(0.0, UV.x, 0.0);
	ALPHA  = a;
}