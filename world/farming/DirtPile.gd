extends Spatial


var current_plant = null
var interact_hint = ""


func _ready():
	var wetness_handler = Runtime.Handler.fetch(self, Runtime.Handler.Wetness)
	wetness_handler.connect("water_released", self, "_on_WetnessHandler_water_released")


func _on_WetnessHandler_water_released(litres_released):
	if current_plant == null: return
	current_plant.absorb_water(litres_released)


func plant(seed_object):
	if current_plant != null: return
	
	var parent = seed_object.get_parent()
	if  parent != null:
		parent.remove_child(seed_object)
	add_child(seed_object)
	
	var plant = SHOOT_GEN.instance()
	plant.settings_script = seed_object.foliagegen_settings
	current_plant = plant
	add_child(plant)


const SHOOT_GEN = preload("res://world/foliage/gen/shoot/ShootGen.tscn")
