extends Spatial

var control_hint = "Open"


func control(interactor):
	$ChestController.active  = true
	$ChestController.primary = true
	$ChestController.controlled_from = interactor.global_transform.basis.z


func control_end():
	$ChestController.primary = false
	$ChestController.active  = false
