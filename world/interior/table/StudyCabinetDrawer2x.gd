extends Spatial

var control_hint = "Open"


func control(interactor):
	$DrawerController.active  = true
	$DrawerController.primary = true
	$DrawerController.controlled_from = interactor.global_transform.basis.z


func control_end():
	$DrawerController.primary = false
	$DrawerController.active  = false
