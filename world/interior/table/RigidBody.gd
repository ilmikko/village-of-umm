extends RigidBody


var intent = Vector3(0, 0, 0)


func _integrate_forces(state):
	state.apply_central_impulse(intent)
