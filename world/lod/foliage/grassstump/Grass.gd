extends Spatial

var energy = 100

func consume(amount_to_consume):
	if energy <= 1:
		return 0
	
	var amount_consumed = min(amount_to_consume, energy)
	energy = energy - amount_consumed + 1
	return amount_consumed
