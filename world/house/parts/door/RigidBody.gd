extends RigidBody


var intent_torque = Vector3(0, 0, 0)


func _integrate_forces(state):
	if intent_torque.length() == 0: return
	apply_torque_impulse(intent_torque)
