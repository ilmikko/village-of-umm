extends Spatial

var control_hint = "Move"


func control(interactor):
	$DoorController.active  = true
	$DoorController.primary = true
	$DoorController.controlled_from = interactor.global_transform.basis.z


func control_end():
	$DoorController.primary = false
	$DoorController.active  = false
