tool
extends Spatial


export(Shader) var shader setget _set_shader
func _set_shader(_shader):
	shader = _shader
	rebuild()


## WARNING, IF YOU TOUCH THIS, EDIT ALSO HeightMap.tscn shader ##

# Precomputed values for Simplex Noise
var C = Vector3(
	0.211324865405187, # (3.0-sqrt(3.0))/6.0
	0.366025403784439, # 0.5*(sqrt(3.0)-1.0)
	-0.577350269189626 # -1.0 + 2.0 * C.x
);

func mod293v3(x):
	return x - GL.floor_(x * 0.0034602076124567474) * 289.0;
func mod293v2(x):
	return x - GL.floor_(x * 0.0034602076124567474) * 289.0;
func permutev3(x):
	return mod293v3(((x*13.0)+GL.vec3(1.0))*x);

func random(xy):
	return GL.fract(sin(GL.dot(Geo.vec2xy(xy), Vector2(-41.1210058,64.121))) * 43283.2942819);

func noise(v):
	# First corner (x0)
	var i0 = GL.floor_(v + GL.vec2(GL.dot(v, GL.vec2yy(C))));
	var x0 = v - i0 + GL.vec2(GL.dot(i0, GL.vec2xx(C)));

	# Other two corners (x1, x2)
	var i1 = Vector2(1.0, 0.0);
	if (x0.x < x0.y):
		var tmp = GL.vec2yx(i1);
		i1.x = tmp.x;
		i1.y = tmp.y;
	var x1 = GL.vec2xy(x0) + GL.vec2xx(C) - i1;
	var x2 = GL.vec2xy(x0) + GL.vec2zz(C);

	# Do some permutations to avoid
	# truncation effects in permutation
	i0 = mod293v2(i0);
	var p = permutev3(
		permutev3(GL.vec3(i0.x) + GL.vec3(0.0, i1.x, 1.0))
				+ GL.vec3(i0.y) + GL.vec3(0.0, i1.y, 1.0)
	);

	# 1.0 / 13.0 =~ 0.07692
	var x  = 2.0 * GL.fract(p * GL.vec3(0.07692307692307692308)) - GL.vec3(1.0);
	var h  = GL.abs_(x) - GL.vec3(0.5);
	var a0 = x - GL.floor_(x + GL.vec3(0.5));

	var m = GL.max_(GL.vec3(0.5) - GL.vec3(GL.dot(x0,x0), GL.dot(x1,x1), GL.dot(x2,x2)), 0.0);

	m *= 5.0 * (a0*a0+h*h);

	var g = GL.vec3(0.0);
	g.x = a0.x  * x0.x  + h.x  * x0.y;
	var tmp = GL.vec2yz(a0) * Vector2(x1.x, x2.x) + GL.vec2yz(h) * Vector2(x1.y, x2.y);
	g.y = tmp.x;
	g.z = tmp.y;
	
	return 293.0 * GL.dot(m, g);

func fbm(xy):
	var amp = 1.0;
	var v = 0.0;
	var shift = Vector2(931.0, 931.0);
	# Rotate by some amount to reduce axial bias.
	var rot = GL.mat2(GL.vec2(
		cos(0.333), sin(0.333)
	), Vector2(
		-sin(0.125), cos(0.125)
	));
	for _i in range(0, 3):
		# Add values
		v += amp * noise(xy);
		# Rotate our canvas
		xy = rot * xy * 2.0 + shift;
		# Lessen amplitude of the new waves
		amp *= 0.75;
	
	return v;

func height(global, vertex):
	var amplitude = 1.5;
	var scale = 0.0005;
	var xy = global + vertex;
	return fbm(xy * scale) * amplitude;

## END WARNING ##


func rebuild():
	pass
