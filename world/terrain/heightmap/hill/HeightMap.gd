tool
extends Spatial


export(Shader) var shader setget _set_shader
func _set_shader(_shader):
	shader = _shader
	rebuild()


## WARNING, IF YOU TOUCH THIS, EDIT ALSO HeightMap.tscn shader ##

func height(_global, vertex):
	var d = vertex.length();
	return max(0.0, 1.0 - d*d*0.05);

## END WARNING ##


func rebuild():
	pass
