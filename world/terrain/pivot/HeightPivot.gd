tool
extends Spatial


export(NodePath) var terrain setget _set_terrain
func _set_terrain(_terrain):
	terrain = _terrain
	rebuild()


func rebuild():
	if !is_inside_tree():
		return
	if !terrain:
		return
	
	var terrain_node = get_node(terrain).get_node("TerrainBase")
	global_transform.origin.y = terrain_node.height(global_transform.origin)


func _init():
	set_notify_transform(true)
	rebuild()


func _notification(what):
	match what:
		NOTIFICATION_TRANSFORM_CHANGED:
			rebuild()
		NOTIFICATION_READY:
			rebuild()
