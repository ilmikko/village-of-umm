tool
extends MeshInstance


func rebuild():
	if heightmap == null:
		push_warning("TerrainBase/Mesh Tried to rebuild mesh without any heightmap!")
		return
	
	var plane = PlaneMesh.new()
	
	plane.size = size
	plane.subdivide_depth = resolution.y - 1
	plane.subdivide_width = resolution.x - 1
	
	mesh = plane
	
	mat.shader = heightmap.shader
	mat.set_shader_param("albedo_scale", albedo_scale)
	set_surface_material(0, mat)
	
	rebuild_aabb()
	rebuild_params()


func rebuild_aabb():
	# TODO: AABB should be coming from the collision shape
	var s = Vector3(5000.0, 5000.0, 5000.0)
	set_custom_aabb(AABB(global_transform.origin - s / 2.0, s))


func rebuild_params():
	mat.set_shader_param("origin", global_transform.origin)
	mat.set_shader_param("scale",  global_transform.basis.get_scale())


# TODO: Have a gradient we can use in the shader.
# Usually set externally by TerrainBase.
var albedo_scale
var heightmap
var resolution = Vector2(10.0, 10.0)
var size       = Vector2(1, 1)
var mat        = ShaderMaterial.new()
