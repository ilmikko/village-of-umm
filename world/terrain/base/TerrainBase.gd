tool
extends Spatial


func height():
	return $HeightMap.height()


func rebuild():
	if !is_inside_tree(): return
	
	if get_node_or_null("HeightMap") == null:
		push_warning("TerrainBase %s does not have a HeightMap; terrain will not work." % self)
		return
	
	rebuild_fast()
	rebuild_grass()


func rebuild_fast():
	if !is_inside_tree(): return
	
	rebuild_mesh()
	rebuild_collision()


func rebuild_grass():
	if get_node_or_null("GrassField") == null: return
	$GrassField.auto_rebuild = false
	$GrassField.heightmap = $HeightMap
	$GrassField.field_size = Geo.Vectorxz($Mesh.get_aabb().size) / 2.0
	$GrassField.rebuild()


func rebuild_collision():
	if !enable_collision: return
	$Collision.resolution = collision_resolution * size.normalized()
	$Collision.heightmap = $HeightMap
	$Collision.bounds = $Mesh.get_aabb()
	$Collision.rebuild()


func rebuild_mesh():
	$Mesh.resolution = mesh_resolution * size.normalized()
	$Mesh.size = size
	$Mesh.heightmap = $HeightMap
	$Mesh.albedo_scale = albedo_scale
	$Mesh.rebuild()


func rebuild_mesh_params():
	$Mesh.rebuild_params()


func _init():
	if Engine.is_editor_hint():
		set_notify_transform(true)
	rebuild()


func _notification(what):
	match what:
		NOTIFICATION_TRANSFORM_CHANGED:
			rebuild_fast()
		NOTIFICATION_READY:
			rebuild()


export(Vector2) var size = Vector2(1, 1) setget _set_size
func _set_size(r):
	size = r
	rebuild_fast()


export(bool) var enable_collision = true setget _set_enable_collision
func _set_enable_collision(ec):
	enable_collision = ec
	rebuild_collision()


export(float, 1, 250) var mesh_resolution = 10.0 setget _set_mesh_resolution
func _set_mesh_resolution(mr):
	mesh_resolution = mr
	rebuild_fast()


export(float, 2, 250) var collision_resolution = 10.0 setget _set_collision_resolution
func _set_collision_resolution(cr):
	collision_resolution = cr
	rebuild_fast()


export(Texture) var albedo_scale setget _set_albedo_scale
func _set_albedo_scale(als):
	albedo_scale = als
	rebuild_fast()
