tool
extends StaticBody

# Usually set externally by TerrainBase.
var resolution = Vector2(10, 10)
var heightmap = null
var bounds


func rebuild():
	var collision = $Shape
	
	if !enabled:
		collision.shape = null
		return
	
	if heightmap == null:
		push_warning("TerrainBase/Collision Tried to rebuild collision shape without any heightmap!")
		return
	
	# Calculate collision heightmap shape based on heightmap.
	var res = resolution.floor()
	var resp1 = res + Vector2(1, 1)
	
	var global = global_transform.origin / global_transform.basis.get_scale()
	global = Geo.Vectorxz(global)
	
	# Godot doesn't like when heightmap values are in the extremes; it messes
	# with the AABB collision detection. We can cheat a bit by finding the max
	# and min points of collision, and then artificially moving the shape closer
	# to the StaticBody to prevent this.
	var points = []
	var height_max = -INF
	var height_min = +INF
	for z in range(0, resp1.y):
		for x in range(0, resp1.x):
			var vertex = Vector2(x, z) / res - Vector2(0.5, 0.5)
			vertex *= Geo.Vectorxz(bounds.size)
			
			var height = heightmap.height(global, vertex)
			
			if height > height_max:
				height_max = height
			
			if height < height_min:
				height_min = height
			
			points.append(height)
	
	var delta    = (height_max - height_min) / 2.0
	var midpoint = height_min + delta
	
	for i in range(points.size()):
		points[i] = points[i] - midpoint
	
	var shape = HeightMapShape.new()
	shape.map_width = resp1.x # X
	shape.map_depth = resp1.y # Z
	shape.map_data = PoolRealArray(points)
	
	collision.shape = shape
	var s = Geo.Vectorxz(bounds.size) / res
	collision.scale = Geo.Vectorx0y(s) + Vector3(0, 1, 0)
	transform.origin = Vector3(0, height_min + delta, 0)


export var enabled = true setget _set_enabled
func _set_enabled(e):
	enabled = e
	rebuild()
