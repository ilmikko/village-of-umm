tool
extends Spatial


export(Shader) var shader setget _set_shader
func _set_shader(_shader):
	shader = _shader
	rebuild()

## WARNING, IF YOU TOUCH THIS, EDIT ALSO HeightMap.tscn shader ##

func height(global, vertex):
	return 0;

## END WARNING ##

func rebuild():
	pass
