tool
extends RigidBody

var interact_hint = "Pickup"

var NodeMatcher = preload("res://script/class/NodeMatcher.gd")

func rebuild():
	if !is_inside_tree(): return
	$Water.visible = has_water


func handle_interaction_by_actor(actor):
	var held_node_matcher = NodeMatcher.new()
	held_node_matcher.matching_node = self
	actor.InteractionNotifier.add_interaction("secondary", self, null, held_node_matcher)



func interaction_triggered(actor, object, interactor_name):
	if object == null:
		_set_has_water(false)
		return
	
	if has_water:
		Debug.print(self, "POURED ONTO SOMETHING %s" % object.get_path())
		var wetness_handler = Runtime.Handler.fetch(object, Runtime.Handler.Wetness)
		wetness_handler.add_litres_of_water(5)
		_set_has_water(false)
		return

	if object.is_in_group("water"):
		_set_has_water(true)


func _ready():
	rebuild()


export var has_water = false setget _set_has_water
func _set_has_water(hw):
	has_water = hw
	rebuild()
