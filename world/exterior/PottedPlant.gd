extends RigidBody


func _ready():
	var wetness_handler = Runtime.Handler.fetch(self, Runtime.Handler.Wetness)
	wetness_handler.connect("water_released", self, "_on_WetnessHandler_water_released")


func _on_WetnessHandler_water_released(litres_released):
	$FoliageGen.absorb_water(litres_released)
