extends RigidBody


export var HIT_IMPULSE_AMOUNT = 45.0
export var HIT_DAMAGE = 0.25


var primed = false


func hit(body):
	# If the axe wasn't primed, the hit won't make any damage.
	if !primed: return
	primed = false
	
	Runtime.Expert.ToolHit.hit_transform(self, Util.get_root_parent(body),
		$CollisionShape/MeshInstance/Head/CollisionShape.global_transform)


func play_hit_sound(sound):
	var player = $CollisionShape/MeshInstance/Head/AudioStreamPlayer3D
	if sound != player.stream:
		player.stream = sound
	player.play()


func _on_Area_body_entered(body):
	hit(body)
