tool
extends Spatial


export(Curve) var ambient_curve setget _set_ambient_curve
func _set_ambient_curve(_ambient_curve):
	ambient_curve = _ambient_curve
	rebuild()


export(float, 0, 180) var sky_fov = 70.0 setget _set_sky_fov
func _set_sky_fov(_sky_fov):
	sky_fov = _sky_fov
	rebuild()


export(Curve) var energy_curve setget _set_energy_curve
func _set_energy_curve(_energy_curve):
	energy_curve = _energy_curve
	rebuild()


export(Curve) var ground_color_curve setget _set_ground_color_curve
func _set_ground_color_curve(_ground_color_curve):
	ground_color_curve = _ground_color_curve
	rebuild()


export(Gradient) var ground_color_cycle setget _set_ground_color_cycle
func _set_ground_color_cycle(_ground_color_cycle):
	ground_color_cycle = _ground_color_cycle
	rebuild()


export(Gradient) var horizon_bottom_color_cycle setget _set_horizon_bottom_color_cycle
func _set_horizon_bottom_color_cycle(_horizon_bottom_color_cycle):
	horizon_bottom_color_cycle = _horizon_bottom_color_cycle
	rebuild()


export(Gradient) var horizon_top_color_cycle setget _set_horizon_top_color_cycle
func _set_horizon_top_color_cycle(_horizon_top_color_cycle):
	horizon_top_color_cycle = _horizon_top_color_cycle
	rebuild()


export(Curve) var sky_color_curve setget _set_sky_color_curve
func _set_sky_color_curve(_sky_color_curve):
	sky_color_curve = _sky_color_curve
	rebuild()


export(Gradient) var sky_color_cycle setget _set_sky_color_cycle
func _set_sky_color_cycle(_sky_color_cycle):
	sky_color_cycle = _sky_color_cycle
	rebuild()


export(Gradient) var sun_color_cycle setget _set_sun_color_cycle
func _set_sun_color_cycle(_sun_color_cycle):
	sun_color_cycle = _sun_color_cycle
	rebuild()


export(Gradient) var sun_light_color_cycle setget _set_sun_light_color_cycle
func _set_sun_light_color_cycle(_sun_light_color_cycle):
	sun_light_color_cycle = _sun_light_color_cycle
	rebuild()


export(Curve) var sun_curve setget _set_sun_curve
func _set_sun_curve(_sun_curve):
	sun_curve = _sun_curve
	rebuild()


export(float, 0, 1) var phase = 0.0 setget _set_phase
func _set_phase(_phase):
	phase = _phase
	rebuild_auto()


export(float, 0, 1) var phase_year = 0.0 setget _set_phase_year
func _set_phase_year(_phase_year):
	phase_year = _phase_year
	rebuild_auto()


export(bool) var auto_rebuild = true setget _set_auto_rebuild
func _set_auto_rebuild(_auto_rebuild):
	auto_rebuild = _auto_rebuild
	rebuild_auto()


export var HDR_LUMINOSITY_MIN = 2.0
export var HDR_LUMINOSITY_MAX = 2.3
export var HDR_ADJUST_SPEED   = 0.1
export var HDR_INTENSITY      = 1.2

export var HDR_GLOW_INTENSITY = 2.2
export var HDR_GLOW_THRESHOLD = 0.96


var environment : Environment
var light       : DirectionalLight
var sky         : ProceduralSky


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_slow()
	rebuild_fast()


func rebuild_ambient():
	environment.ambient_light_energy = ambient_curve.interpolate(phase)


func rebuild_auto():
	if !auto_rebuild: return
	rebuild()


func rebuild_fast():
	rebuild_light()


func rebuild_fog():
	environment.fog_color = horizon_bottom_color_cycle.interpolate(phase)


func rebuild_light():
	rebuild_once_light()
	
	var rot = Vector3(-1, 0, 0) * PI * 0.5
	
	# Latitude
	rot += Vector3(1, 0, 0) * TAU * 0.22 * sin(phase * TAU)
	# Longitude
	rot += Vector3(0, -1, 0) * TAU * phase_year
	
	light.rotation    = rot
	light.light_color = sun_light_color_cycle.interpolate(phase)
	light.light_energy = energy_curve.interpolate(phase)


func rebuild_once_environment():
	if environment: return
	environment = Environment.new()
	
	environment.background_mode = Environment.BG_SKY
	environment.auto_exposure_enabled = true
	environment.glow_enabled = true
	environment.fog_enabled = true
	
	# Fog settings.
	environment.fog_depth_enabled = true
	environment.fog_depth_begin = 256.0
	environment.fog_depth_end = 1024.0
	environment.fog_transmit_enabled = true
	
	$WorldEnvironment.environment = environment


func rebuild_once_light():
	if light: return
	light = $DirectionalLight
	light.shadow_enabled = true


func rebuild_once_sky():
	if sky: return
	sky = ProceduralSky.new()
	sky.radiance_size = Sky.RADIANCE_SIZE_32
	sky.texture_size  = ProceduralSky.TEXTURE_SIZE_256
	environment.background_sky = sky


func rebuild_once_settings():
	rebuild_once_environment()
	rebuild_once_settings_background()
	rebuild_once_settings_hdr()


func rebuild_once_settings_background():
	environment.background_energy = energy_curve.interpolate(phase)
	environment.background_sky_custom_fov = sky_fov


func rebuild_once_settings_hdr():
	environment.auto_exposure_min_luma = HDR_LUMINOSITY_MIN
	environment.auto_exposure_max_luma = HDR_LUMINOSITY_MAX
	environment.auto_exposure_speed    = HDR_ADJUST_SPEED
	environment.auto_exposure_scale    = HDR_INTENSITY
	rebuild_once_settings_hdr_glow()


func rebuild_once_settings_hdr_glow():
	environment.glow_intensity     = HDR_GLOW_INTENSITY
	environment.glow_strength      = 1.0
	environment.glow_hdr_threshold = HDR_GLOW_THRESHOLD


func rebuild_slow():
	rebuild_once_settings()
	rebuild_ambient()
	rebuild_fog()
	rebuild_sky()
	rebuild_sky_ground()
	rebuild_sky_sun()


func rebuild_sky():
	rebuild_once_sky()
	sky.sky_curve         = sky_color_curve.interpolate(phase)
	sky.sky_top_color     = sky_color_cycle.interpolate(phase)
	sky.sky_horizon_color = horizon_top_color_cycle.interpolate(phase)


func rebuild_sky_ground():
	sky.ground_curve         = ground_color_curve.interpolate(phase)
	sky.ground_bottom_color  = ground_color_cycle.interpolate(phase)
	sky.ground_horizon_color = horizon_bottom_color_cycle.interpolate(phase)


func rebuild_sky_sun():
	sky.sun_color  = sky_color_cycle.interpolate(phase) + sun_color_cycle.interpolate(phase)
	sky.sun_curve  = sun_curve.interpolate(phase)
	sky.sun_energy = energy_curve.interpolate(phase)
	
	# Sun needs to come up from the horizon, go down, come up again (as moon), and finally go down in the horizon.
	sky.sun_latitude = (sin(phase * TAU)*0.5 + 0.5) * 180
	# Longitude depends on which time of year it is (rotation around the sun).
	sky.sun_longitude = phase_year * 360


func _ready():
	rebuild()
