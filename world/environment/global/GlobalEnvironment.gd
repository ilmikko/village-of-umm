tool
extends Spatial

# Global environment should concern the overall ("planet-wide") metrics such as
# time, temperature, humidity, wind speed...

# We might eventually want to branch some of these off to regionalised
# environments, but given we only have a single village I'd say keep them global
# for now. Time is probably always global :P

# There are two ways to use this.
# 1 (preferred): Subscribe to updates using the signals provided. Any time there
# is a "significant" change to these metrics, your node gets notified.
# 2 (might be sometimes necessary, but please consider 1 first): Poll the
# "current" metric as it is right now. Usually this is connected to a timer, so
# make sure to ask yourself why this timer shouldn't live within
# GlobalEnvironment and send signals instead for updates.


# TODO: these are not gametime specific, they are specific to the GlobalEnvironment.
# How long it takes, in seconds, for one full "yearly" orbit of the planet.
export var PLANETARY_ORBIT_DURATION = 60 * 24 * 365
# How long it takes, in seconds, for one full "daily" rotation of the planet.
export var PLANETARY_REVOLUTION_DURATION = 60 * 24


export(int, 1, 365) var day_of_year = 1 setget _set_day_of_year
func _set_day_of_year(_day_of_year):
	day_of_year = _day_of_year
	rebuild()

export(int, 1, 24)  var hour_of_day = 1 setget _set_hour_of_day
func _set_hour_of_day(_hour_of_day):
	hour_of_day = _hour_of_day
	rebuild()


var planetary_orbit_01 = 0
var planetary_revolution_01 = 0


# current_time returns the current time in a [H,M,S] format.
func current_time():
	var hours   = int(24           * planetary_revolution_01)
	var minutes = int(60 * 24      * planetary_revolution_01) % 60
	var seconds = int(60 * 60 * 24 * planetary_revolution_01) % 60
	return [hours, minutes, seconds]


func rebuild():
	if !is_inside_tree(): return
	if !Engine.is_editor_hint():
		$GlobalLighting.auto_rebuild = false
	$GlobalLighting.rebuild()
	
	Runtime.Game.Time.connect("at_least_5_0_seconds_passed", self, "_on_at_least_5_0_seconds_passed")
	Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")
	Runtime.Game.Time.now = (float(day_of_year) / 365) * PLANETARY_ORBIT_DURATION + (float(hour_of_day) / 24) * PLANETARY_REVOLUTION_DURATION
	 
	rebuild_orbit()


func rebuild_orbit():
	var now = Runtime.Game.Time.now
	var mod_orbit           = now % PLANETARY_ORBIT_DURATION
	var mod_revolution      = now % PLANETARY_REVOLUTION_DURATION
	
	planetary_orbit_01      = float(mod_orbit)      / PLANETARY_ORBIT_DURATION
	planetary_revolution_01 = float(mod_revolution) / PLANETARY_REVOLUTION_DURATION
	
	$GlobalLighting.phase      = planetary_revolution_01
	$GlobalLighting.phase_year = planetary_orbit_01
	
	Debug.text(self, "Current time", "%d" % now)
	Debug.text(self, "Current orbit phase", planetary_orbit_01)
	Debug.text(self, "Current revolution phase", planetary_revolution_01)


func _on_at_least_0_1_seconds_passed(_delta):
	rebuild_orbit()
	$GlobalLighting.rebuild_fast()


func _on_at_least_5_0_seconds_passed(_delta):
	$GlobalLighting.rebuild_slow()


func _ready():
	rebuild()
