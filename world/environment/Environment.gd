tool
extends Spatial

export(Gradient) var color_cycle_ambient : Gradient setget _set_color_cycle_ambient
func _set_color_cycle_ambient(_color_cycle_ambient):
	color_cycle_ambient = _color_cycle_ambient
	rebuild()

export(Gradient) var color_cycle_sky : Gradient setget _set_color_cycle_sky
func _set_color_cycle_sky(_color_cycle_sky):
	color_cycle_sky = _color_cycle_sky
	rebuild()

func rebuild():
	if !is_inside_tree():
		return

	$WorldEnvironment.environment.ambient_light_color = color_cycle_ambient.interpolate(0.0)
	$Sun.rebuild()
