tool
extends Spatial

export(float) var color setget _set_color
func _set_color(_color):
	color = _color
	rebuild()

export(float) var intensity = 2.2 setget _set_intensity
func _set_intensity(_intensity):
	intensity = _intensity
	rebuild()

export(float) var latitude setget _set_latitude
func _set_latitude(_latitude):
	latitude = _latitude
	rebuild()

export(float) var longitude setget _set_longitude
func _set_longitude(_longitude):
	longitude = _longitude
	rebuild()

export(Gradient) var color_cycle_sun : Gradient setget _set_color_cycle_sun
func _set_color_cycle_sun(_color_cycle_sun):
	color_cycle_sun = _color_cycle_sun
	rebuild()

func rebuild():
	$DirectionalLight.rotation = Vector3(0.0, 0.0, 0.0);
