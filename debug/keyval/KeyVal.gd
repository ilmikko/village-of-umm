extends Control


var data = {}


func put(key, val):
	data[key] = val
	render()


func key_val_to_text(kv):
	var s = ""
	for key in kv:
		s += key + ": " + str(kv[key]) + "\n"
	return s


func render():
	$Label.text = key_val_to_text(data)
