tool
extends Spatial

export(Color) var color = Color(0.1, 0.2, 0.8, 1.0) setget _set_color
func _set_color(_color):
	color = _color
	rebuild()

var mat

func rebuild():
	mat = $MeshInstance.get_surface_material(0).duplicate()
	$MeshInstance.set_surface_material(0, mat)
	mat.set_shader_param('color', color)
