extends Control

var index = 0
var points = 0

func plot(i, vec):
	if i < points:
		$Panel/Line2D.remove_point(i)
	$Panel/Line2D.add_point(vec, i)
	if i > rect_size.x:
		i = 0
	index = i
	points += 1


func plot_next(y):
	plot(index + 1, Vector2(index, y))


var color setget _set_color
func _set_color(c):
	if color == c: return
	color = c
	$Panel/Line2D.default_color = color
	$Label.modulate = color


var title setget _set_title
func _set_title(t):
	if title == t: return
	title = t
	$Label.text = t


var width setget _set_width
func _set_width(w):
	if width == w: return
	width = w
	$Panel/Line2D.width = w
