extends Control


var data = []


func print(line):
	data.append(line)
	render()


func key_val_to_text(kv):
	var s = ""
	for key in kv:
		s += str(key) + "\n"
	return s


func render():
	$Label.text = key_val_to_text(data)
