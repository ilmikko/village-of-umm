extends "res://serializers/node/NodeSerializer.gd"

func _init(node).(node): pass

func serialize():
	var data = .serialize()
	
	data["title"] = node.title
	data["id"] = node.id
	data["entries"] = node.entries
	
	return data


func deserialize(data):
	.deserialize(data)
	node.title = data["title"]
	node.id = int(data["id"])
	node.entries = data["entries"]


static func from_data(data):
	var journal_page = load("res://script/class/JournalPage.gd").new(data["title"], data["entries"])
	journal_page.id = data["id"]
	return load("res://serializers/JournalPageSerializer.gd").new(journal_page)
