extends Node

var node

func _init(new_node):
	self.node = new_node


func serialize():
	var data = {
		"node_path": node.get_path(),
		"serializer": self.script.get_path(),
		"tscn_file": node.filename,
	}

	if node.script: 
		data["node_script"] = node.script.get_path()

	return data


func deserialize(_data):
	pass


static func from_data(data):
	var n = load(data["tscn_file"]).instance()
	var serializer = load(data["serializer"]).new(n)
	serializer.deserialize(data)
	return serializer


func vector3_to_array(vector):
	return [vector.x, vector.y, vector.z]


func array_to_vector3(array):
	return Vector3(array[0], array[1], array[2])


func basis_to_array(basis):
	return [
		vector3_to_array(basis.x),
		vector3_to_array(basis.y),
		vector3_to_array(basis.z)
	]


func array_to_basis(array):
	return Basis(
		array_to_vector3(array[0]),
		array_to_vector3(array[1]),
		array_to_vector3(array[2])
	)
