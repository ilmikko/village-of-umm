extends "res://serializers/node/NodeSerializer.gd"

func _init(node).(node): pass

func serialize():
	var data = .serialize()
	data["transform_origin"] = vector3_to_array(node.transform.origin)
	data["transform_basis"] = basis_to_array(node.transform.basis)

	return data


func deserialize(data):
	node.transform.origin = array_to_vector3(data["transform_origin"])
	node.transform.basis = array_to_basis(data["transform_basis"])
