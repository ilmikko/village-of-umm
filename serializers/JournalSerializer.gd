extends "res://serializers/node/NodeSerializer.gd"


func _init(node).(node): pass

func serialize():
	var data = .serialize()
	data["archived"] = node.archived_ids
	data["active"] = node.active_ids
	return data


func deserialize(data):
	.deserialize(data)
	node.active_ids = Util.convert_to_ints(data["active"])
	node.archived_ids = Util.convert_to_ints(data["archived"])
