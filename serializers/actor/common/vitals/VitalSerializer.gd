extends "res://serializers/node/NodeSerializer.gd"


func _init(node).(node): pass

func serialize():
	var data = .serialize()

	data["value"] = node.value
	data["decay_per_time_unit"] = node.decay_per_time_unit
	data["time_unit"] = node.time_unit
	data["state_names"] = node.state_names

	return data

func deserialize(data):
	.deserialize(data)
	node.value = data["value"]
	node.decay_per_time_unit = data["decay_per_time_unit"]
	node.time_unit = int(data["time_unit"])
	node.state_names = data["state_names"]
