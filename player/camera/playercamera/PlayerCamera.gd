extends Spatial


func _enter_tree():
	Runtime.Expert.Camera.set_active_player_camera(self)
	Runtime.Expert.Camera.add_viewport_camera($SimpleCamera.Camera)
	# We can only fetch the node once we're in the tree.
	# Assumes the node is in the parent tree.
	if base_controller == null && base_controller_path != null:
		base_controller = get_node(base_controller_path)


func _exit_tree():
	Runtime.Expert.Camera.remove_viewport_camera($SimpleCamera.Camera)


func show_interact_hint(item):
	Runtime.UI.Hint.center_label = Runtime.Expert.InteractHint.object_pointing_at(
		base_controller.linked_controller, item)


func hide_interact_hint():
	Runtime.UI.Stats.visible = false
	Runtime.UI.Hint.center_label = ""


var Interactor setget , _get_Interactor
func _get_Interactor(): return $Interactor


var Pointer setget , _get_Pointer
func _get_Pointer(): return $Pointer


var Camera setget , _get_Camera
func _get_Camera(): return $SimpleCamera.Camera


# TODO: This should not live here; we should put this into UICameraEffect.
var BookOpenable setget , _get_BookOpenable
func _get_BookOpenable(): return $BookOpenable


var base_controller = null setget _set_base_controller
func _set_base_controller(c):
	base_controller      = c
	base_controller_path = null
export(NodePath) var base_controller_path = null setget _set_base_controller_path
func _set_base_controller_path(cp):
	base_controller      = get_node(cp)
	base_controller_path = cp


func _on_Interactor_pointing_at(object):
	show_interact_hint(object)


func _on_Interactor_pointing_nowhere():
	hide_interact_hint()
