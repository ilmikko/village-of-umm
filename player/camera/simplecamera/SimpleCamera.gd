extends Spatial

var camera_expert = Runtime.Expert.Camera


func _ready():
	$Camera.far  = camera_expert.clip_far
	$Camera.near = camera_expert.clip_near
	_set_fov(camera_expert.fov)


export(float, 1.0, 179.0) var fov = camera_expert.fov setget _set_fov
func _set_fov(f):
	fov = f
	$Camera.fov = fov


var Camera setget , _get_Camera
func _get_Camera(): return $Camera
