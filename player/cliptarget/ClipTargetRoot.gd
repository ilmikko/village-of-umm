tool
extends Spatial


var margin_perc = 1.0


func rebuild_margin_distance():
	margin_perc = 1.0 - margin/distance
	$RayCast.cast_to = Vector3(0, 0, distance + margin)


func rebuild_cliptarget():
	if !is_inside_tree(): return
	if get_node_or_null("ClipTarget") == null:
		Debug.warn(self, "does not have a ClipTarget to modify.")
		return
	
	if $RayCast.is_colliding():
		# Check whether we're within margin.
		# Readjust cliptarget position.
		var collision = $RayCast.get_collision_point()
		var origin = global_transform.origin
		var delta = origin - collision
		
		$ClipTarget.intent_distance = delta.length() * margin_perc
	else:
		$ClipTarget.intent_distance = distance


func _enter_tree():
	Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


## Distance is the maximum distance a ClipTarget will stay from the ClipTargetRoot.
export(float, 0.0, 10.0) var distance = 5.0 setget _set_distance
func _set_distance(d):
	distance = d
	rebuild_margin_distance()

## Margin is additional distance for the raycast in order to react to objects
## behind the ClipTargetRoot.
export(float, 0.0, 5.0) var margin = 1.0 setget _set_margin
func _set_margin(m):
	margin = m
	rebuild_margin_distance()


func _on_at_least_0_1_seconds_passed(_t):
	rebuild_cliptarget()
