tool
extends Spatial


var DISTANCE_FALLOFF = 0.25
var intent_distance = 0.0


# TODO: This is a visual effect; is process the best place to do this?
func _process(_delta):
	if intent_distance == 0.0: return
	
	var current_distance = transform.origin.z
	var distance_delta = intent_distance - current_distance

	transform.origin.z = current_distance + distance_delta * DISTANCE_FALLOFF
