extends Spatial


signal pointing_at(object)
signal pointing_nowhere()

# TODO: There should be ways here to do this without the timer.
# TODO: Then we can also get rid of last_detected_collider as it only fires when
# TODO: needed.

## Distance is the maximum distance that the Interactor can interact with objects.
export(float, 0.0, 10.0) var distance = 5.0 setget _set_distance
func _set_distance(_distance):
	distance = _distance
	$BodyRayCast.cast_to = Vector3(0, 0, -distance)
	$AreaRayCast.cast_to = Vector3(0, 0, -distance)


func _enter_tree():
	Runtime.Game.Time.connect("at_least_0_5_seconds_passed", self, "_on_at_least_0_5_seconds_passed")


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_0_5_seconds_passed", self, "_on_at_least_0_5_seconds_passed")


func raycast():
	var object = ray_object()
	
	Debug.text(self, "ray_object", object)
	
	if object == null:
		pointing_nowhere()
		return
	
	pointing_at(object)


func pointing_at(object):
	emit_signal("pointing_at", object)


func pointing_nowhere():
	emit_signal("pointing_nowhere")


func ray_object():
	var collider = $BodyRayCast.get_collider()

	if  collider == null:
		collider = $AreaRayCast.get_collider()

	Debug.text(self, "Collider path is", collider)
	if  collider == null:
		return null

	var collider_root = Util.get_root_parent(collider)
	Debug.text(self, "Collider colliding with", collider_root)
	return collider_root
	

func _on_at_least_0_5_seconds_passed(_t):
	raycast()
