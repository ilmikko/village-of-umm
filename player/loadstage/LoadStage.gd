tool
extends Spatial


signal stage_load_started()
signal stage_load_ended()


signal stage_unload_started()
signal stage_unload_ended()


func stage_load():
	emit_signal("stage_load_started")
	if scene != null:
		var instance = yield(Runtime.Expert.LoadingInstance.instance(scene), Runtime.Expert.LoadingInstance.INSTANCED)
		$Instances.add_child(instance)
	emit_signal("stage_load_ended")


func stage_unload():
	emit_signal("stage_unload_started")
	# Remove placeholder if it exists.
	var placeholder = get_node_or_null(scene_placeholder)
	if  placeholder != null:
		placeholder.queue_free()
	
	# Remove scene instance if it exists.
	Util.remove_children($Instances)
	emit_signal("stage_unload_ended")


export(NodePath)    var scene_placeholder
export(PackedScene) var scene
