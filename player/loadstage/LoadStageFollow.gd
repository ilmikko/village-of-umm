tool
extends Spatial


# TODO: We should have the ability to follow in the other order when unloading.
# ....: This will probably require quite a bit of refactoring.


func stage_load():
	if !is_inside_tree(): return
	
	var parent = get_parent()
	if !parent.is_in_group("load_stage"):
		Debug.warn(self, "%s does not have LoadStage as parent, but has %s!" % [self, parent])
		return
	
	if !reversed:
		parent.stage_load()
	else:
		parent.stage_unload()


func stage_unload():
	if !is_inside_tree(): return
	
	var parent = get_parent()
	if !parent.is_in_group("load_stage"):
		Debug.warn(self, "%s does not have LoadStage as parent, but has %s!" % [self, parent])
		return
	
	if !reversed:
		parent.stage_unload()
	else:
		parent.stage_load()


func _ready():
	var leader = get_node_or_null(leader_path)
	if  leader == null:
		Debug.warn(self, "LoadStageFollow failed, leader was null!")
		return
	if !leader.is_in_group("load_stage"):
		Debug.warn(self, "LoadStageFollow failed, leader was not a LoadStage!")
		return
	
	if skip_load:
		leader.connect("stage_load_started",   self, "_on_leader_stage_load_started")
	else:
		leader.connect("stage_load_ended",     self, "_on_leader_stage_load_ended")
	
	if skip_unload:
		leader.connect("stage_unload_started", self, "_on_leader_stage_unload_started")
	else:
		leader.connect("stage_unload_ended",   self, "_on_leader_stage_unload_ended")


# Whether the leader Load causes an Unload rather than a Load.
export(bool) var reversed = false


# Whether to entirely skip to sync with the leader to finish loading, before
# loading ourselves.
# Can cause flashing and bugs. Use only if you know what you're doing.
export(bool) var skip_load = false


# Whether to entirely skip to sync with the leader to finish unloading, before
# unloading ourselves.
# Can cause flashing and bugs. Use only if you know what you're doing.
export(bool) var skip_unload = true


export(NodePath) var leader_path setget _set_leader_path
func _set_leader_path(l):
	leader_path = l


func _on_leader_stage_load_started():
	stage_load()


func _on_leader_stage_unload_started():
	stage_unload()


func _on_leader_stage_load_ended():
	stage_load()


func _on_leader_stage_unload_ended():
	stage_unload()
