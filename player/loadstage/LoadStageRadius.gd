tool
extends Spatial


func observed():
	if !is_inside_tree(): return
	
	var parent = get_parent()
	if !parent.is_in_group("load_stage"):
		Debug.warn(self, "%s does not have LoadStage as parent, but has %s!" % [self, parent])
		return
	
	if !reversed:
		parent.stage_load()
	else:
		parent.stage_unload()


func forgotten():
	if !is_inside_tree(): return
	
	var parent = get_parent()
	if !parent.is_in_group("load_stage"):
		Debug.warn(self, "%s does not have LoadStage as parent, but has %s!" % [self, parent])
		return
	
	if !reversed:
		parent.stage_unload()
	else:
		parent.stage_load()

# Whether entering this Event causes an Unload rather than a Load.
export(bool) var reversed = false


export(float, 0.1, 1000.0) var radius = 1000.0 setget _set_radius
func _set_radius(r):
	radius = r
	$Observable.radius = r


func _on_Observable_observed(_position):
	observed()


func _on_Observable_forgotten(_position):
	forgotten()
