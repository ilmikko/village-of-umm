tool
extends Spatial


signal pointing_at(position)
signal pointing_nowhere()

## Distance is the maximum distance that the Interactor can interact with objects.
export(float, 0.0) var distance = 5 setget _set_distance
func _set_distance(_distance):
	distance = _distance
	$BodyRayCast.cast_to = Vector3(0, 0, -distance)


func _exit_tree():
	$Timer.stop()


func check_ray_point():
	var point = ray_point()

	if !is_colliding():
		pointing_nowhere()
		return
	
	pointing_at(point)


func pointing_at(point):
	emit_signal("pointing_at", point)


func pointing_nowhere():
	emit_signal("pointing_nowhere")


func ray_point():
	return $BodyRayCast.get_collision_point()


func is_colliding():
	return $BodyRayCast.is_colliding()


func _on_Timer_timeout():
	check_ray_point()
