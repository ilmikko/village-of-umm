extends Spatial

signal rotated(basis)


# TODO: Set this elsewhere (Game.settings?)
const MOUSE_SPEED = 0.01


var   basis_pitch = 0
var   basis_yaw   = 0


export(Basis) var basis setget, _get_basis
func _get_basis():
	return Basis().rotated(
		Vector3(1.0, 0.0, 0.0), basis_pitch
	).rotated(
		Vector3(0.0, 1.0, 0.0), basis_yaw
	)


func rotated():
	emit_signal("rotated", _get_basis())


func _input(event):
	if event is InputEventMouseMotion:
		basis_yaw   += (-event.relative.x/TAU) * MOUSE_SPEED
		basis_pitch += (-event.relative.y/TAU) * MOUSE_SPEED
		rotated()


func _ready():
	rotated()
