tool
extends Spatial


export(int, 1, 5) var expand_count = 1 setget _set_expand_count
func _set_expand_count(_expand_count):
	expand_count = _expand_count
	rebuild()


export(float, 0.1, 100.0) var size = 1.0 setget _set_size
func _set_size(_size):
	size = _size
	rebuild()


export(Vector3) var cell_position = Vector3(0, 0, 0) setget _set_cell_position
func _set_cell_position(_cell_position):
	cell_position = _cell_position
	rebuild_districts()


var DEBUG_BOX = preload("res://debug/visual/Box.tscn")

var districts = {}


func clear_districts():
	for key in districts:
		Util.remove(districts[key])
	districts = {}


func district_del(pos, delta):
	var global_pos = pos + delta
	var key = district_key(global_pos)
	if districts.has(key):
		Util.remove(districts[key])
		districts.erase(key)


func district_add(pos, delta):
	var box = DEBUG_BOX.instance()
	var d = float(max(abs(delta.x), max(abs(delta.y), abs(delta.z))))/expand_count
	box.color = Color(1-d, 0, 1, 0.05*d)
	var global_pos = pos + delta
	box.transform.origin = global_pos * size
	box.scale = Vector3(1, 1, 1) * size * 0.5;
	districts[district_key(global_pos)] = box
	$DebugView.add_child(box)
	return box


func district_key(pos) -> String:
	pos = Geo.clean_zeroes(pos)
	return "%s,%s,%s" % [pos.x, pos.y, pos.z]


# vec is an integer vector telling the new position.
func district_change(vec):
	if cell_position == vec: return
	var delta = vec-cell_position
	Debug.text(self, "%s District Rebuild" % self.name, delta)
	
	# We compute a simple delta of the grid changes.
	# TODO: This could be probably extracted to a library; I didn't want to do
	# that because this isn't 3d-generic (this implementation is y-bound).
	# But if we want to do a space game this should work very similarly.
	var district_delta = {}
	
	var old_districts = {}
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			old_districts[district_key(cell_position + Vector3(x, 0, y))] = cell_position + Vector3(x, 0, y)
	
	var new_districts = {}
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			new_districts[district_key(vec + Vector3(x, 0, y))] = vec + Vector3(x, 0, y)
	
	var removals = []
	var additions = []
	
	for id in old_districts:
		# No-op, remains visible.
		if new_districts.has(id):
			continue
		removals.append(old_districts[id])
	
	for id in new_districts:
		# No-op, is already visible.
		if old_districts.has(id):
			continue
		additions.append(new_districts[id])
	
	# Apply deltas.
	for d in additions:
		district_add(d, Vector3(0, 0, 0))
	for d in removals:
		district_del(d, Vector3(0, 0, 0))
	
	# Debug: color the box we're currently in differently from the rest.
	Debug.text(self, "NEW VEC POS", vec)
	if districts.has(district_key(cell_position)):
		districts[district_key(cell_position)].color = Color(0, 0, 0, 0.1)
	if districts.has(district_key(vec)):
		districts[district_key(vec)].color = Color(1, 0, 0, 0.1)
	
	cell_position = vec


func rebuild_districts():
	districts = {}
	Util.remove_children($DebugView)
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			district_add(cell_position, Vector3(x, 0, y))


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_districts()


func _ready():
	rebuild()
