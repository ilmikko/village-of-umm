tool
extends Spatial


export(float, 0.0, 200.0) var far = 100.0 setget _set_far
func _set_far(_far):
	far = _far
	rebuild()


export(float, 0.0, 100.0) var near = 50.0 setget _set_near
func _set_near(_near):
	near = _near
	rebuild()


export var active = false setget _set_active
func _set_active(_active):
	active = _active
	rebuild()


var update_subscribers = {}


# TODO: We should also have a Timer for observers that don't need process.
func rebuild():
	var farshape = SphereShape.new()
	farshape.radius = far
	$Influence/Far.disabled = !active
	$Influence/Far.shape = farshape
	
	var nearshape = SphereShape.new()
	nearshape.radius = near
	$Update/Near.disabled = !active
	$Update/Near.shape = nearshape


func observe(observable):
	if !active: return
	observable.observed_by(self, global_transform.origin)


func forget(observable):
	unsubscribe_updates(observable)
	observable.forgotten_by(self, global_transform.origin)


func subscribe_updates(observable):
	if !active: return
	update_subscribers[observable] = 1
	observable.updated_by(self, true, global_transform.origin)


func unsubscribe_updates(observable):
	update_subscribers.erase(observable)
	observable.updated_by(self, false, global_transform.origin)


func _process(_delta):
	if !active: return

	var pos = global_transform.origin
	for observable in update_subscribers:
		observable.position_update(self, pos)


# TODO: Hook these up only when active is set to true.
func _on_Influence_area_entered(area):
	if !active: return
	
	var observable = Util.get_root_parent(area)
	if !observable.is_in_group("observable"): return
	observe(observable)


func _on_Influence_area_exited(area):
	if !active: return
	
	var observable = Util.get_root_parent(area)
	if !observable.is_in_group("observable"): return
	forget(observable)


func _on_Update_area_entered(area):
	if !active: return
	
	var observable = Util.get_root_parent(area)
	if !observable.is_in_group("observable"): return
	subscribe_updates(observable)


func _on_Update_area_exited(area):
	if !active: return
	
	var observable = Util.get_root_parent(area)
	if !observable.is_in_group("observable"): return
	unsubscribe_updates(observable)
