extends Area


func _enter_tree():
	Runtime.Expert.Tracker.add_player_tracker(self)


func _exit_tree():
	Runtime.Expert.Tracker.remove_player_tracker(self)
