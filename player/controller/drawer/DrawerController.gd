extends Spatial

export var active = false setget _set_active
func _set_active(_active):
	active = _active

export var primary = true setget _set_primary
func _set_primary(_primary):
	primary = _primary


export var SENSITIVITY = 0.01
export var VELOCITY_MAX = 0.722


var controlled_from = Vector3(0, 0, 1)


func move_slider(intent):
	var basis = $RigidBody.global_transform.basis
	
	var intensity = 0
	intensity    += intent.x * basis.z.dot(controlled_from)
	intensity    += intent.y * basis.x.dot(controlled_from)
	
	$RigidBody.sleeping = false
	$RigidBody.intent = basis.x * intensity


func _input(event):
	if !active:  return
	if !primary: return
	
	if event is InputEventMouseMotion:
		_input_mouse_move_slider(event)
		return
	
	_input_keyboard_move_slider(event)


func _input_keyboard_move_slider(_event):
	var intent = Geo.Vector2(0)

	if Input.is_action_pressed("control_move_left"):
		intent.x -= 1
	if Input.is_action_pressed("control_move_right"):
		intent.x += 1
	if Input.is_action_pressed("control_move_forwards"):
		intent.y -= 1
	if Input.is_action_pressed("control_move_backwards"):
		intent.y += 1
	
	move_slider(intent.normalized())


func _input_mouse_move_slider(event):
	var velocity = event.relative * SENSITIVITY
	
	move_slider(velocity.clamped(VELOCITY_MAX))

