extends Spatial


func rebuild():
	rebuild_basis()
	
	$RotatingGimbal/ClipTargetRoot.active = true
	# TODO: Remove
	# _get_PlayerCamera().Interactor.distance = 5.0
	# _get_PlayerCamera().Pointer.distance = 8.0


func rebuild_basis():
	if basis == null: return
	$RotatingGimbal.basis = basis


func _ready():
	rebuild()


export var basis = Basis() setget _set_basis
func _set_basis(_basis):
	basis = _basis
	rebuild_basis()


var PlayerCamera setget , _get_PlayerCamera
func _get_PlayerCamera(): return $RotatingGimbal/ClipTargetRoot/ClipTarget/ControlHandler/BaseController.PlayerCamera
