extends Spatial

export var active = false setget _set_active
func _set_active(_active):
	active = _active

export var primary = true setget _set_primary
func _set_primary(_primary):
	primary = _primary


export var SENSITIVITY = 0.025
export var VELOCITY_MAX = 0.3


var controlled_from = Vector3(0, 0, 1)


# TODO: Move this to _integrate_forces on the rigid body - this will fix the
# TODO: input lag issues.
func move_hinge(intent):
	var basis = $RigidBody.global_transform.basis
	
	var intensity = 0
	intensity    += intent.x * -basis.x.dot(controlled_from)
	intensity    += intent.y *  basis.z.dot(controlled_from)
	
	$RigidBody.intent_torque = Vector3(0, -1, 0) * intensity


func _input(event):
	if !active:  return
	if !primary: return
	
	if event is InputEventMouseMotion:
		_input_mouse_move_hinge(event)
		return
	
	_input_keyboard_move_hinge(event)


func _input_keyboard_move_hinge(_event):
	var intent = Geo.Vector2(0)

	if Input.is_action_pressed("control_move_left"):
		intent.x -= 1
	if Input.is_action_pressed("control_move_right"):
		intent.x += 1
	if Input.is_action_pressed("control_move_forwards"):
		intent.y -= 1
	if Input.is_action_pressed("control_move_backwards"):
		intent.y += 1
	
	move_hinge(intent.normalized())


func _input_mouse_move_hinge(event):
	var velocity = event.relative * SENSITIVITY
	
	move_hinge(velocity.clamped(VELOCITY_MAX))
