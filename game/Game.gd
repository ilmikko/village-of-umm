tool
extends Spatial

var Achievement = preload("res://script/class/Achievement.gd").new()
var Quest       = preload("res://script/class/Quest.gd").new()
var Journal     = preload("res://script/class/Journal.gd").new()
var Time        = preload("res://script/class/Time.gd").new()


func rebuild():
	Debug.print(self, "GAME REBUILT")
	# Add Time in editor as it's useful to have lighting update based on
	# changes to the time of day.
	add_child(Time)

	if !is_inside_tree(): return

	# Don't add below modules in editor.
	if Engine.is_editor_hint(): return

	add_child(Journal, true)
	
	# Game starts unpaused.
	_set_paused(false)


func _enter_tree():
	rebuild()


var in_menu = true setget _set_in_menu
func _set_in_menu(i):
	if in_menu == i: return
	in_menu = i
	if i:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


var paused = true setget _set_paused
func _set_paused(p):
	if paused == p: return
	paused = p
	_set_in_menu(p)
	
	get_tree().paused = p


# TODO: We should get BaseController instead.
var PlayerController setget , _get_PlayerController
func _get_PlayerController():
	var players = get_tree().get_nodes_in_group("player-controller")
	var number_of_players = players.size()
	if number_of_players == 1:
		return players[0]
	
	if number_of_players == 0 :
		push_error("There is no player in this scene")

	push_error("There should only be one player in the scene. %s were found" % number_of_players)
