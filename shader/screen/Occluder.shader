shader_type spatial;
render_mode unshaded, depth_test_disable;

uniform bool underwater = false;
uniform float murkiness : hint_range(0.0, 4.0) = 1.0;

uniform vec4 color_shallow : hint_color = vec4(vec3(1.0), 1.0);
uniform vec4 color_deep : hint_color = vec4(vec3(0.0, 0.005, 0.01), 1.0);

float depth_value(sampler2D tex, mat4 mat, vec3 vert, vec2 uv, float blur) {
	float depth = texture(tex, uv, blur).r * 2.0 - 1.0;
	depth = mat[3][2] / (depth + mat[2][2]) + vert.z;
	return clamp(0.0, 1.0, exp(-depth * murkiness));
}

void fragment() {
	vec3 mask = vec3(1.);
	float blur = 0.0;
	
	if (underwater) {
		float depth = depth_value(DEPTH_TEXTURE, PROJECTION_MATRIX, VERTEX, SCREEN_UV, blur);
		mask *= mix(color_deep.rgb, color_shallow.rgb, depth);
		blur += 4.5;
	}
	
	vec3 col = texture(SCREEN_TEXTURE, SCREEN_UV, blur).rgb;
	col *= mask;
	ALBEDO = col;
}