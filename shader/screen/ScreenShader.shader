shader_type canvas_item;

uniform bool underwater = false;

void fragment() {
	float blur = 0.0;
	vec4 mask = vec4(1.0);
	
	if (underwater) {
		mask = vec4(0.6, 0.7, 1.0, 1.0);
		blur = 2.0;
	}
	
	vec4 col = texture(SCREEN_TEXTURE, SCREEN_UV, blur);
	
	col *= mask;
	
	COLOR = vec4(col);
}