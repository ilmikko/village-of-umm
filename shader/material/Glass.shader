shader_type spatial;
render_mode vertex_lighting, depth_draw_alpha_prepass;

void fragment() {
	vec4 col = vec4(1., 1., 1., 0.7);
	
	METALLIC = 1.;
	SPECULAR = 1.;
	ROUGHNESS = 0.3;
	
	ALBEDO = col.rgb;
	ALPHA  = col.a;
}