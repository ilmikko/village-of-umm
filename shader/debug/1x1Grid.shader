shader_type spatial;

void fragment() {
	vec2 uv = UV;
	uv = fract(uv * 50.0 / 2.0);
	vec3 col = vec3(1.0);
	if ((uv.x < 0.5 && uv.y > 0.5) || (uv.x > 0.5 && uv.y < 0.5)) {
		col = vec3(0.0);
	}
	col *= 0.1;
	ALBEDO = col;
}