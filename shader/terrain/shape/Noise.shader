shader_type spatial;
render_mode cull_disabled;

// WARNING, IF YOU TOUCH THIS, EDIT ALSO HeightMap.gd //

// Precomputed values for Simplex Noise
const vec3 C = vec3(
	0.211324865405187, // (3.0-sqrt(3.0))/6.0
	0.366025403784439, // 0.5*(sqrt(3.0)-1.0)
	-0.577350269189626 // -1.0 + 2.0 * C.x
);

vec3 mod293v3(vec3 x) { return x - floor(x * 0.0034602076124567474) * 289.0; }
vec2 mod293v2(vec2 x) { return x - floor(x * 0.0034602076124567474) * 289.0; }
vec3 permutev3(vec3 x) { return mod293v3(((x*13.0)+1.0)*x); }

float random(in vec2 xy) {
    return fract(sin(dot(xy.xy, vec2(-41.1210058,64.121))) * 43283.2942819);
}


float noise(vec2 v) {
	// First corner (x0)
	vec2 i0 = floor(v + dot(v, C.yy));
	vec2 x0 = v - i0 + dot(i0, C.xx);

	// Other two corners (x1, x2)
	vec2 i1 = vec2(1.0, 0.0);
	if (x0.x < x0.y) { i1.xy = i1.yx; }
	vec2 x1 = x0.xy + C.xx - i1;
	vec2 x2 = x0.xy + C.zz;

	// Do some permutations to avoid
	// truncation effects in permutation
	i0 = mod293v2(i0);
	vec3 p = permutev3(
		permutev3(i0.x + vec3(0.0, i1.x, 1.0))
		        + i0.y + vec3(0.0, i1.y, 1.0)
	);

	// 1.0 / 13.0 =~ 0.07692
	vec3 x  = 2.0 * fract(p * vec3(0.07692307692307692308)) - 1.0;
	vec3 h  = abs(x) - 0.5;
	vec3 a0 = x - floor(x + 0.5);

	vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x1,x1), dot(x2,x2)), 0.0);

	m *= 5.0 * (a0*a0+h*h);

	vec3 g = vec3(0.0);
	g.x  = a0.x  * x0.x  + h.x  * x0.y;
	g.yz = a0.yz * vec2(x1.x,x2.x) + h.yz * vec2(x1.y,x2.y);
	
	return 293.0 * dot(m, g);
}

float fbm(in vec2 xy) {
	float amp = 1.0;
	float v = 0.0;
	vec2 shift = vec2(931.0);
	// Rotate by some amount to reduce axial bias.
	mat2 rot = mat2(vec2(
		cos(0.333), sin(0.333)
	), vec2(
		-sin(0.125), cos(0.125)
	));
	for (float i = 0.0; i < 3.0; ++i) {
		// Add values
		v += amp * noise(xy);
		// Rotate our canvas
		xy = rot * xy * 2.0 + shift;
		// Lessen amplitude of the new waves
		amp *= 0.75;
	}
	return v;
}

float height(vec2 global, vec2 vertex) {
  float amplitude = 1.5;
  float scale = 0.0005;
  vec2 xy = global + vertex;
  return fbm(xy * scale) * amplitude;
}

// END WARNING //

// INCLUDE res://shader/terrain/Base.shader //
// WARNING: The code below is generated by generate_shaders.sh                   // <-- FROM res://shader/terrain/Base.shader -->
// WARNING: The file included is resource res://shader/terrain/Base.shader       // <-- FROM res://shader/terrain/Base.shader -->
// WARNING: You should not edit this section directly; please edit the source    // <-- FROM res://shader/terrain/Base.shader -->
// WARNING: file and run the generation script again.                            // <-- FROM res://shader/terrain/Base.shader -->
uniform vec3 origin;                                                             // <-- FROM res://shader/terrain/Base.shader -->
uniform vec3 scale;                                                              // <-- FROM res://shader/terrain/Base.shader -->
uniform sampler2D albedo_scale;                                                  // <-- FROM res://shader/terrain/Base.shader -->
                                                                                 // <-- FROM res://shader/terrain/Base.shader -->
const vec2 epsilon = vec2(0.01, 0.0);                                            // <-- FROM res://shader/terrain/Base.shader -->
                                                                                 // <-- FROM res://shader/terrain/Base.shader -->
vec3 normal(vec2 global, vec2 vertex) {                                          // <-- FROM res://shader/terrain/Base.shader -->
  return normalize(                                                              // <-- FROM res://shader/terrain/Base.shader -->
    vec3(                                                                        // <-- FROM res://shader/terrain/Base.shader -->
       height(global, vertex - epsilon.xy)                                       // <-- FROM res://shader/terrain/Base.shader -->
      -height(global, vertex + epsilon.xy),                                      // <-- FROM res://shader/terrain/Base.shader -->
       2.0*epsilon.x,                                                            // <-- FROM res://shader/terrain/Base.shader -->
       height(global, vertex + epsilon.xy)                                       // <-- FROM res://shader/terrain/Base.shader -->
      -height(global, vertex - epsilon.xy)                                       // <-- FROM res://shader/terrain/Base.shader -->
    )                                                                            // <-- FROM res://shader/terrain/Base.shader -->
  );                                                                             // <-- FROM res://shader/terrain/Base.shader -->
}                                                                                // <-- FROM res://shader/terrain/Base.shader -->
                                                                                 // <-- FROM res://shader/terrain/Base.shader -->
void vertex() {                                                                  // <-- FROM res://shader/terrain/Base.shader -->
  float height = height(origin.xz/scale.xz, VERTEX.xz);                          // <-- FROM res://shader/terrain/Base.shader -->
  vec3  normal = normal(origin.xz/scale.xz, VERTEX.xz);                          // <-- FROM res://shader/terrain/Base.shader -->
  VERTEX += NORMAL * height;                                                     // <-- FROM res://shader/terrain/Base.shader -->
  NORMAL = normal;                                                               // <-- FROM res://shader/terrain/Base.shader -->
  COLOR.r = normal.z * 0.015;                                                    // <-- FROM res://shader/terrain/Base.shader -->
  COLOR.g = -height/140.0 - 0.5;                                                 // <-- FROM res://shader/terrain/Base.shader -->
}                                                                                // <-- FROM res://shader/terrain/Base.shader -->
                                                                                 // <-- FROM res://shader/terrain/Base.shader -->
void fragment() {                                                                // <-- FROM res://shader/terrain/Base.shader -->
  ALBEDO = texture(albedo_scale, vec2(COLOR.r, COLOR.g)).rgb;                    // <-- FROM res://shader/terrain/Base.shader -->
}                                                                                // <-- FROM res://shader/terrain/Base.shader -->
