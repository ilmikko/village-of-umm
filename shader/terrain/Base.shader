// Common heightmap base: Do not use directly; instead, run generate_shaders.sh
// to propagate this change to other shaders.
// Nothing before the first newline is included in the generated snippet.
shader_type spatial;
float height(vec2 global, vec2 vertex) {
	return 0.0;
}

uniform vec3 origin;
uniform vec3 scale;
uniform sampler2D albedo_scale;

const vec2 epsilon = vec2(0.01, 0.0);

vec3 normal(vec2 global, vec2 vertex) {
	return normalize(
		vec3(
			 height(global, vertex - epsilon.xy)
			-height(global, vertex + epsilon.xy),
			 2.0*epsilon.x,
			 height(global, vertex + epsilon.xy)
			-height(global, vertex - epsilon.xy)
		)
	);
}

void vertex() {
	float height = height(origin.xz/scale.xz, VERTEX.xz);
	vec3  normal = normal(origin.xz/scale.xz, VERTEX.xz);
	VERTEX += NORMAL * height;
	NORMAL = normal;
	COLOR.r = normal.z * 0.015;
	COLOR.g = -height/140.0 - 0.5;
}

void fragment() {
	ALBEDO = texture(albedo_scale, vec2(COLOR.r, COLOR.g)).rgb;
}
