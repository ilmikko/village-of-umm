shader_type spatial;

uniform vec4 albedo : hint_color = vec4(0, 0, 0, 1);

void fragment() {
	ALBEDO = albedo.rgb;
}