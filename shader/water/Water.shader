shader_type spatial;
render_mode cull_disabled, depth_draw_always;

uniform float murkiness : hint_range(0.0, 4.0) = 1.0;
uniform float roughness : hint_range(0.0, 1.0) = 0.0;
uniform float shallow_fade : hint_range(0.0, 1.0) = 0.85;

uniform float normal_intensity : hint_range(0.0, 2.0) = 0.2;
uniform float refract_intensity : hint_range(0.0, 1.0) = 0.07;
uniform float refract_bias : hint_range(0.0, 0.05) = 0.006;

uniform vec4 color_shallow : hint_color = vec4(vec3(1.0), 1.0);
uniform vec4 color_deep : hint_color = vec4(vec3(0.0, 0.005, 0.01), 1.0);

uniform float wave_amplitude : hint_range(0.0, 1.0) = 0.05;
uniform float wave_scale : hint_range(0.5, 20.0) = 9.0;
uniform float wave_speed : hint_range(0.0, 4.0) = 1.0;

uniform sampler2D wave_texture;

vec3 waves(vec2 uv, float t) {
	float r = texture(wave_texture, uv + vec2(cos(t) * 0.005, t * 0.005 + sin(t) * 0.0025)).r;
	float g = texture(wave_texture, uv + vec2(t * 0.01 + sin(t) * 0.005, cos(t) * 0.015)).g;
	float b = texture(wave_texture, uv + vec2(t * 0.015, t * 0.007)).b;
	return vec3(r, g, b) * 0.1;
}

void vertex() {
	VERTEX.y += sin(UV.y * wave_scale * 0.98 + TIME * wave_speed) * cos(UV.x * wave_scale * 1.01 + TIME * wave_speed) * wave_amplitude;
}

float depth_value(sampler2D tex, mat4 mat, vec3 vert, vec2 uv) {
	float depth = texture(tex, uv).r * 2.0 - 1.0;
	depth = mat[3][2] / (depth + mat[2][2]) + vert.z;
	return clamp(0.0, 1.0, exp(-depth * murkiness));
}

void fragment() {
	vec3 waves = waves(UV * 2.0, TIME);
	vec2 wave_offset = waves.rg * refract_intensity - refract_bias;
	vec3 col = texture(SCREEN_TEXTURE, SCREEN_UV + wave_offset).rgb;
	
	float depth_real = depth_value(DEPTH_TEXTURE, PROJECTION_MATRIX, VERTEX, SCREEN_UV);
	float depth_wave = depth_value(DEPTH_TEXTURE, PROJECTION_MATRIX, VERTEX, SCREEN_UV + wave_offset);
	
	// Camera distance fade
	float fade = smoothstep(0.05, 0.12, -VERTEX.z);
	
	NORMALMAP = 0.5 + waves * normal_intensity;
	ALBEDO = mix(color_deep.rgb, color_shallow.rgb, depth_wave) * col;
	METALLIC = depth_wave;
	ROUGHNESS = depth_real * roughness;
	ALPHA = smoothstep(1.0, shallow_fade, depth_real) * fade;
}