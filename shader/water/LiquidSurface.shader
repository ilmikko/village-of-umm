shader_type spatial;
render_mode cull_front, vertex_lighting;

void vertex() {
	VERTEX += -NORMAL * 0.015;
}

void fragment() {
	vec3 uvw = VERTEX.xyz;
	
	uvw = (CAMERA_MATRIX * vec4(uvw, 1.0)).xyz;
	uvw = (inverse(WORLD_MATRIX) * vec4(uvw, 1.0)).xyz;
	
	if (uvw.y > 0.12) {
		ALPHA = 0.0;
	} else {
		ALBEDO = vec3(0.0, 1.0, 0.0);
	}
}