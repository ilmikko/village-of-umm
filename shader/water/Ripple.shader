shader_type spatial;
render_mode cull_disabled, depth_test_disable;

uniform float normal_intensity : hint_range(0.0, 2.0) = 0.2;
uniform float roughness : hint_range(0.0, 1.0) = 0.0;

uniform float ripple_radius : hint_range(0.0, 2.0) = 0.5;
uniform float ripple_thickness : hint_range(0.0, 1.0) = 0.1;
uniform float ripple_amplitude : hint_range(0.0, 1.0) = 0.1;

uniform float wave_amplitude : hint_range(0.0, 1.0) = 0.05;
uniform float wave_scale : hint_range(0.5, 20.0) = 9.0;
uniform float wave_speed : hint_range(0.0, 4.0) = 1.0;

uniform sampler2D wave_texture;

vec3 waves(vec2 uv, float t) {
	float r = texture(wave_texture, uv + vec2(t * 0.005, cos(t) * 0.005)).r;
	float g = texture(wave_texture, uv + vec2(t * 0.01, cos(t) * 0.015)).g;
	float b = texture(wave_texture, uv + vec2(t * 0.015, t * 0.007)).b;
	return vec3(r, g, b) * 0.1;
}

float ripple(vec2 uv) {
	float d = length(uv);
	return smoothstep(ripple_radius - ripple_thickness, ripple_radius, d)
	- smoothstep(ripple_radius, ripple_radius + ripple_thickness, d);
}

void vertex() {
	float rip = ripple(UV * 2.0 - 1.0);
	VERTEX.y += rip * ripple_amplitude;
	VERTEX.y += sin(UV.y * wave_scale * 0.98 + TIME * wave_speed) * cos(UV.x * wave_scale * 1.01 + TIME * wave_speed) * wave_amplitude;
}

void fragment() {
	vec3 col = vec3(0.1);
	vec3 waves = waves(UV, TIME);
	
	float rip = ripple(UV * 2.0 - 1.0);
	col *= rip;
	
	// Camera distance fade
	float fade = smoothstep(0.05, 0.12, -VERTEX.z);
	
	NORMALMAP = 0.5 + waves * normal_intensity;
	ALBEDO = col;
	METALLIC = 1.0;
	ROUGHNESS = roughness;
	ALPHA = rip * fade * ripple_amplitude;
}