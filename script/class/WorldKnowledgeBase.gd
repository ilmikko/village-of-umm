extends Node
class_name WorldKnowledgeBase

var items_by_group = {}
var knowledge_about_items = {}

func get_item_knowledge(item):
	return knowledge_about_items[item]

func get_items_knowledges_in_group(group_name):
	var knowledges = items_by_group.get(group_name, [])
	_remove_outdated_knowledges(knowledges)
	return knowledges

func get_items_knowledges_in_groups(group_names):
	var item_knowledges_dict = {}
	for group_name in group_names:
		for item in get_items_knowledges_in_group(group_name):
			item_knowledges_dict[item] = true
	return item_knowledges_dict.keys()

func remove_knowledge(knowledge):
	knowledge.outdated = true

func add_item(item):
	_add_knowledge_about_item(item)
	_add_item_by_group(item)

func _add_knowledge_about_item(item):
	if not knowledge_about_items.has(item):
		knowledge_about_items[item] = ItemKnowledge.new(item)
	knowledge_about_items[item].update_knowledge()

func _add_item_by_group(item):
	for group in item.get_groups():
		var items_in_group = _get_or_create_collection_value(items_by_group, group)
		_add_item_if_not_exists(items_in_group, item)

func _add_item_if_not_exists(array, item):
	var item_knowledge = knowledge_about_items[item]
	if not item_knowledge in array:
		array.append(item_knowledge)

func _get_or_create_collection_value(dictionary, key):
	if not dictionary.has(key):
		dictionary[key] = []
	return dictionary[key]

func _remove_outdated_knowledges(knowledges):
	for i in range(knowledges.size() - 1, -1, -1):
		if knowledges[i].outdated:
			knowledges.remove(i)
