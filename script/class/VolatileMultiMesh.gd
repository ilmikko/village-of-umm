extends MultiMesh
class_name VolatileMultiMesh

# Used in the git hook script to remove all generated mesh data.
export(bool) var delete_mesh_data = true
