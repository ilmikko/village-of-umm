extends Node
class_name ItemKnowledge

var item: Node
var location: Vector3
var groups = []
var outdated = false

func _init(it):
	item = it
	update_knowledge()

func update_knowledge():
	if "driver" in item:
		location = item.driver.global_transform.origin
	else:
		location = item.global_transform.origin
	groups = item.get_groups()
