extends Node
class_name Quest


var states  = {}
var quests  = {}
var scripts = {}


# State 0 = not started.
func is_started(id):
	return state(id) != 0


# State < 0 = finished in some state.
func is_finished(id):
	return state(id) < 0


func progress(id, next_state):
	states[id] = next_state
	quests[id].state = next_state


func quest_load(id) -> bool:
	if scripts.has(id): return true
	
	var path = Quests.path[id]
	scripts[id] = load(path)
	if scripts[id] == null:
		push_error("Quest ID script %s from path %s has failed to load." % [id, path])
		return false
	
	return true

func get_quest(id):
	return quests[id]

# Loads the quest, begins it, and optionally progresses it to a new state.
func start(id, next_state = 0):
	if  is_started(id): return
	if !quest_load(id): return
	
	var quest = scripts[id].new()
	quests[id] = quest
	quest.start()
	
	Runtime.UI.Hint.quest_title     = quest.title
	Runtime.UI.Hint.quest_title_sub = quest.title_sub
	Runtime.UI.Hint.quest_title_animate()
	
	if next_state <= 0: return
	progress(id, next_state)


func state(id):
	if !states.has(id): return 0 # Unstarted.
	return states[id]
