extends Node
class_name Duration

var total_seconds = 0

func _init(units = {}):
	total_seconds += units.get("seconds", 0)
	total_seconds += units.get("minutes", 0) * 60
	total_seconds += units.get("hours", 0) * 60 * 60
	total_seconds += units.get("days", 0) * 24 * 60 * 60

func days():
	return int(total_seconds / 60 / 60 / 24)

func hours():
	return int(total_seconds / 60 / 60) % 24

func minutes():
	return int(total_seconds / 60) % 60

func seconds():
	return fmod(total_seconds, 60)


func add_seconds(seconds):
	total_seconds += seconds

func greater_than(other_duration):
	return total_seconds > other_duration.total_seconds

func less_than(other_duration):
	return total_seconds < other_duration.total_seconds

func _to_string():
	# TODO: Remove redundent elements
	return "P%sD%sH%sM%sS" % [days(), hours(), minutes(), seconds()]
