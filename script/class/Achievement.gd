extends Node


var unlocked = {}


func is_unlocked(id):
	return unlocked.has(id)


func unlock(id):
	if is_unlocked(id): return
	unlocked[id] = true
	
	if !Achievement.data.has(id):
		push_warning("Achievement #%s not found in Achievement.data." % id)
		return
	
	var achievement = Achievement.data[id]
	var description = "Achievement unlocked!"
	if "desc" in achievement:
		description = achievement.desc
	
	Runtime.UI.Achievement.description = description
	Runtime.UI.Achievement.animate()
