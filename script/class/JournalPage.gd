extends Node
class_name JournalPage

var title: String
var entries: Array
var id: int setget _set_id

var custom_serializer

func _init(new_title, new_entries = []):
	add_to_group("serialize")
	title = new_title
	entries = new_entries
	custom_serializer = preload("res://serializers/JournalPageSerializer.gd").new(self)

func add_entry(new_entry):
	entries.append(new_entry)


func _set_id(new_id):
	id = new_id
	name = str(id)
