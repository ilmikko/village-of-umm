class_name Scene


# Returns the filepath of a node's scene.
static func filepath(node):
	if node == null: return ""
	if node.filename: return node.filename
	var r = root(node)
	if  r == null: return ""
	return r.filename


# Returns the scene root or itself.
static func root(node):
	if node == null:       return null
	if node.owner == null: return node
	return node.owner
