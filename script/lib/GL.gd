# This library tries to mimic GL scripts with minimal changes to original code.
# It is helpful as then mathematical formulas from shaders can be re-used within
# the game. 
extends Object
class_name GL

static func abs_(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return abs(s);
	elif t == TYPE_VECTOR2:
		return Vector2(abs(s.x), abs(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(abs(s.x), abs(s.y), abs(s.z));

static func cos_(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return cos(s);
	elif t == TYPE_VECTOR2:
		return Vector2(cos(s.x), cos(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(cos(s.x), cos(s.y), cos(s.z));

static func dot(a, b):
	return a.dot(b);

static func fract(s):
	return s-floor_(s);

static func sin_(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return sin(s);
	elif t == TYPE_VECTOR2:
		return Vector2(sin(s.x), sin(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(sin(s.x), sin(s.y), sin(s.z));

static func floor_(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return floor(s);
	elif t == TYPE_VECTOR2:
		return Vector2(floor(s.x), floor(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(floor(s.x), floor(s.y), floor(s.z));

static func length(vec):
	return vec.length()

static func mat2(x, y):
	return Transform2D(x, y, Vector2(0, 0))

static func max_(s, m):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return max(s, m);
	elif t == TYPE_VECTOR2:
		return Vector2(max(s.x, m), max(s.y, m));
	elif t == TYPE_VECTOR3:
		return Vector3(max(s.x, m), max(s.y, m), max(s.z, m));

static func mix(a, b, p):
	return lerp(a, b, p)

static func random(s) -> float:
	return random2(Vector2(4281.230, s)).y

static func random2(vec) -> Vector2:
	var a = Vector2(20.0, -4.0);
	var b = Vector2(0.3, -146.3);
	var c = Vector2(-2.9, -0.2);
	vec += c;
	var some = Vector2(
		dot(vec, a),
		dot(vec, b)
	);
	return sin_(some);

static func random3(vec) -> Vector3:
	var a = Vector3(20.0, -4.0, 0.62);
	var b = Vector3(0.3, -146.3, 0.00515138);
	var c = Vector3(-2.9, -0.2, 80.3218);
	var d = Vector3(0.4124, 25.0321, -820.0214);
	vec += d;
	var some = Vector3(
		dot(vec, a),
		dot(vec, b),
		dot(vec, c)
	);
	return sin_(some)

static func vec2(x, y=null) -> Vector2:
	return Geo.Vector2(x, y)

static func vec2xx(v):
	return Vector2(v.x, v.x)

static func vec2xy(v):
	return Vector2(v.x, v.y)

static func vec2yx(v):
	return Vector2(v.y, v.x)

static func vec2yy(v):
	return Vector2(v.y, v.y)

static func vec2yz(v):
	return Vector2(v.y, v.z)

static func vec2zz(v):
	return Vector2(v.z, v.z)

static func vec3(x, y=null, z=null):
	return Geo.Vector3(x, y, z)
