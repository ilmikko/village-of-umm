extends Node
class_name Geo

static func quat_yaw(quat):
	quat.x = 0
	quat.y = quat.y
	quat.z = 0
	quat.w = quat.w
	return quat

static func quat_pitch(quat):
	quat.x = 0
	quat.y = quat.y
	quat.z = quat.z
	quat.w = 0
	return quat

static func basis_dot(basis1, basis2):
	return min(basis1.x.dot(basis2.x),
		   min(basis1.y.dot(basis2.y),
			   basis1.z.dot(basis2.z)))

static func basis_pitch_quat(basis):
	return quat_pitch(basis.get_rotation_quat())

static func basis_pitch(basis):
	return Basis(basis_pitch_quat(basis))

static func basis_yaw_quat(basis):
	return quat_yaw(basis.get_rotation_quat())

static func basis_yaw(basis):
	return Basis(basis_yaw_quat(basis))

static func clean_zeroes(vec):
	if typeof(vec) == TYPE_VECTOR2:
		if vec.x == -0: vec.x = 0
		if vec.y == -0: vec.y = 0
		return vec
	elif typeof(vec) == TYPE_VECTOR3:
		if vec.x == -0: vec.x = 0
		if vec.y == -0: vec.y = 0
		if vec.z == -0: vec.z = 0
		return vec

static func RandnVector2(rng = null) -> Vector2:
	if rng == null:
		return Vector2(randf(), randf())
	return Vector2(rng.randf(), rng.randf())

static func RandnVector3(rng = null) -> Vector3:
	if rng == null:
		return Vector3(randf(), randf(), randf())
	return Vector3(rng.randf(), rng.randf(), rng.randf())

static func RandVector2(rng = null) -> Vector2:
	return RandnVector2(rng) * 2.0 - Vector2(1.0, 1.0)

static func RandVector3(rng = null) -> Vector3:
	return RandnVector3(rng) * 2.0 - Vector3(1.0, 1.0, 1.0)

static func Vector2(a = null, b = null) -> Vector2:
	if b != null:
		return Vector2(a, b)
	if a != null:
		return Vector2(a, a)
	return Vector2(1, 1)

static func Vector3(a = null, b = null, c = null) -> Vector3:
	if c != null:
		return Vector3(a, b, c)
	if b != null:
		return Vector3(a, b, b)
	if a != null:
		return Vector3(a, a, a)
	return Vector3(1, 1, 1)

static func Vector2x0(x) -> Vector2:
	return Vector2(x, 0)

static func Vector2y0(y) -> Vector2:
	return Vector2(0, y)

static func Vector3x0(x) -> Vector3:
	return Vector3(x, 0, 0)

static func Vector3y0(y) -> Vector3:
	return Vector3(0, y, 0)

static func Vector3z0(z) -> Vector3:
	return Vector3(0, 0, z)

static func Vectorxy(vec) -> Vector2:
	return Vector2(vec.x, vec.y)

static func Vectorxy0(vec) -> Vector3:
	return Vector3(vec.x, vec.y, 0)

static func Vectorx0y(vec) -> Vector3:
	return Vector3(vec.x, 0, vec.y)

static func Vectorxz(vec) -> Vector2:
	return Vector2(vec.x, vec.z)

static func Vectorx0z(vec) -> Vector3:
	return Vector3(vec.x, 0, vec.z)

static func Vectorzx(vec) -> Vector2:
	return Vector2(vec.z, vec.x)

static func Vectorz0x(vec) -> Vector3:
	return Vector3(vec.z, 0, vec.x)

static func sum(vec) -> float:
	return vec[0] + vec[1] + vec[2]

static func volume(vec) -> float:
	return vec[0] * vec[1] * vec[2]
