class_name Quests

enum {
	NONE
	MATILDA_AND_JIM
	THE_RING_OF_INVISIBILITY
}

const path = {
	MATILDA_AND_JIM: "res://quest/live/matildaandjim/Quest.gd",
	THE_RING_OF_INVISIBILITY: "res://quest/live/theringofinvisibility/Quest.gd"
}
