class_name Achievement

enum achievement {
	NONE
	TEST
	CLIMB_TREE
	RIDE_BARREL_10M
}

# TODO: Colour achievements differently based on their rarity.
enum rarity {
	COMMON # >50%
	RARE   # >10%
	RAREST # >1%
}

const data = {
	achievement.TEST:            { "desc": "This is a Test Achievement! Congratulations!" },
	achievement.CLIMB_TREE:      { "desc": "Climb a tree to the very top." },
	achievement.RIDE_BARREL_10M: { "desc": "Ride a barrel for 10 meters." },
}
