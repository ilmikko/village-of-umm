class_name Materials

enum {
	VOID
	DIRT
	FLESH
	METAL
	STONE
	WOOD
}

const names = {
	VOID: "Void",
	DIRT: "Dirt",
	FLESH: "Flesh",
	METAL: "Metal",
	STONE: "Stone",
	WOOD: "Wood",
}

static func name(i):
	return names[i]
