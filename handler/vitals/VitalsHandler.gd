extends Spatial


signal food_eaten(food)


export(NodePath) var interaction_notifier_path
export(NodePath) var holder_handler_path

var NodeMatcher = preload("res://script/class/NodeMatcher.gd")


var water_group_matcher = NodeMatcher.new()
var food_group_matcher = NodeMatcher.new()


func _ready():
	if interaction_notifier_path:
		water_group_matcher.matching_group = "water"
		get_node(interaction_notifier_path).add_interaction("primary", self, water_group_matcher)

		food_group_matcher.matching_group = "food"
		get_node(interaction_notifier_path).add_interaction("secondary", self, null, food_group_matcher)


func interaction_triggered(actor, pointing_at_object, _interactor_name):
	var held_object = _get_held_object()
	if water_group_matcher.is_matching(pointing_at_object):
		$Thirst.change_value_by(1)
	if food_group_matcher.is_matching(held_object):
		Runtime.Expert.ConsumableEffect.apply_consumable_effect_to_actor(actor, held_object)
		emit_signal("food_eaten", held_object)


func _get_held_object():
	return get_node(holder_handler_path).held_object()


func _on_Thirst_state_changed(_new_state):
	pass


func _on_Hunger_state_changed(new_state):
	pass
