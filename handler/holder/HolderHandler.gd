extends Spatial

signal object_held(object)
signal object_dropped(object)


export(PackedScene) var held_obj


func _ready():
	if held_obj:
		hold(held_obj.instance())

	interaction_notifier.add_interaction("primary", self)


func interaction_triggered(actor, object, interactor_name):
	if object && (object is RigidBody):
		hold(object)
	else:
		drop()


func drop():
	skeleton.anim_holding_left = 0.0
	skeleton.anim_holding_right = 0.0
	
	if rigid_body_holder.held_object != null:
		rigid_body_holder.drop_object()
	if skeleton.HolderArmLeft.held_object != null:
		skeleton.HolderArmLeft.drop_object()
	if skeleton.HolderArmRight.held_object != null:
		skeleton.HolderArmRight.drop_object()

	emit_signal("object_dropped", held_obj)
	held_obj = null


func held_object():
	if rigid_body_holder.held_object != null:
		return rigid_body_holder.held_object
	if skeleton.HolderArmLeft.held_object != null:
		return skeleton.HolderArmLeft.held_object
	if skeleton.HolderArmRight.held_object != null:
		return skeleton.HolderArmRight.held_object
	return null


# TODO: Different holding animations for different shapes of items
func hold(object):
	if is_holding(): return
	
	held_obj = object

	if object.is_in_group("equippable"):
		# TODO: What about left arm? :P
		skeleton.HolderArmRight.hold_object(object)
		skeleton.anim_holding_right = 0.4
		emit_signal("object_held", object)
		return
	
	if object is RigidBody:
		rigid_body_holder.hold_object(object)
		skeleton.anim_holding_left = 1.0
		skeleton.anim_holding_right = 1.0
		emit_signal("object_held", object)
		return
	
	skeleton.anim_holding = false


func is_holding():
	return held_object() != null


export(NodePath) var interaction_notifier_path
onready          var interaction_notifier = get_node(interaction_notifier_path)

export(NodePath) var rigid_body_holder_path
onready          var rigid_body_holder = get_node(rigid_body_holder_path)

export(NodePath) var skeleton_path
onready          var skeleton = get_node(skeleton_path)
