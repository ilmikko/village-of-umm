extends Spatial

signal wetness_changed(new, old)
signal water_released(amount_released)

var body


func _ready():
	body = get_parent()
	Runtime.Game.Time.connect("at_least_1_0_seconds_passed", self, "_on_Time_at_least_1_0_seconds_passed")


# Set to 0.0 for health to be automatically derived by HealthExpert.
export var max_wetness = 0.0

func add_litres_of_water(number_of_litres):
	_set_wetness(wetness + number_of_litres)

var wetness = 0.0 setget _set_wetness
func _set_wetness(new_wetness):
	emit_signal("wetness_changed", new_wetness, wetness)
	
	wetness = new_wetness
	Debug.text(body, "wetness", wetness)


func _on_Time_at_least_1_0_seconds_passed(n):
	if wetness == 0: return

	var litres_released = min(wetness, n / 100.0)
	_set_wetness(wetness - litres_released)
	emit_signal("water_released", litres_released) 
