extends Spatial

var barrel_scene = preload("res://world/barrel/Barrel.tscn")

var barrels = []

func _on_Timer_timeout():
	var barrel = barrel_scene.instance()
	barrel.global_transform.origin = Vector3(-5, 5, 0)
	barrels.append(barrel)
	add_child(barrel)
	barrel.apply_central_impulse(Vector3(100,1,1))
	
	if barrels.size() > 5:
		barrels[0].queue_free()
		barrels.remove(0)
