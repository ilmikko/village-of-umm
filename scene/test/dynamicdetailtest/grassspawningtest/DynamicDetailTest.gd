tool
extends Spatial

export(float, 0.0, 1.0) var error_margin = 0.1 setget _set_error_margin
func _set_error_margin(_error_margin):
	error_margin = _error_margin
	rebuild()

export(float, 0.0, 10.0) var radius = 10.0 setget _set_radius
func _set_radius(_radius):
	radius = _radius
	rebuild()

var collision;
var observer;
var material;
var mesh;

func rebuild():
	if !is_inside_tree():
		return
	if !material:
		if !mesh:
			mesh = $MeshInstance
		material = mesh.get_surface_material(0).duplicate()
		mesh.set_surface_material(0, material)
	if !collision:
		collision = $Area/CollisionShape
	
	_shader("error_margin", error_margin)
	
	if observer:
		var spawn_radius = radius - radius*error_margin
		var new_transform = Geo.Vectorx0z(observer.global_transform.origin)
		new_transform += Geo.Vectorx0z(Geo.RandVector3()).normalized()*spawn_radius
		global_transform.origin = new_transform
	_shader("self_position", global_transform.origin)
	
	_shader("radius", radius)
	var sphere = SphereShape.new()
	sphere.radius = radius
	collision.shape = sphere

func _process(delta):
	if !observer:
		return
	
	_shader("observer_position", observer.global_transform.origin)

func _ready():
	rebuild()

func _init():
	rebuild()

func _shader(param, val):
	# TODO: remove this line, right now it's used to debug when the shader gets
	# fed data
	Debug.text(self, "set shader param", randf())
	material.set_shader_param(param, val)

func _on_Area_area_entered(area):
	if !area.is_in_group("observer"):
		return
	
	Debug.text(self, "Observed", "true")
	observer = area;

func _on_Area_area_exited(area):
	if !area.is_in_group("observer"):
		return
	
	# When the observer exits, we'll try to find a new position just next to the
	# observer, inside of the area.
	# TODO: What happens if the observer never enters the area?
	rebuild()
	
	Debug.text(self, "Observed", "false")
	observer = null;
