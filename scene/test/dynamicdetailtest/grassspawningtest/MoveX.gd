extends Spatial

var t = 0.0;


func _process(delta):
	# rotate(Vector3.DOWN, 0.005)
	t += delta
	global_transform.origin = Vector3(cos(t*0.25)*3.0, cos(t*2.0), sin(t*0.25)*3.0)
