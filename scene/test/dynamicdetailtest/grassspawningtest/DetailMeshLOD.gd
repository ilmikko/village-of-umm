extends Spatial


var material : ShaderMaterial;


func spawn():
	$LOD0.visible = true


func despawn():
	$LOD0.visible = false


func _ready():
	material = $LOD0.get_surface_material(0)
	material.set_shader_param("self_position", global_transform.origin)


func _on_Observable_observed():
	spawn()


func _on_Observable_forgotten():
	despawn()


func _on_Observable_updated(pos):
	Debug.text(self, "updated pos", pos)
	material.set_shader_param("observer_position", pos)
