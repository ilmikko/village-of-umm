extends Spatial

var t = 0.0;

func _process(delta):
	# rotate(Vector3.DOWN, 0.005)
	t += delta
	global_transform.origin = Vector3(0, 0, cos(1.5 + t*0.15)*20.0)
