extends Spatial


var instance_count = 0;


func observed():
	$WavyDebugTerrain/TerrainBase/GrassField.rebuild()


func forgotten():
	instance_count = $WavyDebugTerrain/TerrainBase/GrassField.multimesh.instance_count;
	$WavyDebugTerrain/TerrainBase/GrassField.multimesh.instance_count = 0


func observer_update(pos):
	$WavyDebugTerrain/TerrainBase/GrassField.observer_update(pos);


func _ready():
	instance_count = $WavyDebugTerrain/TerrainBase/GrassField.multimesh.instance_count;


func _on_Observable_forgotten():
	forgotten()


func _on_Observable_observed():
	observed()


func _on_Observable_updated(pos):
	observer_update(pos)
