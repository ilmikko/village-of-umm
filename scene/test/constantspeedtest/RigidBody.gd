extends RigidBody

var STRENGTH = 1.25
var MAX_VELOCITY = 4.0

func _integrate_forces(state):
	angular_damp = 1.0
	Debug.text(self, $"..".name, state.linear_velocity.z)
	var mx = max(1.0 - state.linear_velocity.length() / MAX_VELOCITY, 0.0)
	Debug.text(self, $"..".name+" MAX", mx)
	apply_central_impulse(Vector3(0, 0, -1) * mass * STRENGTH * mx)
