extends Spatial

func _ready():
	var quest_id = Quests.THE_RING_OF_INVISIBILITY
	Runtime.Game.Quest.start(quest_id)
	Runtime.Game.Quest.progress(quest_id, Runtime.Game.Quest.get_quest(quest_id).S_AGREED_TO_HELP)
