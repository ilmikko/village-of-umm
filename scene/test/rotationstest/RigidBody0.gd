extends RigidBody

var t = 0

func _integrate_forces(state):
	var basis01 = $Axis00.global_transform.basis
	var basis11 = $"../../Row1/Axis10".global_transform.basis
	var vdiff1 = Quat(basis01).slerp(basis11, 0.25)
	var off = min(basis01.x.dot(basis11.x), min(basis01.y.dot(basis11.y), basis01.z.dot(basis11.z))) * 0.5 + 0.5
	Debug.text(self, "OFF", off)
	Debug.text(self, "VDIFF1", vdiff1)
	state.apply_torque_impulse(-state.angular_velocity * 0.01)   # Angular Damping
	state.apply_torque_impulse(-vdiff1.get_euler() * off) # Seek offset
	# state.apply_torque_impulse(Vector3(1, 0, 0) * 0.0001)
	t += 1;
