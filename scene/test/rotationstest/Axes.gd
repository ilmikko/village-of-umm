extends Spatial

func _process(delta):
	var basis01 = $Row0/RigidBody1/Axis01.global_transform.basis;
	var basis02 = $Row0/RigidBody2/Axis02.global_transform.basis;
	var basis03 = $Row0/RigidBody3/Axis03.global_transform.basis;
	
	var basis11 = $Row1/Axis11.global_transform.basis;
	var basis12 = $Row1/Axis12.global_transform.basis;
	var basis13 = $Row1/Axis13.global_transform.basis;
	
	var dot1 = basis01.y.dot(basis11.y)
	var dot2 = basis02.z.dot(basis12.z)
	var dot3 = basis03.x.dot(basis13.x)
	
	var vdiff1 = basis01.y - basis11.y
	var vdiff2 = basis02.z - basis12.z
	var vdiff3 = basis03.x - basis13.x
	
	Debug.text(self, "dot1", dot1)
	Debug.text(self, "dot2", dot2)
	Debug.text(self, "dot3", dot3)
	
	Debug.text(self, "vdiff1", vdiff1)
	Debug.text(self, "vdiff2", vdiff2)
	Debug.text(self, "vdiff3", vdiff3)
