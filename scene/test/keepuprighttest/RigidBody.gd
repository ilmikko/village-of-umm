extends RigidBody

func _integrate_forces(state):
	Debug.text(self, "ANGULAR VEL", state.angular_velocity)
	Debug.text(self, "BASIS",       global_transform.basis)
	Debug.text(self, "BASIS QUAT",  Quat(global_transform.basis))
	Debug.text(self, "BASIS NIL",   Basis())
