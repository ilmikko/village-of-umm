tool
extends Spatial

export(float, 0.1, 10.0) var width = 1.0 setget _set_width
func _set_width(_width):
	width = _width
	rebuild()

export(float, 0.1, 10.0) var height = 0.5 setget _set_height
func _set_height(_height):
	height = _height
	rebuild()

export(float, 0.1, 10.0) var depth = 1.0 setget _set_depth
func _set_depth(_depth):
	depth = _depth
	rebuild()

export(float, 0.1, 500.0) var gen_seed = 123.0 setget _set_gen_seed
func _set_gen_seed(_gen_seed):
	gen_seed = _gen_seed
	rebuild()

export(int, 1.0, 5.0) var gen_floors = 1.0 setget _set_gen_floors
func _set_gen_floors(_gen_floors):
	gen_floors = _gen_floors
	rebuild()

export(int, 0.0, 5.0) var gen_max_depth = 1.0 setget _set_gen_max_depth
func _set_gen_max_depth(_gen_max_depth):
	gen_max_depth = _gen_max_depth
	rebuild()

export(float, 0.0, 1.0) var gen_uniformity = 1.0 setget _set_gen_uniformity
func _set_gen_uniformity(_gen_uniformity):
	gen_uniformity = _gen_uniformity
	rebuild()

var Part = preload("res://scene/test/housegeneration/part/Part.tscn")

var rooms = []

func rebuild():
	if !is_inside_tree():
		return
	
	var origin = Vector3(0, height, 0)
	var scale  = Vector3(width, height, depth)
	
	$Area.scale = scale
	$Area.global_transform.origin = origin
	
	rooms = []
	
	generate_partitions(origin, scale)
	generate_turtle(rooms)

func add_box(origin, size, color):
	var box = Part.instance()
	box.scale *= size
	box.transform.origin += origin
	$Rooms.add_child(box)

func slice(origin, size, axis, amount):
	var slice_left   = Geo.Vector3(1.0)
	var slice_right  = Geo.Vector3(1.0)
	var offset_left  = Geo.Vector3(0.0)
	var offset_right = Geo.Vector3(0.0)
	
	slice_left[axis]   =       amount
	slice_right[axis]  = 1.0 - amount
	offset_left[axis]  = 1.0
	offset_right[axis] = 1.0
	
	var size_left  = size * slice_left
	var size_right = size * slice_right
	
	var origin_left  = origin + size_right * offset_left
	var origin_right = origin - size_left  * offset_right
	
	return [
		[origin_left,  size_left],
		[origin_right, size_right],
	]

func generate_partitions(origin, size):
	Util.remove_children($Rooms)
	
	var floor_size = size * Vector3(1.0, 1.0/gen_floors, 1.0)
	
	for f in (gen_floors):
		var rng = RandomNumberGenerator.new()
		rng.seed = gen_seed
		var p = float(f)/int(gen_floors)
		p = p*2.0 - 1.0
		generate_partitions_for_shape(
			origin + (Vector3(0, p, 0)) * height + floor_size * Vector3(0, 1, 0),
			floor_size,
			rng, 0,
			Vector3(0, 0, 0),
			Vector3(0, f, 0))

func generate_partitions_for_shape(origin, size, rng, current_axis, current_depth, current_index):
	var p = rng.randf()
	var slice = slice(origin, size, current_axis, 0.5 + (p-0.5)*(1.0-gen_uniformity))
	
	var current_depth_sum = Geo.sum(current_depth)
	
	var index_offset_left  = Vector3(0, 0, 0)
	var index_offset_right = Vector3(0, 0, 0)
	
	index_offset_right[current_axis] = pow(2, (gen_max_depth - current_depth_sum + 1)) - 1
	
	var index_left  = current_index + index_offset_left
	var index_right = current_index + index_offset_right
	
	if (current_depth_sum >= gen_max_depth):
		rooms.append(slice[0])
		# add_box(slice[0][0], slice[0][1]*0.99, Color(1.0, 0.0, 0.0, 0.0))
		rooms.append(slice[1])
		# add_box(slice[1][0], slice[1][1]*0.99, Color(0.0, 1.0, 0.0, 0.0))
		return
	
	current_depth[current_axis] += 1
	
	# Don't slice along the Y axis here.
	if current_axis == 0:
		current_axis = 2
	else:
		current_axis = 0
	
	generate_partitions_for_shape(slice[0][0], slice[0][1], rng, current_axis, current_depth, index_left)
	generate_partitions_for_shape(slice[1][0], slice[1][1], rng, current_axis, current_depth, index_right)

func generate_turtle(rooms):
	var rng = RandomNumberGenerator.new()
	rng.seed = gen_seed
	
	var total_volume = 0
	
	for room in rooms:
		if (rng.randf() > 0.5) || (total_volume == 0):
			add_box(room[0], room[1], Color(1.0, 0.0, 0.0, 0.0))
			total_volume += Geo.volume(room[1])
