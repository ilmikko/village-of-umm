extends Spatial

var BreakageExpert = Runtime.Expert.Breakage


func _ready():
	BreakageExpert.prepare($Chair)
	BreakageExpert.prepare($Chair2)


func _input(_event):
	if Input.is_action_just_pressed("debug_1"):
		BreakageExpert.break($Chair)
	if Input.is_action_just_pressed("debug_2"):
		BreakageExpert.break($RoofPillar4x)
	if Input.is_action_just_pressed("debug_3"):
		BreakageExpert.break($StudyTable/StaticBody)
