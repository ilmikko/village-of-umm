extends Spatial


func tick():
	$GlobalLighting.phase = fmod($GlobalLighting.phase + 0.0005, 1)


func _ready():
	$GlobalLighting.rebuild()
	$Timer.start()


func _on_Timer_timeout():
	tick()
