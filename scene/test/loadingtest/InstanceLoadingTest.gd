extends Spatial


var t = 0.0


func tick():
	var si = yield(Runtime.Expert.LoadingInstance.instance(EXPENSIVE_SCENE), Runtime.Expert.LoadingInstance.INSTANCED)
	si.global_transform.origin = Vector3(0, 0, t)
	t += 15.0
	$Instances.add_child(si)


var EXPENSIVE_SCENE = preload("res://world/house/simple2x1/2x1House.tscn")


func _on_Timer_timeout():
	tick()
